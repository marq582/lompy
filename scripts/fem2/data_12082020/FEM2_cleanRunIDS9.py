import pandas as pd
import csv
import sys, traceback
import os
import gc
import random

if ('--help' in sys.argv):
  print ("> python FEM2_cleanRunIDs.py [RUNIDFILE='runIDs_found.csv']   ")
  print ("Examples:")
  print ("  > python FEM2_cleanRunIDs.py ") 
  print ("  > python FEM2_cleanRunIDs.py 'another.csv'") 
  print ("  > ~/Desktop/Delivery/opt/csiro.au/evacuation_modelling_fem2_batch/bin/app_FEM2_batch --launch python3.6 FEM2_cleanRunIDS1.py")
  exit()

arglen = len(sys.argv)

# input tables
runidCSV = "runIDs_found.csv"  
floodscenCSV = "FEM2_flood_events.csv"
  
if (arglen>1) :
  runidCSV = sys.argv[1]    
  
print ("\n **** Current working directory: "+os.getcwd())

# output tables
cleanCSV = "ensemble_runs_attribs_rev.csv"

#try:

flood_df = pd.read_csv(floodscenCSV, header=None)
flood_df.columns= ['floodevent']

flood_df = flood_df.sort_values(['floodevent'], ascending=[True])
flood_df['rowno']= flood_df.reset_index().index
req_runIDs = len(flood_df.index)

print("\nflood_df Tail")
print(flood_df.tail(5))  
print("Required number of runIDs per dam scenario  = "+str(req_runIDs))
                       
runidReader = pd.read_csv(runidCSV)
print("\nInitial Shape of User_file")
print(runidReader.dtypes)
print(runidReader)

runID_list = []

#create ensemble summary table
for index, row in runidReader.iterrows():
   
    isnotFileset = True

    pathnm = row[1].strip()
    flname = row[2].strip()
    curr_ensmbl_file =  pathnm+"/"+flname
    rev_ensmbl_file =  pathnm+"/"+cleanCSV

    print("\nProcessing <"+curr_ensmbl_file+">")      
    ensmbl_df = pd.read_csv(curr_ensmbl_file, na_filter=False, error_bad_lines=False)
    curr_runIDs = len(ensmbl_df.index)
    col_hdrs = ensmbl_df.columns
    print("Available number of runIDs = "+str(curr_runIDs))
    
    if (curr_runIDs < req_runIDs):
      print("*********************************************************")
      print("*** ERROR: Insufficient number of runIDs available! ***")
      print("*********************************************************")
      sys.exit()
      
    if ('timestamp' not in col_hdrs):
      print("*********************************************************")
      print("**** Missing timestamp header in  <"+curr_ensmbl_file+"> . *********")
      print("*********************************************************")
      col_hdrs.append('timestamp')
      ensmbl_df.columns = col_hdrs
      
    ensmbl_df['run_id'] = ensmbl_df['run_id'].astype(str)
    ensmbl_df['timestamp'] = ensmbl_df['timestamp'].astype(str)

    ensmbl_df['isRemove'] = 1      
    ensmbl_df = ensmbl_df.sort_values(['output_dir', 'timestamp'], ascending=[True, False])

    #set flood event column
    ensmbl_df['floodevent'] = ensmbl_df['output_dir'].str.split('/',expand=True).iloc[:,-1]

    print("\nHead 5 of ensmbl_df")
    print(ensmbl_df[1:5])
    #print(ensmbl_df.dtypes)

    ok_cnt = 0

    for index2, row2 in ensmbl_df.iterrows():
      curr_id = row2['run_id'].strip()
      curr_fe = row2['floodevent'].strip()
      curr_stamp = row2['timestamp'].strip()
      
      if (len(curr_id)>7 and len(curr_stamp)>10):
        if (isnotFileset):
          curr_dir = row2['output_dir'].strip()
          curr_dir = curr_dir.rsplit("/", 1)[0]

          curr_fefile = row2['flood_event_file'].strip()
          curr_fefile = curr_fefile.rsplit("/",1)[0]

          curr_zonefile = row2['zone_shapefile'].strip()
          curr_linkfile = row2['link_shapefile'].strip()
          curr_nodefile = row2['node_shapefile'].strip()
          curr_hydfile = ""
          isnotFileset = False
        
        if (curr_id not in runID_list): 
          ensmbl_df.at[index2,'isRemove'] = 0
          runID_list.append(curr_id)
          ok_cnt +=1

      if (ok_cnt >= req_runIDs+2):
        break 
    
    
    if (ok_cnt < req_runIDs):
      print("*********************************************************")
      print("**** Incomplete runIDs in  <"+rev_ensmbl_file+"> . *********")
      print("*********************************************************")
      sys.exit()

    # delete all rows with isRemove = 1 
    indexNames = ensmbl_df[ (ensmbl_df['isRemove'] > 0) ].index
    ensmbl_df.drop(indexNames , inplace=True)
    ensmbl_df['rowno']= ensmbl_df.reset_index().index
    print("\ntail 5 of ensmbl_df")
    print(ensmbl_df.tail(5))
    #print(ensmbl_df.dtypes)

    #merge flood event and runIDs
    revIDs_df =  ensmbl_df[['rowno','run_id','timestamp']]
    revIDs_df = pd.merge(flood_df, revIDs_df, on='rowno', how='left')    

    revIDs_df ['output_dir'] = curr_dir + "/" + revIDs_df ['floodevent']
    revIDs_df ['flood_event_file'] = curr_fefile + "/" + revIDs_df ['floodevent'] + ".csv"
    revIDs_df ['zone_shapefile'] = curr_zonefile
    revIDs_df ['link_shapefile'] = curr_linkfile
    revIDs_df ['node_shapefile'] = curr_nodefile
    revIDs_df ['node_shapefile'] = curr_nodefile
    revIDs_df ['hydrograph_shapefile'] = ""
    revIDs_df ['timestamp'] = curr_stamp

    print("\ntail 5 of revIDs_df")
    print(revIDs_df.tail(5))
    #print(revIDs_df.dtypes)

    exc_cols = ['run_id', 'output_dir', 'flood_event_file','zone_shapefile', 'link_shapefile', 'node_shapefile','hydrograph_shapefile', 'timestamp']

    rev_ensmbl_df =  revIDs_df[exc_cols]

    print("\n>>>>>>>>>>>>>>> Tail 5 of rev_ensmbl_df <<<<<<<<<<<<<<<<<<")
    print(rev_ensmbl_df.tail(5))
    #save to csv file
    rev_ensmbl_df.to_csv(rev_ensmbl_file, index=False)
    print("Completed file <"+rev_ensmbl_file+">" ) 
            
#except:
  #traceback.print_exc(file=sys.stdout)
  #print ("**** Error encountered. Printing traceback! ****")
  #traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
  #print("*** Print_exception:")
  ## exc_type below is ignored on 3.5 and later
  #traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)

#finally:

print( "\nExecution of <FEM2_cleanRunIDS.py> completed!")