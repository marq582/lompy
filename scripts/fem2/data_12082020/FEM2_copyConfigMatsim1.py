import os
import sys
import subprocess

if ('--help' in sys.argv):
  print ("FEM2_copyConfigMatsim1.py [SRCDIR='.'] [COPYLIMIT=none]")
  print ("Examples:") 
  print ("  > python FEM2_copyConfigMatsim1.py ") 
  print ("  > python FEM2_copyConfigMatsim1.py A0  ") 
  print ("  > FEM2_copyConfigMatsim1.py olddir  100") 
 
  exit()
  
arglen = len(sys.argv)
srcDir = "."
copyLimit = -1

#selFiles = ['configMATSim.xml', 'onerunXareasum.csv', 'onerunXlinksum.csv', 'volcapratio_per_interval.csv']
selFiles = ['configMATSim.xml']
#selFiles = ['ensemble_runs_attribs_rev.csv']
if (arglen>1) :
  srcDir = sys.argv[1]
if (arglen>2) :
  copyLimit = int(sys.argv[3])

for selfile in selFiles:
  knt = 0
  for (path, dirs, files) in os.walk(srcDir):
    if selfile in files: # compares to your specified files
      knt = knt+1
      newfle = selfile.replace(".", "_rev.", 1)

      cpcommand = "cp -rf '"+path+"/"+selfile+ "' '"+path+"/"+newfle+"'"   
      print (knt,': ',cpcommand)
      os.system(cpcommand)
      if copyLimit > 0:
        if knt >= copyLimit:
          break
print ("\n")
                