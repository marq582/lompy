import csv
import os
import sys
import subprocess

if ('--help' in sys.argv):
  print ("> python FEM2_deleteLogOuts.py [POPNSCENFILE='FEM2_popn_scenarios.csv']   ")
  print ("Examples:")
  print ("  > python FEM2_deleteLogOuts.py ") 
  print ("  > python FEM2_deleteLogOuts.py 'another.csv'") 
  print ("  > ~/Desktop/Delivery/opt/csiro.au/evacuation_modelling_fem2_batch/bin/app_FEM2_batch --launch python3.6 FEM2_deleteLogOuts.py")
  exit()

arglen = len(sys.argv)

# input tables
popnscenCSV = "FEM2_popn_scenarios.csv" 
if (arglen>1) :
  popnscenCSV = sys.argv[1]    
  
print ("Current working directory: "+os.getcwd())

try:

  knt = 0

  with open(popnscenCSV, 'r') as  popncsvReader:
   for popn_row in popncsvReader:
  
      #get popn runs directory
      popn_scen = popn_row.strip()
      run_dir = "./"+popn_scen+"/runs"
      print("Deleting from Rundir <"+run_dir+">")
      out_files = run_dir + "/*.out"

      cmd = "rm -f " + out_files
      returned_value = subprocess.call(cmd, shell=True)  # returns the exit code in unix
      print(knt,' Returned value:', returned_value, 'for <',cmd,'>')
                
except:
  traceback.print_exc(file=sys.stdout)
  print ("**** Error encountered. Printing traceback! ****")
  traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
  print("*** Print_exception:")
  # exc_type below is ignored on 3.5 and later
  traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)

finally:

  print( "\nExecution of <FEM2_deleteLogOuts.py> completed!")