#!/bin/bash
#SBATCH --job-name=__minOut
#SBATCH --mem=100mb
#SBATCH --time=02:00:00
#SBATCH --output=__copyMinXX.out
#SBATCH --error=__copyMinXX.err

starttime=`date +%s`
echo "StartTime: $(date)"

python FEM2_copyMinOutput3.py

endtime=`date +%s`
echo "EndTime: $(date)"
echo "Time: $(( ( endtime - starttime ) / 60 )) mins"

exit 0

