import csv
import sys, traceback
import os
import gc

if ('--help' in sys.argv):
  print ("FEM2_runIDsToPBIcsv.py [POPCSVFILE='FEM2_popn_scenarios.csv'] [RUNIDFILE='revisedIDs_found.csv']")
  print ("Examples:") 
  print ("  > python FEM2_runIDsToPBIcsv.py ") 
  print ("  > python FEM2_runIDsToPBIcsv.py somesrcfile.csv  ") 
  exit()
  
arglen = len(sys.argv)

# input tables
popnCSV =  "FEM2_popn_scenarios.csv"
if (arglen>1) :
  popnCSV = sys.argv[1]
runidCSV = "revisedIDs_found.csv"  
if (arglen>2) :
  runidCSV = sys.argv[2]    
  
print ("Current working directory: "+os.getcwd())

# output tables
scenRunCSV = "ScenarioRuns.csv"

# Write rerun list
rowrec = ['Output_Dir', 'Scen_Name']
scenRunFile = open(scenRunCSV, 'w')
scenRunWriter = csv.writer(scenRunFile)
scenRunWriter.writerow(rowrec)

try:
  print("\nCreate popn scen list")
  popn_lst = []
  with open(popnCSV, 'r') as  popncsvReader:
    for popn_row in popncsvReader:
      if (len(popn_row.strip())>0):
        popn_scen = popn_row.strip()
        popn_lst.append(popn_scen)
  print("Popn_Scen:", popn_lst)
        
  isFirst = True
  print("\nCreate Source Run list")  
  with open(runidCSV, 'r') as  runIDReader:
    for runID_row in runIDReader: 
      if (isFirst): 
        isFirst = False
      elif (len(runID_row.strip())>0):
        runID_path = runID_row.rsplit(",")[1]
        runID_file = runID_row.rsplit(",")[2]
        #print("runID_path",runID_path)
        src_file = runID_path.strip()+"/"+runID_file.strip()
        #print("src_file",src_file)
        chk_scen = runID_path.rsplit("/")
        #print("Chk_scen",chk_scen)
        popn_scen = "---"
        for scen in chk_scen:
          if (scen in popn_lst):
            popn_scen = scen
        rowrec = [src_file, " "+popn_scen]
        print(rowrec)
        scenRunWriter.writerow(rowrec)
except:
  traceback.print_exc(file=sys.stdout)
  print ("**** Error encountered. Printing traceback! ****")
  traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
  print("*** Print_exception:")
  # exc_type below is ignored on 3.5 and later
  traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)

finally:
  scenRunFile.close()
  print( "\nExecution of <FEM2_runIDsToPBIcsv.py> completed!")                