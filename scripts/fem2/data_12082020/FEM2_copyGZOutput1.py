import os
import sys
import subprocess

if ('--help' in sys.argv):
  print ("FEM2_copyGZOutput.py [SRCDIR='.'] [DESTDIR='../__destdir'] [COPYLIMIT='']")
  print ("Examples:") 
  print ("  > python FEM2_copyGZOutput.py ") 
  print ("  > python FEM2_copyGZOutput.py A0  A5  10") 
 
  exit()
  
arglen = len(sys.argv)
srcDir = "."

destDirName = "__GZOutput"
destDir = "../"+destDirName

#selFiles = ['area_evactrips.csv', 'onerunXareasum.csv', 'onerunXlinksum.csv', 'volcapratio_per_interval.csv']
#selFiles = ['output_change_events.xml.gz']
selFiles = ['output_change_events.xml.gz', 'output_events.xml.gz', 'output_network.xml.gz']

if (arglen>1) :
  srcDir = sys.argv[1]
if (arglen>2) :
  destDir = sys.argv[2]
copyLimit = -1
if (arglen>3) :
  copyLimit = int(sys.argv[3])

for selfile in selFiles:
  print ("Copying <"+selfile+"> from <"+srcDir+"> to <"+destDir+">")
  knt = 0
  for (path, dirs, files) in os.walk(srcDir):
    if selfile in files: # compares to your specified files
      knt = knt+1
      newpath = path.replace("./", destDir+"/")
      mdcommand = "mkdir -p '"+newpath+"'"
      print (mdcommand)
      os.system(mdcommand)
      cpcommand = "cp -rf '"+path+"/"+selfile+ "' '"+newpath+"'"   
      print (cpcommand)
      os.system(cpcommand)
      if copyLimit > 0:
        if knt >= copyLimit:
          break
print (str(knt)+" output files have been copied from <"+srcDir+"> to <"+destDir+">.")
print ("List of files copied: ",selFiles)
print ("\n")
                