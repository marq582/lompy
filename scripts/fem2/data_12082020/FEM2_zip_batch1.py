import csv
import subprocess
import os
import sys

if ('--help' in sys.argv):
  print ("> python FEM2_zip_batch.py [SCENARIO_DIR] ")
  print ("Examples:")
  print ("  > python FEM2_zip_batch.py  ScenarioB11A") 
  exit()
  
arglen = len(sys.argv)
if (arglen<2) :
  print("ERROR: Missing scenario directory.")  
  exit()


if (arglen>2) :
  optno = int(sys.argv[2])  
popn_scen = sys.argv[1]
  
# input tables
zipIN= "FEM2_zip1.in"

# Read workflow file
slurmFileMaster = open(zipIN, 'r')
slurmDataMaster = slurmFileMaster.read();
slurmFileMaster.close()

jobname = popn_scen[-5:] + "zip"        
knt = 1     
      
# Create new slurm file
slurmFileName = "./" + popn_scen+"/zip_"+ popn_scen +".sh"
slurmData = slurmDataMaster.replace('__JOB__', jobname)
slurmData = slurmData.replace('__SCENDIR__', popn_scen)
            
with open(slurmFileName, 'w') as slurmFile:
  slurmFile.write(slurmData)
  slurmFile.close()

print("Jobname <"+jobname+"> for slurmFileName <"+slurmFileName+">.")
        
# Run job
print(knt, "sbatch", slurmFileName)
subprocess.call(["sbatch", slurmFileName])

print("\n *** Submitted "+str(knt)+" jobs for ZIP. ***")
