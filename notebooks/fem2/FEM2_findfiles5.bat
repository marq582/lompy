#!/bin/bash
echo "Find configMATSim.xml and save path in configMAT_found.csv"
python FEM2_findfiles1.py configMATSim.xml './' > configMAT_found.csv
echo "Find volcapratio_per_interval.csv and save path in volcap_found.csv" 
python FEM2_findfiles1.py volcapratio_per_interval.csv './' > volcap_found.csv 
echo "Find output_events.xml.gz and save path in eventsgz_found.csv"
python FEM2_findfiles1.py output_events.xml.gz './' > eventsgz_found.csv
echo "Find output_network.xml.gz and save path in networkgz_found.csv"
python FEM2_findfiles1.py output_network.xml.gz './' > networkgz_found.csv
echo "Find scorestats.txt and save path in scorestats_found.csv"
python FEM2_findfiles1.py scorestats.txt './' > scorestats_found.csv
echo "Find input_network.xml and save path in inputnet_found.csv"
python FEM2_findfiles1.py input_network.xml './' > inputnet_found.csv
echo "Find arrdepXtime.csv and save path in arrdep_found.csv" 
python FEM2_findfiles1.py arrdepXtime.csv './' > arrdep_found.csv 
echo "Find matsim_output.db and save path in matsimDB_found.csv"  
python FEM2_findfiles1.py matsim_output.db './' > matsimDB_found.csv
echo "Find output_matsim_journeys.txt and save path in matsim_journeys_found.csv"  
python FEM2_findfiles1.py output_matsim_journeys.txt './' > matsim_journeys_found.csv
echo "Find ensemble_runs_attribs.csv and save path in runIDs_found.csv"
python FEM2_findfiles1.py ensemble_runs_attribs.csv './' > runIDs_found.csv
echo "Find ensemble_runs_attribs_rev.csv and save path in revisedIDs_found.csv"
python FEM2_findfiles1.py ensemble_runs_attribs_rev.csv './' > revisedIDs_found.csv

            