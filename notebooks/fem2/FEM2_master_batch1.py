import csv
import os
import subprocess

# Read workflow file
slurmFileMaster = open('FEM2_master.in', 'r')
slurmDataMaster = slurmFileMaster.read();
slurmFileMaster.close()

# Read flood events
floodcsvFile = open('FEM2_flood_events.csv', 'r')
floodcsvReader = csv.reader(floodcsvFile)

# Read population scenarios
popncsvFile = open('FEM2_popn_scenarios.csv', 'r')
popncsvReader = csv.reader(popncsvFile)

# Read dam scenarios
damcsvFile = open('FEM2_dam_scenarios.csv', 'r')
damcsvReader = csv.reader(damcsvFile)

for popn_row in popncsvReader:
   print ("pop <"+popn_row[0].strip()+">")
for dam_row in damcsvReader:
   print("dam <"+dam_row[0].strip()+">")
for flood_row in floodcsvReader:
   print("flood <"+flood_row[0].strip()+">")

knt = 0

for popn_row in popncsvReader:
  
   #create popn runs directory
   popn_scen = popn_row[0].strip()
   run_dir = "./"+popn_scen+"/runs"
   if not os.path.exists(popn_scen):
     os.makedirs(popn_scen)   
   if not os.path.exists(run_dir):
     os.makedirs(run_dir)   
   
   # Read project file template
   projTemplateName = 'FEM2_master_{0}.xml'.format(popn_scen)
   projFileMaster = open(projTemplateName, 'r')
   projDataMaster = projFileMaster.read();
   projFileMaster.close() 

   for dam_row in damcsvReader:
     dam_scenario = dam_row[0].strip()
      
     for flood_row in floodcsvReader:
        floodeventID = flood_row[0].strip()
        # Create name
        name = '{0}_{1}'.format(dam_scenario, floodeventID)
        print(name)

        # Create new project file
        projFileName = run_dir+"/"
        projFileName += 'FEM2_run_{0}.xml'.format(name)
        projData = projDataMaster.replace('__DAM_SCENARIO__', dam_scenario)
        projData = projData.replace('__FLOOD_EVENT__', floodeventID + '.csv')
            
        with open(projFileName, 'w') as projFile:
            projFile.write(projData)
            projFile.close()

        # Create new slurm file
        slurmFileName = run_dir+"/"
        slurmFileName += 'FEM2_run_{0}.in'.format(name)
        slurmData = slurmDataMaster.replace('__NAME__', name)
        slurmData = slurmData.replace('__DAM_SCENARIO__', dam_scenario)
        slurmData = slurmData.replace('__RUNDIR__', run_dir)
        slurmData = slurmData.replace('__FLOOD_EVENT__', floodeventID)
            
        with open(slurmFileName, 'w') as slurmFile:
            slurmFile.write(slurmData)
            slurmFile.close()
    
        # Run job
        knt += 1
        print(knt, "sbatch", slurmFileName)
        #subprocess.call(["sbatch", slurmFileName])
