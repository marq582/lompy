import os
import sys
import subprocess

if ('--help' in sys.argv):
  print ("> python FEM2_replaceSTR1inTXT.py [SOURCEstr] [REPLACEMENTstr]   ")
  print ("Replace all occurrences of [SOURCEstr] by [REPLACEMENTstr] in all configMATSIM.xml")
  print ("Examples:")
  print ("  > python FEM2_replaceSTR1inTXT.py 'TEXT1' 'TEXT2' ") 
  exit()

arglen = len(sys.argv)
optno = 1
SRCstr = sys.argv[1]
REPstr = sys.argv[2]
srcDir = "."
selFile = 'configMATSim_rev.xml'

print(" Sourcedir in  <"+srcDir+"> ")
print(" SRCstr in  <"+SRCstr+"> ")
print(" REPstr in  <"+REPstr+"> ")

knt = 0

for path, dirs, files in os.walk(os.path.abspath(srcDir)):
    #if name in files: # compares to your specified files

    for name in files:      
      if name == selFile: 

            knt = knt+1
            filepath = os.path.join(path, name)
            print ("*** FILEPATH ",knt," in  <"+filepath+"> ")

            with open(filepath) as f: 

                s = f.read() 

            s = s.replace(SRCstr, REPstr) 

            with open(filepath, "w") as f:

                f.write(s) 