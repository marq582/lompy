import pandas as pd
import csv
import sys, traceback
import os
import gc
import random

if ('--help' in sys.argv):
  print ("> python FEM2_deleteRunIDS.py [RUNIDFILE='runIDs_found.csv']   ")
  print ("Examples:")
  print ("  > python FEM2_deleteRunIDS.py ") 
  print ("  > python FEM2_deleteRunIDS.py 'another.csv'") 
  print ("  > ~/Desktop/Delivery/opt/csiro.au/evacuation_modelling_fem2_batch/bin/app_FEM2_batch --launch python3.6 FEM2_cleanRunIDS1.py")
  exit()

arglen = len(sys.argv)

# input tables
runidCSV = "runIDs_found.csv"  
if (arglen>1) :
  runidCSV = sys.argv[1]    
  
print ("Current working directory: "+os.getcwd())

try:
  runidReader = pd.read_csv(runidCSV)
  print("\nInitial Shape of User_file")
  print(runidReader.dtypes)
  print(runidReader)
  
  runID_list=[]

  #loop through list
  knt = 0
  for index, row in runidReader.iterrows():
      pathnm = row[1].strip()
      flname = row[2].strip()
      curr_ensmbl_file =  pathnm+"/"+flname
      cmd = "rm -f " + curr_ensmbl_file
      returned_value = subprocess.call(cmd, shell=True)  # returns the exit code in unix
      print(knt,' returned value:', returned_value, 'for <',cmd,'>'')
                
except:
  traceback.print_exc(file=sys.stdout)
  print ("**** Error encountered. Printing traceback! ****")
  traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
  print("*** Print_exception:")
  # exc_type below is ignored on 3.5 and later
  traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)

finally:

  print( "\nExecution of <FEM2_deleteRunIDS.py> completed!")