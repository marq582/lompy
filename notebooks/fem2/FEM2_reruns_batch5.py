import csv
import subprocess
import os
import sys

if ('--help' in sys.argv):
  print ("> python FEM2_reruns_batch.py [RERUNFILE] [OPTIONNO]")
  print ("OPTIONNO = 1 : Calls  [~/.../app_FEM2_batch  [projectXML] --bypassMATSim  --removeDB] with [minimum_output]")
  print ("OPTIONNO = 2 : Calls  [~/.../app_FEM2_batch  [projectXML] --bypassMATSim  --removeDB] with [medium_output]")
  print ("OPTIONNO = 3 : Calls  [~/.../app_FEM2_batch  [projectXML] --removeDB] with [minimum_output]")
  print ("OPTIONNO = 4 : Calls  [~/.../app_FEM2_batch  [projectXML] --removeDB] with [medium_output]")
  print ("OPTIONNO = 5 : Calls  [~/.../app_FEM2_batch  [projectXML] --bypassMATSim] with [minimum_output]")
  print ("OPTIONNO = 6 : Calls  [~/.../app_FEM2_batch  [projectXML] --bypassMATSim] with [medium_output]")
  print ("OPTIONNO = 7 : Calls  [~/.../app_FEM2_batch  [projectXML] ] with [minimum_output]")
  print ("OPTIONNO = 8 : Calls  [~/.../app_FEM2_batch  [projectXML] ] with [medium_output]")
  print ("Examples:")
  print ("  > python FEM2_reruns_batch.py rerun_bypass.csv ") 
  print ("  > python FEM2_reruns_batch.py rerun_bypass.csv 5 ") 
  print ("  > python FEM2_reruns_batch.py rerun_Matsim.csv 3 ") 
  print ("  > python FEM2_reruns_batch.py rerun_Matsim.csv 7 ") 
  exit()
  
arglen = len(sys.argv)
if (arglen<2) :
  print("ERROR: Missing rerun file.")  
  exit()

optno = 1
if (arglen>2) :
  optno = int(sys.argv[2])  
rerunCSV = sys.argv[1]
  
#output options
medium_output = "#medium output - no deletions except database"
minimum_output = "#minimum output - only specific files retained"
minimum_output += "\ncp -fp input_population_attrs.txt input_population_attrs.log"
minimum_output += "\ncp -fp hydrograph_linkID_time.txt hydrograph_linkID_time.log"
minimum_output += "\ncp -fp configMATSim.xml configMATSim.log"
minimum_output += "\nrm *.xml"
minimum_output += "\nrm *.txt"
minimum_output += "\nrm *.csv"
minimum_output += "\nrm *.gz"
minimum_output += "\nrm input_network*.*"
minimum_output += "\nmv -f input_population_attrs.log input_population_attrs.txt"
minimum_output += "\nmv -f hydrograph_linkID_time.log hydrograph_linkID_time.txt"
minimum_output += "\nmv -f configMATSim.log configMATSim.xml"
minimum_output += "\n#save desired outputs"
minimum_output += "\nmkdir --parents output2"
minimum_output += "\ncp -p ./output/*.csv ./output2"
minimum_output += "\ncp -p ./output/*.db ./output2"
minimum_output += "\ncp -p ./output/output_matsim_*.txt ./output2"
minimum_output += "\ncp -p ./output/output_events.xml.gz ./output2"
minimum_output += "\ncp -p ./output/output_network.xml.gz ./output2"
minimum_output += "\ncp -p ./output/output_config.* ./output2"
minimum_output += "\nrm -rf ./output"
minimum_output += "\nmv -f output2 output"   

_options = '--bypassMATSim  --removeDB'
_output = minimum_output
if (optno==2):
  _output = medium_output
if (optno==3):
  _options = '--removeDB'
if (optno==4):
  _options = '--removeDB'
  _output = medium_output
if (optno==5):
  _options = '--bypassMATSim'
if (optno==6):
  _options = '--bypassMATSim'
  _output = medium_output
if (optno==7):
  _options = ''
if (optno==8):
  _options = ''
  _output = medium_output
    
# input tables
masterIN = "FEM2_master3.in"

# output tables


# Read workflow file
slurmFileMaster = open(masterIN, 'r')
slurmDataMaster = slurmFileMaster.read();
slurmFileMaster.close()

# Read XML filelist
rerunFile = open(rerunCSV, 'r')
rerunReader = csv.reader(rerunFile)
knt = 0
isFirst=True
for row in rerunReader:
    if (isFirst):
        isFirst = False
    else:
        #print(row)
        #print(row[1])
        #print(row[1].split("/output"))
        knt = knt+1
        rname = row[1].split("/output")[1]
        dam_scenario = rname.split("/")[1]
        floodeventID = rname.split("/")[2]
        rname = dam_scenario+"_"+floodeventID
        #print(rname)
        run_dir = "./"+row[3].strip()+"/runs"  
        #print(run_dir)      
        
        # Create new slurm file
        slurmFileName = run_dir+"/"
        slurmFileName += 'FEM2_rerun_{0}.in'.format(rname)
        slurmData = slurmDataMaster.replace('__NAME__', rname)
        slurmData = slurmData.replace('__DAM_SCENARIO__', dam_scenario)
        slurmData = slurmData.replace('__RUNDIR__', run_dir)
        slurmData = slurmData.replace('__FLOOD_EVENT__', floodeventID)
        slurmData = slurmData.replace('__CLEAN_UP__', _output)
        slurmData = slurmData.replace('__RUN_OPTIONS__', _options)
            
        with open(slurmFileName, 'w') as slurmFile:
            slurmFile.write(slurmData)
            slurmFile.close()
        
        # Run job
        print(knt, "sbatch", slurmFileName)
        subprocess.call(["sbatch", slurmFileName])

print("\n *** Submitted "+str(knt)+" jobs for RERUN. ***")
