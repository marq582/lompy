import csv
import subprocess
import os
import sys

if ('--help' in sys.argv):
  print ("> python FEM2_reruns_batch.py [RERUNFILE] [OPTIONNO]")
  print ("OPTIONNO = 1 : Calls  [~/.../app_FEM2_batch  [projectXML] --bypassMATSim  --removeDB] with [minimum_output]")
  print ("OPTIONNO = 2 : Calls  [~/.../app_FEM2_batch  [projectXML] --bypassMATSim  --removeDB] with [medium_output]")
  print ("OPTIONNO = 3 : Calls  [~/.../app_FEM2_batch  [projectXML] --removeDB] with [minimum_output]")
  print ("OPTIONNO = 4 : Calls  [~/.../app_FEM2_batch  [projectXML] --removeDB] with [medium_output]")
  print ("OPTIONNO = 5 : Calls  [~/.../app_FEM2_batch  [projectXML] --bypassMATSim] with [minimum_output]")
  print ("OPTIONNO = 6 : Calls  [~/.../app_FEM2_batch  [projectXML] --bypassMATSim] with [medium_output]")
  print ("OPTIONNO = 7 : Calls  [~/.../app_FEM2_batch  [projectXML] ] with [minimum_output]")
  print ("OPTIONNO = 8 : Calls  [~/.../app_FEM2_batch  [projectXML] ] with [medium_output]")
  print ("Examples:")
  print ("  > python FEM2_reruns_batch.py rerun_bypass.csv ") 
  print ("  > python FEM2_reruns_batch.py rerun_bypass.csv 5 ") 
  print ("  > python FEM2_reruns_batch.py rerun_Matsim.csv 3 ") 
  print ("  > python FEM2_reruns_batch.py rerun_Matsim.csv 7 ") 
  exit()
  
arglen = len(sys.argv)
if (arglen<2) :
  print("ERROR: Missing rerun file.")  
  exit()

optno = 1
if (arglen>2) :
  optno = int(sys.argv[2])  
rerunCSV = sys.argv[1]
  


_options = '--bypassMATSim  --removeDB'
_output = minimum_output
if (optno==2):
  _output = medium_output
if (optno==3):
  _options = '--removeDB'
if (optno==4):
  _options = '--removeDB'
  _output = medium_output
if (optno==5):
  _options = '--bypassMATSim'
if (optno==6):
  _options = '--bypassMATSim'
  _output = medium_output
if (optno==7):
  _options = ''
if (optno==8):
  _options = ''
  _output = medium_output
    
# input tables
masterIN = "FEM2_master_A0A1A2.in"
damscenCSV = "FEM2_dam_scenarios.csv"
floodscenCSV = "FEM2_flood_events.csv" 
popnscenCSV = "FEM2_popn_scenarios.csv"

# output tables


# Read workflow file
slurmFileMaster = open(masterIN, 'r')
slurmDataMaster = slurmFileMaster.read();
slurmFileMaster.close()

knt = 0

with open(popnscenCSV, 'r') as  popncsvReader:
  for popn_row in popncsvReader:
  
    #create popn runs directory
    popn_scen = popn_row.strip()
    run_dir = "./"+popn_scen+"/runs"
    #print("Rundir <"+run_dir+">")
    
    if not os.path.exists(popn_scen):
      os.makedirs(popn_scen)   
    if not os.path.exists(run_dir):
      os.makedirs(run_dir)   
   
    with open(damscenCSV, 'r') as damcsvReader:
      for dam_row in damcsvReader:
        dam_scenario = dam_row.strip()
      
        with open(floodscenCSV, 'r') as  floodcsvReader:
          for flood_row in floodcsvReader:
            floodeventID = flood_row.strip()
            # Create name
            name = '{0}_{1}'.format(dam_scenario, floodeventID)
  
            # Create new slurm file
            slurmFileName = run_dir+"/"
            slurmFileName += 'FEM2_rerun_{0}.in'.format(name)
            slurmData = slurmDataMaster.replace('__NAME__', name)
            slurmData = slurmData.replace('__POPN_SCENARIO__', popn_scen)
            slurmData = slurmData.replace('__DAM_SCENARIO__', dam_scenario)
            slurmData = slurmData.replace('__RUNDIR__', run_dir)
            slurmData = slurmData.replace('__FLOOD_EVENT__', floodeventID)
            
            with open(slurmFileName, 'w') as slurmFile:
              slurmFile.write(slurmData)
              slurmFile.close()
    
            # Run job
            knt += 1
            print(knt, "sbatch", slurmFileName)
            #subprocess.call(["sbatch", slurmFileName])

          
print ("FEM2_rerun_A0A1A2 completed.")
