import os
import sys
import subprocess

if ('--help' in sys.argv):
  print ("FEM2_copyMinOutput.py [SRCDIR='.'] [DESTDIR='../__destdir'] [COPYLIMIT='']")
  print ("Examples:") 
  print ("  > python FEM2_copyMinOutput.py ") 
  print ("  > python FEM2_copyMinOutput.py A0  A5  10") 
 
  exit()
  
arglen = len(sys.argv)
srcDir = "."
destDir = "../__minOutput"

#selFiles = ['area_evactrips.csv', 'onerunXareasum.csv', 'onerunXlinksum.csv', 'volcapratio_per_interval.csv']
selFiles = ['area_evactrips.csv', 'onerunXareasum.csv', 'onerunXlinksum.csv', 'volcapratio_per_interval.csv', 'ensemble_runs_attribs.csv', 'ensemble_runs_attribs_rev.csv']
#selFiles = ['area_evactrips.csv', 'onerunXareasum.csv', 'onerunXlinksum.csv', 'volcapratio_per_interval.csv', 'ensemble_runs_attribs_rev.csv', 'ScenarioRuns.csv']

if (arglen>1) :
  srcDir = sys.argv[1]
if (arglen>2) :
  destDir = sys.argv[2]
copyLimit = -1
if (arglen>3) :
  copyLimit = int(sys.argv[3])
copied_files = []

for selfile in selFiles:
  knt = 0
  for (path, dirs, files) in os.walk(srcDir):
    if selfile in files: # compares to your specified files
      knt = knt+1
      newpath = path.replace("./", destDir+"/")
      mdcommand = "mkdir -p '"+newpath+"'"
      #print (mdcommand)
      os.system(mdcommand)
      cpcommand = "cp -rf '"+path+"/"+selfile+ "' '"+newpath+"'"   
      #print (cpcommand)
      os.system(cpcommand)
      if copyLimit > 0:
        if knt >= copyLimit:
          break
  copied_files.append(selfile )
  print ("Completed copying "+ str(knt)+" files of <"+selfile+"> from <"+srcDir+"> to <"+destDir+">")

print ("List of files copied: ",copied_files)
print ("\n")
                