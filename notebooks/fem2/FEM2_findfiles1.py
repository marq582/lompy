import os
import sys

if ('--help' in sys.argv):
  print ("FEM2_findfiles.py [SEARCHFILE] [STARTDIRECTORY] {COUNTLIMIT}")
  print ("Examples:")
  print ("  > python FEM2_findfiles.py arrdepXtime.csv './' > arrdep_found.csv") 
  print ("  > python FEM2_findfiles.py output_events.xml.gz './' > eventsgz_found.csv") 
  print ("  > python FEM2_findfiles.py output_plans.xml.gz './' > plansgz_found.csv") 
  print ("  > python FEM2_findfiles.py ensemble_runs_attribs.csv './' > runIDs_found.csv") 
  print ("  > python FEM2_findfiles.py ensemble_runs_attribs_rev.csv './' > revisedIDs_found.csv") 
  print ("  > python FEM2_findfiles.py matsim_output.db './' > matsimDB_found.csv") 
  exit()
  
arglen = len(sys.argv)
srchFile = 'arrdepXtime.csv'
if (arglen>1) :
  srchFile = sys.argv[1]
srchRoot = "."
if (arglen>2) :
  srchRoot = sys.argv[2]
srchLimit = -1
if (arglen>3) :
  srchLimit = int(sys.argv[3])

cur_dir =  os.getcwd()+"/"
#print ("Current working directory: "+cur_dir)

knt = 0
#print ("Searching for <"+srchFile+"> from <"+srchRoot+">")
print ("seqno, path, filename")
for (path, dirs, files) in os.walk(srchRoot):
  if srchFile in files: # compares to your specified conditions
      knt = knt+1 
      fullpath = path.replace("./",cur_dir)
      if (srchFile=='ensemble_runs_attribs_rev.csv'): 
        print (str(knt)+", "+fullpath+", "+srchFile)
      elif (srchFile=='ensemble_runs_attribs.csv'): 
        print (str(knt)+", "+fullpath+", "+srchFile)
      else: 
        print (str(knt)+", "+path+", "+srchFile)
        #print (str(knt)+", "+path+", ", files)
      if srchLimit > 0:
        if knt >= srchLimit:
          break
print ("\n")
                