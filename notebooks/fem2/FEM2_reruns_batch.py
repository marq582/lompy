import csv
import subprocess
import os
import sys

if ('--help' in sys.argv):
  print ("> python FEM2_reruns_batch.py [OPTIONNO] [ERRFILELIST]  ")
  print ("OPTIONNO = 1 : Calls  [~/.../app_FEM2_batch  [projectXML] --bypassMATSim  --removeDB] with [minimum_output]")
  print ("OPTIONNO = 2 : Calls  [~/.../app_FEM2_batch  [projectXML] --bypassMATSim  --removeDB] with [default_output]")
  print ("OPTIONNO = 3 : Calls  [~/.../app_FEM2_batch  [projectXML] --removeDB] with [minimum_output]")
  print ("OPTIONNO = 4 : Calls  [~/.../app_FEM2_batch  [projectXML] --removeDB] with [default_output]")
  print ("OPTIONNO = 5 : Calls  [~/.../app_FEM2_batch  [projectXML] --bypassMATSim] with [minimum_output]")
  print ("OPTIONNO = 6 : Calls  [~/.../app_FEM2_batch  [projectXML] --bypassMATSim] with [default_output]")
  print ("OPTIONNO = 7 : Calls  [~/.../app_FEM2_batch  [projectXML] ] with [minimum_output]")
  print ("OPTIONNO = 8 : Calls  [~/.../app_FEM2_batch  [projectXML] ] with [default_output]")
  print ("Examples:")
  print ("  > python FEM2_reruns_batch.py 1 bypassErrfile.txt") 
  print ("  > python FEM2_reruns_batch.py 4 rerunErrfile.txt") 
  exit()
  
arglen = len(sys.argv)
optno = int(sys.argv[1])
errfilename = sys.argv[2]
_options = '--bypassMATSim  --removeDB'
if (optno==3) or (optno==4):
  _options = '--removeDB'
if (optno==5) or (optno==6) :
  _options = '--bypassMATSim'
if (optno==4) :
  _options = ''
  
#output options
default_output = "#default output - no deletions except database"
minimum_output = "#minimum output - only specific files retained"

# Read workflow file
slurmFileMaster = open('FEM2_rerun.in', 'r')
slurmDataMaster = slurmFileMaster.read();
slurmFileMaster.close()

# Read ERR filelist
txtFile = open(errfilename, 'r')
csvReader = csv.reader(txtFile,  delimiter=',', skipinitialspace=True)
knt = 0
isFirst = True
for row in csvReader: 
  if (isFirst):
      isFirst = False
  else:
        knt = knt+1
        name = row[1]
        name = name.replace(".err","")   
        rundir = "./"+row[2]+"/runs/"        
        
        # Create new slurm file
        slurmFileName = 'FEM2_rerun_{0}.in'.format(name)
        slurmFileName = rundir+slurmFileName

        slurmDataMaster = slurmDataMaster.replace('__NAME__', name)
        slurmDataMaster = slurmDataMaster.replace('__RUNDIR__', rundir)
        slurmData = slurmDataMaster.replace('__RUN_OPTIONS__', _options)
            
        with open(slurmFileName, 'w') as slurmFile:
            slurmFile.write(slurmData)
            slurmFile.close()
        
        # Run job
        print(knt, "sbatch", slurmFileName)
        subprocess.call(["sbatch", slurmFileName])

print("\n *** Submitted "+str(knt)+" jobs for RERUN. ***")
