import pandas as pd
import csv
import sys, traceback
import os
import gc
import random

if ('--help' in sys.argv):
  print ("> python FEM2_cleanRunIDs.py [RUNIDFILE='runIDs_found.csv']   ")
  print ("Examples:")
  print ("  > python FEM2_cleanRunIDs.py ") 
  print ("  > python FEM2_cleanRunIDs.py 'another.csv'") 
  print ("  > ~/Desktop/Delivery/opt/csiro.au/evacuation_modelling_fem2_batch/bin/app_FEM2_batch --launch python3.6 FEM2_cleanRunIDS1.py")
  exit()

arglen = len(sys.argv)

# input tables
runidCSV = "runIDs_found.csv"  
#runidCSV = "testIDs_found.csv"
floodscenCSV = "FEM2_flood_events.csv"
  
if (arglen>1) :
  runidCSV = sys.argv[1]    
  
print ("\n **** Current working directory: "+os.getcwd())

# output tables
cleanCSV = "ensemble_runs_attribs_rev.csv"
#cleanCSV = "ensemble_runs_attribs_revXX.csv"

try:

  flood_df = pd.read_csv(floodscenCSV, header=None)
  tot_rows = len(flood_df.index)

  flood_df.columns= ['floodevent']
  flood_df = flood_df.sort_values(['floodevent'], ascending=[True])

  #print("\nflood_df Tail")
  #print(flood_df.tail(10))  
  print("Target valid records = "+str(len(flood_df.index)))
                         
  runidReader = pd.read_csv(runidCSV)
  print("\nInitial Shape of User_file")
  print(runidReader.dtypes)
  print(runidReader)
  
  runID_list=[]

  #create ensemble summary table
  for index, row in runidReader.iterrows():
      pathnm = row[1].strip()
      flname = row[2].strip()
      curr_ensmbl_file =  pathnm+"/"+flname
      rev_ensmbl_file =  pathnm+"/"+cleanCSV

      ensmbl_df = pd.read_csv(curr_ensmbl_file, na_filter=False)
      col_hdrs = ensmbl_df.columns
   
      #ensmbl_df = ensmbl_df.loc[ensmbl_df['run_id']!=""]
      if ('timestamp' not in col_hdrs):
        print("Missing timestamp header in  <"+curr_ensmbl_file+"> .")
        col_hdrs.append('timestamp')
        ensmbl_df.columns = col_hdrs
        
      ensmbl_df['run_id'] = ensmbl_df['run_id'].astype(str)
      ensmbl_df['isRemove'] = 0      
      ensmbl_df = ensmbl_df.sort_values(['output_dir', 'timestamp'], ascending=[True, False])

      #set flood event column
      ensmbl_df['floodevent'] = ensmbl_df['output_dir'].str.split('/',expand=True).iloc[:,-1]
     
      #print("Top 10 tempdf rows before marking")
      #print(ensmbl_df[['run_id','output_dir','isRemove','floodevent']].head(10))   

      prev_dir = ""
      dup_cnt = 0
      ok_cnt = 0
      #mark duplicate output dirs
      for index2, row2 in ensmbl_df.iterrows():
        curr_id = row2['run_id'].strip()
        if (len(curr_id)>0):
          curr_dir = row2['output_dir'].strip()
          if (prev_dir == curr_dir):  
            ensmbl_df.loc[index2,['isRemove']] = 1    
            dup_cnt += 1
          else:
            ok_cnt += 1
            if (curr_id in runID_list):
              rno = random.randint(10,99)
              new_id = curr_id+str(rno)
              #ensmbl_df.iloc[index2,ensmbl_df.index.get_loc('run_id')] = new_id 
              ensmbl_df.iloc[index2,0] = new_id 
              runID_list.append(new_id)     
              print("Duplicate runID <"+curr_id+"> changed to <"+new_id+">.")
            else:
              runID_list.append(curr_id)     
              
          prev_dir = curr_dir
        else:
          ensmbl_df.loc[index2,['isRemove']] = 1
          print("Null runID at <"+row2['output_dir']+">")
          dup_cnt += 1

      #print("Top 10 tempdf rows after marking")
      #print(ensmbl_df[['run_id','output_dir','isRemove','floodevent']].head(10))
           
      if (dup_cnt>0):
        print("*** "+str(dup_cnt)+" duplicates found in <"+curr_ensmbl_file+"> ***")
      print("*** "+str(ok_cnt)+" valid records in <"+rev_ensmbl_file+"> ***\n")

      if (ok_cnt<tot_rows):
        #get unused runIDs
        excessIDs_df = ensmbl_df.loc[ensmbl_df['isRemove']==1]
        excessIDs_df = excessIDs_df.sort_values(['timestamp','output_dir'], ascending=[False, True])
        excessIDs_df.reset_index(drop=True, inplace=True)
        excessIDs_df['seqno'] = excessIDs_df.index
        #print("\n excessIDs_df head")
        #print(excessIDs_df.head(5))  
        #print("Excess rows = "+str(len(excessIDs_df.index)))        
        
      #getvalid runIDs
      ensmbl_df = ensmbl_df.loc[ensmbl_df['isRemove']==0]
      ensmbl_df.reset_index(drop=True, inplace=True)  
      #print("\nensmbl_df columns")
      #print(ensmbl_df.columns)      
      
      if (ok_cnt<tot_rows):                                                      
        flood_df = pd.merge(flood_df, ensmbl_df,on='floodevent',how='left')
        flood_df = flood_df[pd.isnull(flood_df['run_id'])]
        #print("\nflood_df head")
        #print(flood_df.head(10))  
        #print("Missing rows = "+str(len(flood_df.index)))
        exc_cols = ['run_id', 'output_dir', 'flood_event_file','zone_shapefile', 'link_shapefile', 'node_shapefile','hydrograph_shapefile', 'timestamp', 'isRemove']
        
        flood_df.drop(exc_cols, axis=1, inplace=True)  
        flood_df.columns= ['newevent']        
        flood_df.reset_index(drop=True, inplace=True)  
        flood_df['seqno'] = flood_df.index  
        #print("\nflood_df head")
        #print(flood_df.head(10)) 

          
        flood_df = pd.merge(flood_df, excessIDs_df,on='seqno',how='left')
        for index3, row3 in flood_df.iterrows():
          new_event = row3['newevent']
          old_event = row3['floodevent']
          old_dir = row3['output_dir']
          old_file = row3['flood_event_file']
          flood_df.loc[index3,['output_dir']] = old_dir.replace(old_event,new_event)
          flood_df.loc[index3,['flood_event_file']] = old_file.replace(old_event,new_event)
          flood_df.loc[index3,['floodevent']] = new_event
          rno = random.randint(10,99)
          flood_df.loc[index3,['run_id']] = row3['run_id']+str(rno)
     
        flood_df.drop(['newevent', 'seqno'], axis=1, inplace=True)  
        print("\nflood_df head")
        print(flood_df.head(10))  
        #print("\nflood_df columns")
        #print(flood_df.columns)
        print("\nTail 5 of ensmbl_df before append")
        print(ensmbl_df.tail(5))
        ensmbl_df = ensmbl_df.append(flood_df, ignore_index=True)     
      
      
      ensmbl_df.drop(['isRemove'], axis=1, inplace=True)
      print("\nTail 5 of ensmbl_df")
      print(ensmbl_df.tail(5))
      #save to csv file
      ensmbl_df.to_csv(rev_ensmbl_file, index=False)
      print("Completed file <"+rev_ensmbl_file+">" ) 
              
except:
  traceback.print_exc(file=sys.stdout)
  print ("**** Error encountered. Printing traceback! ****")
  traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
  print("*** Print_exception:")
  # exc_type below is ignored on 3.5 and later
  traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)

finally:

  print( "\nExecution of <FEM2_cleanRunIDS.py> completed!")