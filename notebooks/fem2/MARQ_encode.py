import os
import sys
import subprocess

keycode = "JONMARQUEZ@PBWYCVD$FKGL#XH?STI"
keycode = keycode.lower()
digitstr = "0123456789"
coderep = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
arglen = len(sys.argv)

instr = sys.argv[1].strip()
print ("In String = <"+instr+">")
outstr = list(instr)
strlen =  len(instr)
for x in range(strlen):
  if instr[x] in digitstr:
    xval = int(instr[x])-1
    if xval<0:
      xval = 9
    if coderep[xval]<1:
      coderep[xval] += 1
      outstr[x]=keycode[xval]
    elif coderep[10+xval]<1:
      coderep[10+xval] += 1
      outstr[x]=keycode[10+xval]
    else:
      coderep[20+xval] += 1
      outstr[x]=keycode[20+xval]
  else:
      outstr[x]= instr[x]
newstr = "".join(outstr)
print ("Out STRING = "+newstr+"")