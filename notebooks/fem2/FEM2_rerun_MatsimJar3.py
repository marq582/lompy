import csv
import subprocess
import os
import sys

if ('--help' in sys.argv):
  print ("> python FEM2_rerun_MatsimJar.py [INITFILE=FEM2_master2_A0A1A2.in] [RERUNFILE=rerun_Matsim.csv] ")
  print ("Examples:")
  print ("  > python FEM2_rerun_MatsimJar.py ") 
  print ("  > python FEM2_rerun_MatsimJar.py FEM2_master3_ScenA3A.in") 
  print ("  > python FEM2_rerun_MatsimJar.py FEM2_master3_ScenA3A.in rerun_Matsim_2.csv ") 
  exit()
  
arglen = len(sys.argv)

# input tables
masterIN = "FEM2_JustMatsim3.in"
if (arglen>1) :
  masterIN = sys.argv[2]

rerunCSV = "rerun_Matsim.csv"
if (arglen>2) :
  rerunCSV = sys.argv[1]  

# Read workflow file
slurmFileMaster = open(masterIN, 'r')
slurmDataMaster = slurmFileMaster.read();
slurmFileMaster.close()

# Read XML filelist
rerunFile = open(rerunCSV, 'r')
rerunReader = csv.reader(rerunFile)
knt = 0
isFirst=True
for row in rerunReader:
    if (isFirst):
        isFirst = False
    else:
        #print(row)
        #print(row[1])
        #print(row[1].split("/output"))
        knt = knt+1
        popn_scen = row[3].strip()
        rname = row[1].split("/output")[1]
        dam_scenario = rname.split("/")[1]
        floodeventID = rname.split("/")[2]
        rname = dam_scenario+"_"+floodeventID
        #print(rname)
        run_dir = "./"+row[3].strip()+"/runs"  
        #print(run_dir)      
        
        # Create new slurm file
        slurmFileName = run_dir+"/"
        slurmFileName += 'FEM2_rerun_{0}.in'.format(rname)
        slurmData = slurmDataMaster.replace('__NAME__', rname)
        slurmData = slurmData.replace('__POPN_SCENARIO__', popn_scen)
        slurmData = slurmData.replace('__DAM_SCENARIO__', dam_scenario)
        slurmData = slurmData.replace('__RUNDIR__', run_dir)
        slurmData = slurmData.replace('__FLOOD_EVENT__', floodeventID)
            
        with open(slurmFileName, 'w') as slurmFile:
            slurmFile.write(slurmData)
            slurmFile.close()
        
        # Run job
        print(knt, "sbatch", slurmFileName)
        subprocess.call(["sbatch", slurmFileName])

print("\n *** Submitted "+str(knt)+" jobs for RERUN. ***")
