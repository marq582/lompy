# This code creates the CSV file from a Matsim network XML.
 
from lxml import etree
import pandas as pd
import sys, traceback
import os

if ('--help' in sys.argv):
  print ("> python MTSM_netXML2CSV.py [NETXML='./output_network.xml']   ")
  print ("Examples:")
  print ("  > python MTSM_netXML2CSV.py ") 
  print ("  > python MTSM_netXML2CSV.py './testing/'") 
  exit()
  
arglen = len(sys.argv)
#input files
_inputNetXML = './output_network.xml'

#output files
_outNodesCSV = 'matsim_nodes.csv'
_outLinksCSV = 'matsim_links.csv'

if (arglen>1) :
  _inputNetXML = sys.argv[1]
  

print ("Current working directory")
print(os.getcwd())
os.chdir(os.path.dirname(_inputNetXML))
print ("New working directory")
print(os.getcwd())

try:
  dfcols = ['id', 'x', 'y', 'origid']
  node_df = pd.DataFrame(columns=dfcols)
  dfcols = ['id', 'from', 'to', 'length', 'freespeed', 'capacity', 'permlanes', 'oneway', 'modes', 'origid']
  link_df = pd.DataFrame(columns=dfcols)
  
  print("\nParsing XML file <"+_inputNetXML+">. Please wait!")
  tree = etree.parse(_inputNetXML) 
  root = tree.getroot()

  knt = 0
  n_knt=0
  l_knt=0

  for i in root.getiterator():
    knt = knt+1
    if (knt % 10000 == 100):
        print (knt, " > ", i.attrib)

    p_id = i.get('id')
    p_x = i.get('x')
    p_y = i.get('y')
    p_origid = i.get('origid')
    p_from = i.get('from')
    p_to = i.get('to')
    p_len = i.get('length')
    p_fs = i.get('freespeed')
    p_cap = i.get('capacity')
    p_lanes = i.get('permlanes')
    p_way = i.get('oneway')
    p_modes = i.get('modes')
 
    if (p_x is not None): 
      n_knt = n_knt+1
      newrec = pd.DataFrame({'id':[p_id], 'x':[p_x], 'y':[p_y], 'origid':[p_origid]})
      #print (newrec)
      node_df = node_df.append(newrec)

    if (p_from is not None):
      l_knt = l_knt+1
      newrec = pd.DataFrame({'id':[p_id], 'from':[p_from], 'to':[p_to], 'length':[p_len], 'freespeed':[p_fs], 'capacity':[p_cap], 'permlanes':[p_lanes], 'oneway':[p_way], 'modes':[p_modes], 'origid':[p_origid]})     
      #print (newrec)
      link_df = link_df.append(newrec)

  #write node table in CSV 
  print("\nShape of node_df")
  print(node_df.shape)
  print(node_df.tail(10))
  node_df.to_csv(_outNodesCSV, index=False)
  print("\nWriting of CSV table <"+_outNodesCSV+"> completed!")
 
  #write link table in CSV 
  print("\nShape of link_df")
  print(link_df.shape)
  print(link_df.tail(10))
  link_df.to_csv(_outLinksCSV, index=False)
  print("\nWriting of CSV table <"+_outLinksCSV+"> completed!")
  
except: 
  traceback.print_exc(file=sys.stdout) 
  print ("**** Error encountered. Printing traceback! ****")
  traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
  print("*** Print_exception:")
  # exc_type below is ignored on 3.5 and later
  traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=2, file=sys.stdout)
                              
finally:                                               
  print( "\nExecution of <MTSM_netXML2CSV> completed!")
                              