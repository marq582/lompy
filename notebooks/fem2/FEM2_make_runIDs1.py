import csv
import os
import sys
import subprocess
import xml.etree.ElementTree as ET

if ('--help' in sys.argv):
  print ("> python FEM2_make_runIDs.py [HYDROGRAPHS_DIR]  ")
  print ("default HYDROGRAPHS_DIR  = Hydrographs_Current")

  exit()
  
arglen = len(sys.argv)
optno = 1
hydro_dir = "Hydrographs_Current"

if (arglen>1) :
  hydro_dir = sys.argv[1]


# input tables
damscenCSV = "FEM2_dam_scenarios.csv"
floodscenCSV = "FEM2_flood_events.csv" 
popnscenCSV = "FEM2_popn_scenarios.csv"

knt = 0

curr_dir = os.getcwd()
print("curr_dir <"+curr_dir+">")

hydro_dir = curr_dir +"/"+hydro_dir
if not os.path.exists(hydro_dir):
  print ("\n **** Hydrograph directory <"+hydro_dir+"> does not exist. Program will terminate.")  
  print ("COMMAND SYNTAX: python FEM2_make_config.py [HYDROGRAPHS_DIR]  ")
  print ("default HYDROGRAPHS_DIR  = Hydrographs_Current \n")
  exit() 




with open(popnscenCSV, 'r') as  popncsvReader:
  for popn_row in popncsvReader:
  
    #create popn runs directory
    popn_scen = popn_row.strip()
    if (len(popn_scen)>0) :

     
      #Read project file template
      projTemplateName = 'FEM2_project_{0}.xml'.format(popn_scen)
      print("projTemplateName <"+projTemplateName+">")

      tree = ET.parse(projTemplateName)
      root = tree.getroot() 
                 
      print('\nAll globalnames:')
      for opertn in root.iter('input'):
        x_atrib = opertn.attrib
        x_text = opertn.text
        print (x_atrib)
        print (x_text)
    
      gn_item = root.find("./input[@globalname='Road network shapefile']")
      net_shpfl = gn_item.text
      gn_item = root.find("./input[@globalname='Sub-sector shapefile']")
      sub_shpfl = gn_item.text
      gn_item = root.find("./input[@globalname='Evacuation node shapefile']")
      nde_shpfl = gn_item.text
      gn_item = root.find("./input[@globalname='Output directory']")
      out_dir = gn_item.text
          
print ("FEM2_make_config.py completed.")
