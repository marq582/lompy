import csv
import subprocess

# Read workflow file
batchFileMaster = open('FEM2_master_A2.xml', 'r')
batchDataMaster = batchFileMaster.read();
batchFileMaster.close()

# Read workflow file
slurmFileMaster = open('FEM2_master.in', 'r')
slurmDataMaster = slurmFileMaster.read();
slurmFileMaster.close()

# Read stations summaries
csvFile = open('FEM2_flood_events.csv', 'r')
csvReader = csv.reader(csvFile)

scenarios = ['D_14m', 'D_20m', 'Exg', 'FSL-5']

for row in csvReader:

    for scenario in scenarios:

        # Create name
        gaugeID = row[0]
        name = '{0}_{1}'.format(scenario, gaugeID)
        print(name)

        # Create new batch file
        batchFileName = 'FEM2_run_{0}.xml'.format(name)
        batchData = batchDataMaster.replace('__SCENARIO__', scenario)
        batchData = batchData.replace('__GAUGE_FILE__', gaugeID + '.csv')
            
        with open(batchFileName, 'w') as batchFile:
            batchFile.write(batchData)
            batchFile.close()

        # Create new slurm file
        slurmFileName = 'FEM2_run_{0}.in'.format(name)
        slurmData = slurmDataMaster.replace('__NAME__', name)
            
        with open(slurmFileName, 'w') as slurmFile:
            slurmFile.write(slurmData)
            slurmFile.close()
    
        # Run job
        subprocess.call(["sbatch", slurmFileName])
