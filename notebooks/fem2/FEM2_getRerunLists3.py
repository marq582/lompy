import csv
import sys, traceback
import os
import gc

if ('--help' in sys.argv):
  print ("> python FEM2_getRerunLists.py [dirPrefix='./']   ")
  print ("Examples:")
  print ("  > python FEM2_getRerunLists.py ") 
  print ("  > python FEM2_getRerunLists.py './testing/'") 
  exit()
  
arglen = len(sys.argv)
_prefix = './'
if (arglen>1) :
  _prefix = sys.argv[1]    
  
print ("Current working directory: "+os.getcwd())

# input tables
arrCSV = "arrdep_found.csv"
eventsgzCSV = "eventsgz_found.csv"
plansgzCSV = "plansgz_found.csv"
#damscenCSV = "FEM2_dam_scenarios - Copy.csv"
#floodscenCSV = "FEM2_flood_events - Copy.csv" 
#popnscenCSV = "FEM2_popn_scenarios - Copy.csv"
damscenCSV = "FEM2_dam_scenarios.csv"
floodscenCSV = "FEM2_flood_events.csv" 
popnscenCSV = "FEM2_popn_scenarios.csv"

# output tables
bypassCSV = "rerun_bypass.csv"
matsimCSV = "rerun_Matsim.csv"
cleanupCSV = "rerun_cleanup.csv"

# Write rerun list
rowrec = ['seqno', ' outputPath', ' projFileName', ' popnScen']
bypassFile = open(bypassCSV, 'w')
bypassWriter = csv.writer(bypassFile)
bypassWriter.writerow(rowrec)

matsimFile = open(matsimCSV, 'w')
matsimWriter = csv.writer(matsimFile)
matsimWriter.writerow(rowrec)

cleanupFile = open(cleanupCSV, 'w')
cleanupWriter = csv.writer(cleanupFile)
cleanupWriter.writerow(rowrec)

try:
  #create completed list
  print("\nCreate completed list")
  arr_lst = []
  isFirst = True
  arr_cnt = 0
  with open(arrCSV, 'r') as  arrcsvReader:
    for arr_row in arrcsvReader:      
      if (isFirst): 
        isFirst = False
      elif (len(arr_row.strip())>0):
        #print(arr_row)
        arr_scen = arr_row.rsplit(",")[1]
        arr_scen = arr_scen.strip()
        #print(arr_scen)
        arr_lst.append(arr_scen)
        arr_cnt += 1

  print("\n"+str(arr_cnt)+" completed runs recorded.")
  #print(arr_lst[0],arr_lst[1], arr_lst[2])
  
  print("\nCreate bypass list")
  #events_lst = []
  bypass_lst = []
  event_cnt = 0
  isFirst = True
  with open(eventsgzCSV, 'r') as  evtcsvReader:
    for evt_row in evtcsvReader:
      if (isFirst): 
        isFirst = False
      elif (len(evt_row.strip())>0):
        evt_scen = evt_row.rsplit(",")[1]
        evt_scen = evt_scen.strip()
        #events_lst.append(evt_scen)
        event_cnt += 1
        if (evt_scen not in arr_lst):
          bypass_lst.append(evt_scen)
          #print(evt_scen)

  print("\nCreate clean up list")
  cleanup_lst = []
  isFirst = True
  with open(plansgzCSV, 'r') as  cleanupcsvReader:
    for cleanup_row in cleanupcsvReader:
      if (isFirst): 
        isFirst = False
      elif (len(cleanup_row.strip())>0):
        cleanup_scen = cleanup_row.rsplit(",")[1]
        cleanup_scen = cleanup_scen.strip()
        if (cleanup_scen in arr_lst):
          cleanup_lst.append(cleanup_scen)

  print("\n"+str(event_cnt)+" Matsim events recorded.")
  #print(bypass_lst[0],bypass_lst[1], bypass_lst[2])
   
  bypass_cnt = 0
  matsim_cnt = 0
  cleanup_cnt = 0
  print("\nCreate rerun Matsim list")  
  with open(popnscenCSV, 'r') as  popncsvReader:
    for popn_row in popncsvReader: 
      #create popn runs directory
      popn_scen = popn_row.strip()
      with open(damscenCSV, 'r') as damcsvReader:
          for dam_row in damcsvReader:
              dam_scen = dam_row.strip()
              with open(floodscenCSV, 'r') as  floodcsvReader:
                for flood_row in floodcsvReader:
                  flood_scen = flood_row.strip()
                  outpath = _prefix+popn_scen+"/output/"+dam_scen+"/"+flood_scen+"/output"
                  projfile = " FEM2_run_"+dam_scen+"_"+flood_scen+".xml" 
                  #print(outpath)
                  if (outpath not in arr_lst):
                    if (outpath in bypass_lst):
                      bypass_cnt += 1
                      rowrec = [str(bypass_cnt), " "+outpath, projfile, " "+popn_scen]
                      bypassWriter.writerow(rowrec)
                    else:
                      matsim_cnt += 1
                      rowrec = [str(matsim_cnt), " "+outpath, projfile, " "+popn_scen]
                      matsimWriter.writerow(rowrec)
                  elif (outpath in cleanup_lst):
                    cleanup_cnt += 1
                    rowrec = [str(cleanup_cnt), " "+outpath, " output_plans.xml.gz", " "+popn_scen]
                    cleanupWriter.writerow(rowrec)
                  
  print(str(bypass_cnt)+" bypass files recorded in <"+bypassCSV+">.")
  print(str(matsim_cnt)+" rerun Matsim files recorded in <"+matsimCSV+">.")
  print(str(cleanup_cnt)+" clean up directories recorded in <"+cleanupCSV+">.")
 
except:
  traceback.print_exc(file=sys.stdout)
  print ("**** Error encountered. Printing traceback! ****")
  traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
  print("*** Print_exception:")
  # exc_type below is ignored on 3.5 and later
  traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)

finally:
  bypassFile.close()
  matsimFile.close()
  cleanupFile.close()
  print( "\nExecution of <FEM2_getRerunLists> completed!")