# This code creates the CSV file from a Matsim network XML.
 
from lxml import etree
import pandas as pd
import sys, traceback
import os

if ('--help' in sys.argv):
  print ("> python MTSM_planCSV2tripXML.py [PLANCSV='./plan.csv']   ")
  print ("Examples:")
  print ("  > python MTSM_planCSV2tripXML.py ") 
  print ("  > python MTSM_planCSV2tripXML.py '../out/testing.csv'") 
  exit()
  
arglen = len(sys.argv)
#input files
_inputlinkCSV = './matsim_links.csv'
_inputplanCSV = './matsim_plans.csv'

#output files
_outTripXML = 'sumo_trip2.xml'

if (arglen>1) :
  _inputplanCSV = sys.argv[1]
  

print ("Current working directory")
print(os.getcwd())
os.chdir(os.path.dirname(_inputplanCSV))
print ("New working directory")
print(os.getcwd())

try:
  print("\nCreate link ID list from <"+_inputlinkCSV+">.")
  link_lst = []
  isFirst = True
  lnk_knt = 0
  with open(_inputlinkCSV, 'r') as  linkcsvReader:
    for link_row in linkcsvReader:
      if (isFirst): 
        isFirst = False
      elif (len(link_row.strip())>0):
        linkID = link_row.rsplit(",")[0]
        linkID = linkID.strip()
        link_lst.append(linkID)
        lnk_knt += 1
        
  print("\n"+str(lnk_knt)+" link iDs recorded.")
  print(link_lst[0],link_lst[1], link_lst[2])
            
  print("\n Opening output XML file")
  fo = open(_outTripXML, "w")

  # Write sequence of lines at the start of file.
  msg = "<?xml version=\"1.0\"?>"
  res = fo.writelines( msg )
  msg = "\n<!--- SUMO Trip XML file generated from Matsim plan CSV -->"
  res = fo.writelines( msg)


  msg = "\n<!-- ================================================== -->\n"
  msg += "\n  <trips>"
  res = fo.writelines(msg)  

  print("\nReading Node CSV file <"+_inputplanCSV+">. Please wait!")
  trip_df = pd.read_csv(_inputplanCSV)
  print(trip_df.tail(5))
  row_knt = 0
  splitter = "/---/"
  knt = 0
  for row in trip_df.itertuples():
    row_knt += 1
    sub_knt = 0
    p_vehcnt = getattr(row, 'VEHICLES')
    p_depart = str(getattr(row, 'TIME'))
    p_route = str(getattr(row, 'NETWORKROUTEASSTRING'))
    p_depart = "150"
    p_vehcnt = 30
    
    #find src and dest edges from p_route
    currte = p_route
    #print("\n ***currte: "+currte)
    isNoDst = True
    scrID = "XXX"
    dstID = currte
    while (splitter in currte):
      edge1 = currte.rsplit(splitter,1)[1]
      currte = currte.rsplit(splitter,1)[0]

      if (edge1 in link_lst):
        if (isNoDst): 
          dstID = edge1
          isNoDst = False
        scrID = edge1

    if (currte in link_lst):
      scrID = currte
    for sub_knt in range (0, p_vehcnt):
      msg =  "\n    <trip id=\""+str(row_knt)+"_"+str(sub_knt)+"\""
      msg += " depart=\""+p_depart+"\""
      msg += " from=\""+scrID+"\""
      msg += " to=\""+dstID+"\""
      msg += " />"
      #print(msg)
      res = fo.writelines(msg)
      knt += 1
      if (knt % 1000 == 5):
        print(msg)

  msg = "\n  </trips>"    
  res = fo.writelines(msg)  
  print("\nWriting of XML file <"+_outTripXML+"> completed!")     
  
except: 
  traceback.print_exc(file=sys.stdout) 
  print ("**** Error encountered. Printing traceback! ****")
  traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
  print("*** Print_exception:")
  # exc_type below is ignored on 3.5 and later
  traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=2, file=sys.stdout)
  
finally:

  # Close opened file
  fo.close()
  print( "\nExecution of <MTSM_planCSV2tripXML.py> completed!")
