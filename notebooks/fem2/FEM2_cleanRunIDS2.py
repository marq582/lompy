import pandas as pd
import csv
import sys, traceback
import os
import gc

if ('--help' in sys.argv):
  print ("> python FEM2_cleanRunIDs.py [RUNIDFILE='runIDs_found.csv']   ")
  print ("Examples:")
  print ("  > python FEM2_cleanRunIDs.py ") 
  print ("  > python FEM2_cleanRunIDs.py 'another.csv'") 
  print ("  > ~/Desktop/Delivery/opt/csiro.au/evacuation_modelling_fem2_batch/bin/app_FEM2_batch --launch python3.6 FEM2_cleanRunIDS1.py")
  exit()

arglen = len(sys.argv)

# input tables
runidCSV = "runIDs_found.csv"  
if (arglen>1) :
  runidCSV = sys.argv[1]    
  
print ("Current working directory: "+os.getcwd())

# output tables
cleanCSV = "ensemble_runs_attribs_rev.csv"

try:
  runidReader = pd.read_csv(runidCSV)
  print("\nInitial Shape of User_file")
  print(runidReader.dtypes)
  print(runidReader)
  
  runID_list=[]

  #create ensemble summary table
  for index, row in runidReader.iterrows():
      pathnm = row[1].strip()
      flname = row[2].strip()
      curr_ensmbl_file =  pathnm+"/"+flname
      rev_ensmbl_file =  pathnm+"/"+cleanCSV

      ensmbl_df = pd.read_csv(curr_ensmbl_file, na_filter=False)
      col_hdrs = ensmbl_df.columns
      #ensmbl_df = ensmbl_df.loc[ensmbl_df['run_id']!=""]
      if ('timestamp' not in col_hdrs):
        print("Missing timestamp header in  <"+curr_ensmbl_file+"> .")
        col_hdrs.append('timestamp')
        ensmbl_df.columns = col_hdrs
        
      ensmbl_df['run_id'] = ensmbl_df['run_id'].astype(str)
      ensmbl_df['isRemove'] = 0
      
      ensmbl_df = ensmbl_df.sort_values(['output_dir', 'timestamp'], ascending=[True, False])
      #print(ensmbl_df)
      
      prev_dir = ""
      dup_cnt = 0
      ok_cnt = 0
      #mark duplicate output dirs
      for index2, row2 in ensmbl_df.iterrows():
        curr_id = row2['run_id'].strip()
        if (len(curr_id)>0):
          curr_dir = row2['output_dir'].strip()
          if (prev_dir == curr_dir):
            ensmbl_df.ix[index2,'isRemove'] = 1    
            dup_cnt += 1
          else:
            ok_cnt += 1
            if (curr_id in runID_list):
              rno = random.randint(10,99)
              new_id = curr_id+str(rno)
              ensmbl_df.ix[index2,'run_id'] = new_id 
              runID_list.append(new_id)     
              print("Duplicate runID <"+curr_id+"> changed to <"+new_id+">.")
            else:
              runID_list.append(curr_id)     
              
          prev_dir = curr_dir
        else:
          ensmbl_df.ix[index2,'isRemove'] = 1
          print("Null runID at <"+row2['output_dir']+">")
          dup_cnt += 1
           
      if (dup_cnt>0):
        print("*** "+str(dup_cnt)+" duplicates found in <"+curr_ensmbl_file+"> ***")
      print("*** "+str(ok_cnt)+" valid records in <"+rev_ensmbl_file+"> ***\n")

      ensmbl_df = ensmbl_df.loc[ensmbl_df['isRemove']==0]
      ensmbl_df.drop(['isRemove'], axis=1, inplace=True)
      ensmbl_df.to_csv(rev_ensmbl_file, index=False)
               
except:
  traceback.print_exc(file=sys.stdout)
  print ("**** Error encountered. Printing traceback! ****")
  traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
  print("*** Print_exception:")
  # exc_type below is ignored on 3.5 and later
  traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)

finally:

  print( "\nExecution of <FEM2_cleanRunIDS1.py> completed!")