#!/bin/bash

#be carefull with deletions
#minimum output - only specific files retained
cd ../
cp -f input_population_attrs.txt input_population_attrs.log
rm *.xml
rm *.txt
rm *.csv
rm *.gz
mv -f input_population_attrs.log input_population_attrs.txt
#save desired outputs
mkdir --parents output2
cp ./output/*.csv ./output2
cp ./output/output_events.xml.gz ./output2
cp ./output/output_network.xml.gz ./output2
rm -rf ./output
mv -f output2 output

