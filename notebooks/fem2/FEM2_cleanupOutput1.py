import csv
import sys, traceback
import os
import gc

if ('--help' in sys.argv):
  print ("FEM2_cleanupOutput.py [CLEANUPFILE='rerun_cleanup.csv']")
  print ("Examples:") 
  print ("  > python FEM2_cleanupOutput.py ") 
  print ("  > python FEM2_cleanupOutput.py somesrcfile.csv  ") 
  exit()
  
arglen = len(sys.argv)
srcFile="rerun_cleanup.csv"
if (arglen>1) :
  srcFile = sys.argv[1]

cur_dir =  os.getcwd()
print ("Current working directory: "+os.getcwd())
print("\Reading clean up list from file <"+srcFile+">")
cleanup_lst = []
isFirst = True
knt=0
with open(srcFile, 'r') as  cleanupcsvReader:
  for cleanup_row in cleanupcsvReader:
    if (isFirst): 
      isFirst = False
    elif (len(cleanup_row.strip())>0):
      cleanup_dir = cleanup_row.rsplit(",")[1]
      cleanup_dir = cleanup_dir.strip()
      if (os.path.exists(cleanup_dir)):
        knt = knt+1
        print("Cleaning up <"+cleanup_dir+">")
        os.chdir(cleanup_dir)
        os.chdir("../")
        print ("Current working directory: "+os.getcwd())
        os.system("cp -f input_population_attrs.txt input_population_attrs.log")
        os.system("rm *.xml")
        os.system("rm *.txt")
        os.system("rm *.csv")
        os.system("rm *.gz")
        os.system("mv -f input_population_attrs.log input_population_attrs.txt")
        #save desired outputs"
        os.system("mkdir --parents output2")
        os.system("cp ./output/*.csv ./output2")
        os.system("cp ./output/output_events.xml.gz ./output2")
        os.system("cp ./output/output_network.xml.gz ./output2")
        os.system("rm -rf ./output")
        os.system("mv -f output2 output")
        os.chdir(cur_dir)
        print ("AT end Current working directory: "+os.getcwd())
          
print(str(knt)+" directories were cleaned up.")

print( "\nExecution of <FEM2_cleanupOutput> completed!")
                