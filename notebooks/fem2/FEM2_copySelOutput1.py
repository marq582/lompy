import os
import sys
import subprocess

if ('--help' in sys.argv):
  print ("FEM2_copySelOutput.py [SRCDIR='.'] [DESTDIR='_untitled'] [COPYLIMIT=none]")
  print ("Examples:") 
  print ("  > python FEM2_copySelOutput.py ") 
  print ("  > python FEM2_copySelOutput.py A0  ") 
  print ("  > python FEM2_copySelOutput.py A1  A1_copy") 
  print ("  > python FEM2_copySelOutput.py olddir  newdir  100") 
 
  exit()
  
arglen = len(sys.argv)
srcDir = "."
destDir = "_untitled"
selFiles = ['area_evactrips.csv', 'onerunXareasum.csv', 'onerunXlinksum.csv', 'volcapratio_per_interval.csv']
if (arglen>1) :
  srcDir = sys.argv[1]
if (arglen>2) :
  destDir = sys.argv[2]
copyLimit = -1
if (arglen>3) :
  copyLimit = int(sys.argv[3])

for selfile in selFiles:
  print ("Copying <"+selfile+"> from <"+srcDir+"> to <"+destDir+">")
  knt = 0
  for (path, dirs, files) in os.walk(srcDir):
    if selfile in files: # compares to your specified files
      knt = knt+1
      newpath = path.replace(srcDir+"/", destDir+"/")
      mdcommand = "mkdir -p '"+newpath+"'"
      print (mdcommand)
      os.system(mdcommand)
      cpcommand = "cp -rf '"+path+"/"+selfile+ "' '"+newpath+"'"   
      print (cpcommand)
      os.system(cpcommand)
      if copyLimit > 0:
        if knt >= copyLimit:
          break
print ("\n")
                