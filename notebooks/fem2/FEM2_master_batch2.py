import pandas as pd
import csv
import os
import sys
import subprocess

if ('--help' in sys.argv):
  print ("> python FEM2_master_batch.py [OPTIONNO]   ")
  print ("OPTIONNO = '' : Calls  [~/.../app_FEM2_batch  [projectXML] --removeDB] with [minimum_output]")
  print ("OPTIONNO = 1 : Calls  [~/.../app_FEM2_batch  [projectXML] --removeDB] with [minimum_output]")
  print ("OPTIONNO = 2 : Calls  [~/.../app_FEM2_batch  [projectXML] --removeDB] with [default_output]")
  print ("OPTIONNO = 3 : Calls  [~/.../app_FEM2_batch  [projectXML] ] with [minimum_output]")
  print ("OPTIONNO = 4 : Calls  [~/.../app_FEM2_batch  [projectXML] ] with [default_output]")
  print ("Examples:")
  print ("  > python FEM2_reruns_batch.py 1 bypassErrfile.txt") 
  print ("  > python FEM2_reruns_batch.py 4 rerunErrfile.txt") 
  exit()
  
arglen = len(sys.argv)
optno = 1
if (arglen>1) :
  optno = int(sys.argv[1])

#output options
default_output = "#default output - no deletions except database"
minimum_output = "#minimum output - only specific files retained"
minimum_output += "\ncp -f input_population_attrs.txt input_population_attrs.log"
minimum_output += "\nrm *.xml"
minimum_output += "\nrm *.txt"
minimum_output += "\nrm *.csv"
minimum_output += "\nrm *.gz"
minimum_output += "\nmv -f input_population_attrs.log input_population_attrs.txt"
minimum_output += "\n#save desired outputs"
minimum_output += "\nmkdir --parents output2"
minimum_output += "\ncp ./output/*.csv ./output2"
minimum_output += "\ncp ./output/output_events.xml.gz ./output2"
minimum_output += "\ncp ./output/output_network.xml.gz ./output2"
minimum_output += "\nrm -rf ./output"
minimum_output += "\nmv -f output2 output"  
  
  
_options = '--removeDB'
_output = minimum_output
if (optno==2):
  _output = default_output
if (optno==3):
  _options = ''
if (optno==4) :
  _options = ''
  _output = default_output
  
# input tables
damscenCSV = "FEM2_dam_scenarios.csv"
floodscenCSV = "FEM2_flood_events.csv" 
popnscenCSV = "FEM2_popn_scenarios.csv"
masterIN = "FEM2_master3.in"

# output tables
projCSV = "projXMLList.csv"
  
# Read workflow file
slurmFileMaster = open(masterIN, 'r')
slurmDataMaster = slurmFileMaster.read();
slurmFileMaster.close()

# Write project XML file list
projXMLFile = open(projCSV, 'w')
projWriter = csv.writer(projXMLFile)
projrow = ['seqno', ' outputPath', ' projFileName', ' popnScen']
projWriter.writerow(projrow)
projknt = 0

knt = 0

with open(popnscenCSV, 'r') as  popncsvReader:
  for popn_row in popncsvReader:
  
    #create popn runs directory
    popn_scen = popn_row.strip()
    run_dir = "./"+popn_scen+"/runs"
    #print("Rundir <"+run_dir+">")
    
    if not os.path.exists(popn_scen):
      os.makedirs(popn_scen)   
    if not os.path.exists(run_dir):
      os.makedirs(run_dir)   
   
    #Read project file template
    projTemplateName = 'FEM2_project_{0}.xml'.format(popn_scen)
    projFileMaster = open(projTemplateName, 'r')
    projDataMaster = projFileMaster.read();
    projFileMaster.close()

    with open(damscenCSV, 'r') as damcsvReader:
      for dam_row in damcsvReader:
        dam_scenario = dam_row.strip()
      
        with open(floodscenCSV, 'r') as  floodcsvReader:
          for flood_row in floodcsvReader:
            floodeventID = flood_row.strip()
            # Create name
            name = '{0}_{1}'.format(dam_scenario, floodeventID)
            #print(name)

            # Create new project file
            xmlFile = 'FEM2_run_{0}.xml'.format(name)
            projFileName = run_dir+"/"+xmlFile
            projData = projDataMaster.replace('__DAM_SCENARIO__', dam_scenario)
            projData = projData.replace('__FLOOD_EVENT__', floodeventID + '.csv')
            
            with open(projFileName, 'w') as projFile:
              projFile.write(projData)
              projFile.close()
            projknt += 1
            projrow = [str(projknt)]
            projrow.append(" ./"+popn_scen+"/output/"+dam_scenario+"/"+floodeventID+"/output")
            projrow.append(" "+xmlFile)
            projrow.append(" "+popn_scen)
            projWriter.writerow(projrow)
    
            # Create new slurm file
            slurmFileName = run_dir+"/"
            slurmFileName += 'FEM2_run_{0}.in'.format(name)
            slurmData = slurmDataMaster.replace('__NAME__', name)
            slurmData = slurmData.replace('__DAM_SCENARIO__', dam_scenario)
            slurmData = slurmData.replace('__RUNDIR__', run_dir)
            slurmData = slurmData.replace('__FLOOD_EVENT__', floodeventID)
            slurmData = slurmData.replace('__CLEAN_UP__', _output)
            slurmData = slurmData.replace('__RUN_OPTIONS__', _options)
            
            with open(slurmFileName, 'w') as slurmFile:
              slurmFile.write(slurmData)
              slurmFile.close()
    
            # Run job
            knt += 1
            print(knt, "sbatch", slurmFileName)
            #subprocess.call(["sbatch", slurmFileName])

projXMLFile.close()            
print ("FEM2_master_batch completed.")
