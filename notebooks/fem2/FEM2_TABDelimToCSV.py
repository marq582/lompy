import os
import sys
import subprocess

if ('--help' in sys.argv):
  print ("> python FEM2_master_batch.py [OPTIONNO] [POPNSCENFILE] [DAMSCENFILE] [FLOODSCENFILE]  ")
  print ("Examples:")
  print ("  > python FEM2_master_batch.py") 
  print ("  > python FEM2_master_batch.py 1 FEM2_popn_scenarios_1.csv FEM2_dam_scenarios_1.csv FEM2_flood_events_1.csv") 
  print ("  > python FEM2_master_batch.py 2 FEM2_popn_scenarios_1.csv") 
  print ("  > python FEM2_master_batch.py 3python FEM2_master_batch.py 3") 
  exit()

arglen = len(sys.argv)
optno = 1
SRCstr = "\t"
REPstr = ","
srcDir = "."
selFiles = ['input_population_attrs.txt', 'output_matsim_journeys.txt']

print(" Sourcedir in  <"+srcDir+"> ")
print(" SRCstr in  <"+SRCstr+"> ")
print(" REPstr in  <"+REPstr+"> ")


for path, dirs, files in os.walk(os.path.abspath(srcDir)):
    #if name in files: # compares to your specified files

    for name in files:      
      #print(" NAME in  <"+name+"> ")

      #if name.endswith('.xml'): 
      #if name == "configMATSim.xml": 
      if name in selFiles: 

            filepath = os.path.join(path, name)
            print ("*** FILEPATH in  <"+filepath+"> ")

            with open(filepath) as f: 

                s = f.read() 

            s = s.replace(SRCstr, REPstr) 
            newfilepath = filepath+".csv"
            with open(newfilepath, "w") as f2:

                f2.write(s) 