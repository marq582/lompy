#!/usr/bin/env python
# coding: utf-8

# In[15]:


# This code creates the CSV file input_population_attrs.csv from the output_trips.csv.
# This code creates the personXarea table in the database from the output file <input_population_attrs.csv>.
 
import sqlite3
import pandas as pd
#import sys, traceback
import os
import gc


# In[16]:


#dest_src = "D:\\EMV\\scs-aireys-ensemble-2020-10-01\\surf-coast-shire_2\\aireys\\"
#est_src = "D:\\EMV\\scs-aireys-ensemble-2020-10-01\\surf-coast-shire_2\\msg1\\"
dest_src = "D:/EMV/scs-aireys-ensemble-2020-10-01/surf-coast-shire_2/no_msg/"  

print ("Current working directory")
print(os.getcwd())
os.chdir(os.path.dirname(dest_src))
print ("New working directory")
print(os.getcwd())

# output tables
gridAttrsTbl = "input_AllGrids"

cell_wid = 500
base_X = 630000
base_Y = 5600000

min_X = 709576
max_X = 841751

min_Y = 5703854
max_Y = 5847889


# In[20]:


print ("*** Create grids***")
df_cols = ["gridID","rowno","colNo","ctr_X","ctr_Y", "WKT"]
grid_df = pd.DataFrame(columns = df_cols)

knt = 0
#try:
#for iy in range(min_Y, min_Y+10000, cell_wid):
for iy in range(min_Y, max_Y+cell_wid, cell_wid):
    #for ix in range(min_X, min_X+10000, cell_wid):
    for ix in range(min_X, max_X+cell_wid, cell_wid):
        colNo = int (( ix - base_X) / cell_wid )
        rowNo = int (( iy - base_Y) / cell_wid )
        gridID = str(rowNo)+"-"+str(colNo)
        ctr_X = (colNo + 0.5) * cell_wid + base_X
        ctr_Y = (rowNo + 0.5) * cell_wid + base_Y
        wkt = "POLYGON (("
        wkt += str((colNo) * cell_wid + base_X)  + " " + str((rowNo) * cell_wid + base_Y)
        wkt += ", " + str((colNo+1) * cell_wid + base_X)  + " " + str((rowNo) * cell_wid + base_Y)
        wkt += ", " + str((colNo+1) * cell_wid + base_X)  + " " + str((rowNo+1) * cell_wid + base_Y)
        wkt += ", " + str((colNo) * cell_wid + base_X)  + " " + str((rowNo+1) * cell_wid + base_Y)
        wkt += ", " + str((colNo) * cell_wid + base_X)  + " " + str((rowNo) * cell_wid + base_Y)
        wkt += "))"
        grid_df = grid_df.append(pd.Series([gridID, rowNo, colNo, ctr_X, ctr_Y, wkt], index=df_cols), ignore_index=True)
        knt += 1
        if (knt % 5000 == 5):        
            print(knt," : ",gridID, ": ", wkt) 

print("Shape of grid_df");  print(grid_df.shape);  print(grid_df.head(5)); print(grid_df.tail(5))


# In[21]:


#write table in CSV 
summTbl_csv = gridAttrsTbl + ".csv"
grid_df.to_csv(summTbl_csv, index=False)
print("\nWriting of CSV table <"+summTbl_csv+"> completed!")


# In[22]:


#clean up
del [[grid_df]] 
gc.collect()
grid_df = pd.DataFrame()

print( "\nExecution of <make_grids_CSV> completed.")


# In[ ]:




