# Main_areaXsum_df

import pandas as pd
import sys, traceback
import os
import gc

try:
  Ensmblfile = pd.DataFrame()
  area_df = pd.DataFrame()
  link_df = pd.DataFrame()
  vcr_df = pd.DataFrame()

  inputFilePath = df1.iloc[0,0]
  olddir = df2.iloc[0,0]
  newdir = df3.iloc[0,0]
    
  #inputFilePath = "C:/fem2/Delivery/ScenarioRuns-1.csv"
  #olddir = "---"
  #newdir = "---"
  
  #inputFilePath = inputFilePath.replace("\\", "/")
  olddir = olddir.strip()               
  newdir = newdir.strip()

  print("inputFilePath = <" + inputFilePath + ">")
  print("olddir = <" + olddir + ">")
  print("newdir = <" + newdir + ">")
      
  User_file = pd.read_csv(inputFilePath)
  print("\nInitial Shape of User_file")
  print(User_file.dtypes)
  print(User_file.tail(5))

  isDirReplace = ((len(olddir)>0) and (len(newdir)>0) and (olddir != newdir))
  
  #create ensemble summary table
  isfirst_ensmbl = True
  for index, row in User_file.iterrows():
      curr_ensmbl_file = row['Output_Dir']
      curr_scen_name = "----"
      #curr_scen_name = row['Scen_Name']
      curr_scen_name = row[1]
      #print("Index:",curr_scen_name,curr_ensmbl_file)
      if (isDirReplace):
          curr_ensmbl_file = curr_ensmbl_file.replace(olddir,newdir)

      Ensmblfile = pd.read_csv(curr_ensmbl_file)
      Ensmblfile['Scen_Name'] = curr_scen_name
      #print("\nRead of Ensmblfile")
      #print(Ensmblfile.dtypes)
      #print(Ensmblfile.tail(5))

      if isfirst_ensmbl:
          Ensmbl_file = Ensmblfile
          isfirst_ensmbl = False
      else:
          Ensmbl_file = Ensmbl_file.append(Ensmblfile, ignore_index=True)

  Ensmbl_file['Flood_Events']= "---"
  Ensmbl_file['Dam']= "---"

  print("\nInitial Shape of Ensmbl_file")
  print(Ensmbl_file.dtypes)
  print(Ensmbl_file.tail(5))  

  for index_ef, row_ef in Ensmbl_file.iterrows():
      outdir = row_ef['output_dir']
      if (isDirReplace):
        outdir = outdir.replace(olddir, newdir) 
      outdir = outdir.replace("\\","/")
      row_ef['output_dir'] = outdir
      
      fefile = row_ef['flood_event_file']
      fefile = fefile.replace("\\","/")
      if "Existing_91_2019" in fefile:
        row_ef['Dam'] = "Existing_91_2019"                  
      else:
        row_ef['Dam'] = "Existing_46_2015"

      print("fefile:<"+fefile+">, row_ef['Dam']:<"+row_ef['Dam']+">") 
      ss1 = fefile.rsplit("/",1)[1]
      row_ef['Flood_Events'] = ss1.rsplit(".csv",1)[0]
    
#obtain population year
  Ensmbl_file['Popn_year']= "---"
  for index_ef_1, row_ef_1 in Ensmbl_file.iterrows():
    Popnfile = row_ef_1['zone_shapefile']
    if "2026.shp" in Popnfile:
        row_ef_1['Popn_year'] = 2026
    else:
        row_ef_1['Popn_year'] = 2016
  Ensmbl_file['Popn_year'].astype(int)
  
  print("\Final Shape of Ensmbl_file")
  print(Ensmbl_file.dtypes)
  print(Ensmbl_file.tail(5))
 
  #create area summary table
  isfirst = True
  for index_ef, row_ef in Ensmbl_file.iterrows():
      curr_run_id = row_ef['run_id']
      curr_dir = row_ef['output_dir']
      area_file = curr_dir +'/output/onerunXareasum.csv'
      #print(area_file)
      area_df = pd.read_csv(area_file)
      area_df['run_id'] = curr_run_id

      if isfirst:
              Main_areaXsum_df = area_df
              isfirst = False
      else:
              Main_areaXsum_df = Main_areaXsum_df.append(area_df, ignore_index=True)

  Main_areaXsum_df["Num_fail"].fillna(0, inplace = True)
  print("\nShape of Main_areaXsum_df")
  print(Main_areaXsum_df.dtypes)
  print(Main_areaXsum_df.tail(5))
 
except:
  traceback.print_exc(file=sys.stdout)
  print ("**** Error encountered. Printing traceback! ****")
  traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
  print("*** Print_exception:")
  # exc_type below is ignored on 3.5 and later
  traceback.print_exception(exc_type, exc_value, exc_traceback, limit=2, file=sys.stdout)
finally:

  # Close database file
  # conn.close()
  #clean up dataframes
  del  [[Ensmblfile , area_df, link_df, vcr_df]]
  gc.collect()
  Ensmblfile = pd.DataFrame()
  area_df = pd.DataFrame()
  link_df = pd.DataFrame()
  vcr_df = pd.DataFrame()

  print( "\nExecution of <Ensemble_Table> completed