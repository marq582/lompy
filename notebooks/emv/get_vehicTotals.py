
# This code creates the tables <vehicTotals>  and <linkXIsSafe> in the analysis database.
# The code assumes that the  data table <areaSHPdata> from the area shapefile is already in the database.
# Area Data table records should have attributes: SUBSECTOR, TOTALVEHIC, EVAC_NODE.")
# The code assumes that the  data table <nodeSHPdata> from the node shapefile is already in the database.
# Node Data table records should have attributes: ID, LABEL, EVAC_SES, SAFE_SES.")
# The code assumes that the Matsim output table <link> is already in the database.

import sqlite3
import pandas as pd
import sys, traceback
import os
import gc

#fileName = "D:/EMV/Scen1/output/matsim_output.db"

print ("Current working directory")
print(os.getcwd())
os.chdir(os.path.dirname(fileName))
print ("New working directory")
print(os.getcwd())

# input tables
areaSHPtbl = "areaSHPdata"
nodeSHPtbl = "nodeSHPdata"
matsimEventTbl = "event"
matsimNodeTbl = "node"
matsimLinkTbl = "link"

# output tables
vehTotalsTbl = "vehicTotals"
linkSafeTbl = "linkXIsSafe"

# Connect to the database file
conn = sqlite3.connect(fileName)
c = conn.cursor()

try: 
  #initial cleanup
  sqlqry = "DROP TABLE IF EXISTS " + vehTotalsTbl
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  sqlqry = "DROP TABLE IF EXISTS " + linkSafeTbl
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)  
  
  
  # In[25]:
  
  
  print("\nExtract data from area shapefile") 
  sqlqry = "SELECT SUBSECTOR as areaID"
  sqlqry += ", CAST(TOTAL_VEH AS INTEGER) AS totvehicle"
  sqlqry += ", CAST( CAST(EVAC_NODE AS INT) AS TEXT) AS nodeID"
  sqlqry += " FROM " + areaSHPtbl
  sqlqry += " WHERE (SUBSECTOR <> '')"
  sqlqry += " ORDER BY SUBSECTOR" 
  print("Running query <"+sqlqry+">. Please wait!")
  area_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of area_df");   print(area_df.shape); print(area_df.tail(5)); print(area_df.dtypes)
  
  
  # In[26]:
  
  
  print("\nExtract data from node SHPdata") 
  sqlqry = "SELECT CAST(CAST(ID AS INT) AS TEXT) as nodeID"
  sqlqry += ", CAST(EVAC_SES AS INT) AS isEVAC"
  sqlqry += ", CAST(SAFE_SES AS INT) AS isSAFE"
  sqlqry += " FROM " + nodeSHPtbl
  sqlqry += " WHERE (CAST(EVAC_SES AS INT)>0 OR CAST(SAFE_SES AS INT)>0)"
  sqlqry += " ORDER BY ID, EVAC_SES desc, SAFE_SES desc" 
  print("Running query <"+sqlqry+">. Please wait!")
  node_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of node_df"); print(node_df.shape); print(node_df.tail(5)); print(node_df.dtypes)
  
  
  # In[27]:
  
  
  print("\nJoin area and node dataframes to get vehicle totals per areaID")
  vehtot_df = pd.merge(area_df, node_df, on='nodeID', how='left')
  vehtot_df = vehtot_df.sort_values(['nodeID', 'areaID', 'totvehicle'], ascending=[True, True, False])
  vehtot_df['grpsum'] = vehtot_df.groupby(['nodeID'])['totvehicle'].transform('sum')
  print("Shape of vehtot_df"); print(vehtot_df.shape); print(vehtot_df.tail(5)); print(vehtot_df.dtypes)
  
  
  # In[28]:
  
  
  #get proportion
  print("\nGet proportion per areaID")
  vehtot_df['grpprop'] = 0
  vehtot_df.loc[vehtot_df.grpsum>0, 'grpprop'] = vehtot_df.loc[vehtot_df.grpsum>0,['totvehicle']].values/vehtot_df.loc[vehtot_df.grpsum>0,['grpsum']].values
  
  print("Shape of vehtot_df"); print(vehtot_df.shape); print(vehtot_df.tail(10));  print(vehtot_df.dtypes)
  
  
  # In[29]:
  
  
  #write table in database 
  vehtot_df.to_sql(vehTotalsTbl, conn, index=False)
  print("\nWriting of SQL table <"+vehTotalsTbl+"> completed!")
  
  
  # In[30]:
  
  
  # Get ids of used links in event table
  print("\nSet up table of used links")
  sqlqry = "SELECT CAST(link AS TEXT) as linkID"
  sqlqry += " FROM "+matsimEventTbl 
  sqlqry += " WHERE link<>''"
  sqlqry += " GROUP BY link"
  sqlqry += " ORDER BY link"
  print("Running query <"+sqlqry+">. Please wait!")
  used_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of used_df"); print(used_df.shape); print(used_df.tail(5))
  
  
  # In[31]:
  
  
  print("\nSetup linkXisEvacXisSafe table")  
  sqlqry = "SELECT DISTINCT id AS linkID, capacity"
  sqlqry += ", [from] AS origstart, [to] AS origend"
  sqlqry += " FROM "+matsimLinkTbl
  sqlqry += " ORDER BY id"  
  print("Running query <"+sqlqry+">. Please wait!")
  link_df = pd.read_sql_query(sqlqry, conn)
  link_df['capacity'] = link_df['capacity'].astype(float)
  print("Shape of link_df"); print(link_df.shape); print(link_df.tail(5)); 
  
  
  # In[32]:
  
  
  print("\nReduce link table to those used in event")  
  link_df = pd.merge (link_df, used_df, how='inner', on="linkID")
  print("Shape of link_df")
  print(link_df.shape);  print(link_df.tail(5));  print(link_df.dtypes)
  
  
  # In[33]:
  
  
  print("\nGet link nodes attributes from node_df")  
  temp1_df = pd.merge (link_df, node_df[['nodeID', 'isEVAC', 'isSAFE']], how='left', left_on="origstart", right_on="nodeID")
  temp2_df = pd.merge (link_df, node_df[['nodeID', 'isEVAC', 'isSAFE']], how='left', left_on="origend", right_on="nodeID")
  link_df = pd.concat([temp1_df, temp2_df])
  link_df.drop('nodeID', axis=1, inplace=True)
  link_df['isEVAC_SES'] = link_df.groupby(['linkID','capacity','origstart','origend'])['isEVAC'].transform('max')
  link_df['isSAFE_SES'] = link_df.groupby(['linkID','capacity','origstart','origend'])['isSAFE'].transform('max')
  
  link_df.drop('isEVAC', axis=1, inplace=True)
  link_df.drop('isSAFE', axis=1, inplace=True)
  #link_df.rename({'isEVAC':'isEVAC_SES', 'isSAFE':'isSAFE_SES'}, axis=1,  inplace=True)
  
  print("Shape of link_df");  print(link_df.shape);  print(link_df.tail(15))
  
  
  # In[34]:
  
  
  #write table in database 
  link_df.to_sql(linkSafeTbl, conn, index=False)
  print("\nWriting of SQL table <"+linkSafeTbl+"> completed!")
  
  
  # In[46]:
  
  
  print("Get Node coordinates")
  node_df = pd.DataFrame()    
  
  sqlqry = "SELECT id as origstart, x as x1, y as y1"
  sqlqry += " FROM " + matsimNodeTbl
  sqlqry += " ORDER BY id" 
  print("Running query <"+sqlqry+">. Please wait!")
  node_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of node_df"); print(node_df.shape); print(node_df.tail(5)); print(node_df.dtypes)
  
  
  # In[47]:
  
  
  temp1_df = pd.DataFrame()
  temp1_df = pd.merge (link_df, node_df, how='left', on="origstart")
  print("Shape of temp1_df");  print(temp1_df.shape);  print(temp1_df.tail(5))
    
  node_df.columns = ['origend', 'x2', 'y2'] 
  temp1_df = pd.merge (temp1_df, node_df, how='left', on="origend")
  print("Shape of temp1_df");  print(temp1_df.shape);  print(temp1_df.tail(5))
  
  
  # In[48]:
  
  
  temp1_df["WKT"] = "LINESTRING ("+ temp1_df.x1 + " " + temp1_df.y1 + ", " + temp1_df.x2 + " " + temp1_df.y2 + ")"
  temp1_df.drop(['x1','y1','x2','y2','isEVAC_SES', 'isSAFE_SES'], axis=1, inplace=True)
  temp1_df.columns = ['ID', 'capacity', 'INODE', 'JNODE', 'WKT']
  print("Shape of temp1_df.WKT");  print(temp1_df.shape);  print(temp1_df.tail(5))
  
  
  # In[49]:
  
  
  #write table in CSV
  summTbl_csv = "links_WKT.csv"
  temp1_df.to_csv(summTbl_csv, index=False)
  print("\nWriting of CSV table <"+summTbl_csv+"> completed!")
  
  
  # In[19]:
  
  
finally:
  #clean up
  del [[area_df, node_df, vehtot_df, used_df, link_df, temp1_df, temp2_df]] 
  gc.collect()
  area_df = pd.DataFrame()
  node_df = pd.DataFrame()
  vehtot_df = pd.DataFrame()
  used_df = pd.DataFrame()
  link_df = pd.DataFrame()
  temp1_df = pd.DataFrame()
  temp2_df = pd.DataFrame()
  
  # Close database file
  conn.close()
  
  print( "\nExecution of <Run_py_vehicTotals> completed!")
  
  
  # In[ ]:
  



