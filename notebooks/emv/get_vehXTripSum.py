# This code creates the tables <vehXlinkXtime> and <vehXtripsum> in the analysis database.
# The code assumes that the Matsim output table <event> is already in the database.
# The code assumes that the output table <linkXIsSafe> from Run_py_vehicTotals.wsx is already in the database.

import sqlite3
import pandas as pd
import sys, traceback
import os
import gc

#fileName = "D:/EMV/Scen1/output/matsim_output.db"


# In[3]:


print ("Current working directory")
print(os.getcwd())
os.chdir(os.path.dirname(fileName))
print ("\nNew working directory")
print(os.getcwd())


# In[5]:


# input tables
matsimEventTbl = "event"
linkTblnm = "linkXIsSafe"
areaTblnm = "areaSHPdata"
nodeTblnm = "nodeSHPdata"
personXareaTbl = "personXarea"
journeyFile = "output_matsim_journeys.txt"

# output tables
vehTblnm = "vehXlinkXtime"
tripTblnm = "vehXtripsum"


# In[6]:


# Connect to the database file
conn = sqlite3.connect(fileName)
c = conn.cursor()


# In[7]:


try:

  #initial cleanup
  sqlqry = "DROP TABLE IF EXISTS " + vehTblnm
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  sqlqry = "DROP TABLE IF EXISTS " + tripTblnm
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  
  
  # In[12]:
  
  
  #take values from journey table from Matsim
  print("\nSet up vehXjourney table") 
  journey_df = pd.read_csv(journeyFile, sep='\t')
  journey_df = journey_df.astype({"person_id": str})
  journey_df = journey_df.sort_values(['person_id'],ascending=[True])
  journey_df = journey_df[['person_id', 'start_time', 'end_time']] 
  journey_df.columns = ['vehicle', 'deptime', 'arrtime']
  
  print("Shape of journey_df");  print(journey_df.shape);  print(journey_df.tail(10))
  
  
  # In[18]:
  
  
  print("\nSet up vehXmaxtime table")
  sqlqry = "SELECT A1.vehicle as vehicle"
  sqlqry += ", max(CAST(A1.[time] as INT)) AS vehmaxtime"
  sqlqry += " FROM " + matsimEventTbl + " AS A1" 
  sqlqry += " GROUP BY A1.vehicle" 
  sqlqry += " HAVING ((A1.vehicle)<>'')"
  #sqlqry += " AND ((A1.link)<>'')" 
  sqlqry += " ORDER BY A1.vehicle"
  print("Running query <"+sqlqry+">. Please wait!")
  vehmaxtime_df = pd.read_sql_query(sqlqry, conn)
  
  print("Shape of vehmaxtime_df");  print(vehmaxtime_df.shape);  print(vehmaxtime_df.tail(5))
  
  
  # In[21]:
  
  
  print("\nSet up vehXlinkXtime table")
  sqlqry = "SELECT -1 as seqno, A1.vehicle as vehicle, A1.link AS link, A1.type AS type"
  #sqlqry += ", 0 AS isEVAC, 0 as isSAFE, '---' AS origstart, '---' AS origend" 
  sqlqry += ", CAST(A1.[time] AS INT) AS mintime"
  sqlqry += " FROM " + matsimEventTbl + " AS A1" 
  sqlqry += " GROUP BY A1.vehicle, A1.[time], A1.link" 
  sqlqry += " HAVING ((((A1.vehicle)<>'') AND ((A1.link)<>''))" 
  #sqlqry += " AND ((A1.vehicle)='1005' OR (A1.vehicle)='0' OR (A1.vehicle)='1007')" 
  sqlqry += " AND ((A1.type)<>'PersonLeavesVehicle') And ((A1.type)<>'left link') And ((A1.type)<>'vehicle leaves traffic'))" 
  sqlqry += " ORDER BY A1.vehicle, mintime"
  print("Running query <"+sqlqry+">. Please wait!")
  veh_df = pd.read_sql_query(sqlqry, conn)
  
  veh_df['seqno'] = veh_df.index.values
  #veh_df['nxtseq'] = veh_df['seqno']+1
  
  print("\nSetting value of maxtime to time in next row")
  veh_df['maxtime'] = veh_df['mintime'].shift(-1)
  print("Shape of veh_df");  print(veh_df.shape);  print(veh_df.tail(5))
  
  
  # In[22]:
  
  
  print("Get maximum seqno per vehicle")
  maxseq_df = veh_df.groupby(['vehicle'])['seqno'].max().reset_index()
  maxseq_df.columns= ['vehicle', 'maxseq']
  print(maxseq_df.shape); print(maxseq_df.tail(5))
  
  print("\nUpdate maxtime fields in maxseq_df table")
  maxseq_df = pd.merge(maxseq_df, vehmaxtime_df, on='vehicle', how='left')
  print(maxseq_df.shape);  print(maxseq_df.tail(5))
  
  
  # In[23]:
  
  
  veh_df = pd.merge(veh_df, maxseq_df, left_on=['vehicle', 'seqno'], right_on=['vehicle', 'maxseq'], how='left')
  #veh_df[['seqno', 'maxseq']] = veh_df[['seqno', 'maxseq']].astype(int)
  
  #Remove NaN values
  veh_df.fillna(-1, inplace=True)
  print(veh_df.shape);  print(veh_df.tail(5))
  
  
  # In[24]:
  
  
  #update maxtime to vehmaxtime
  idx = veh_df['vehmaxtime']>0
  veh_df.loc[idx,'maxtime'] = veh_df['vehmaxtime'] 
  veh_df.drop(['maxseq','vehmaxtime'], axis=1, inplace=True) 
  print("Shape of veh_df with min-max link times")
  print(veh_df.shape);  print(veh_df.tail(5))
  
  
  # In[25]:
  
  
  #write table in database 
  veh_df.to_sql(vehTblnm, conn, index=False)
  print("\nWriting of SQL table <"+vehTblnm+"> completed!")
  
  
  # In[28]:
  
  
  print("\nSet up trip summary table")
  sqlqry = "SELECT vehicle, count(link) as nlinks"
  sqlqry += ", min(mintime) as deptime, max(maxtime) as arrtime"
  #sqlqry += ", min(seqno) as depseqno, max(seqno) as arrseqno"
  sqlqry += ", 1 AS isEVAC"
  #sqlqry += ", 1 AS isSAFE"
  sqlqry += " FROM "+vehTblnm
  sqlqry += " GROUP BY vehicle"
  sqlqry += " ORDER BY vehicle"
   
  print("Running query <"+sqlqry+">. Please wait!")
  trip_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of trip_df");  print(trip_df.shape);  print(trip_df.tail(5))
  
  
  # In[29]:
  
  
  #print("\nUpdate deptime and arrtime fields from journey table")
  #trip_df = pd.merge(trip_df, journey_df, on='vehicle', how='left')
  
  print("Add column isSuccess to indicate if trip time < 10 hours.")
  trip_df['isSuccess'] = 0
  trip_df.loc[trip_df['arrtime']-trip_df['deptime']<(10*60*60), 'isSuccess'] = 1
  
  print("Shape of trip_df");  print(trip_df.shape);   print(trip_df.tail(5))
  
  
  # In[32]:
  
  
  print("\nFind subsector of vehicles")
  sqlqry = "SELECT A1.person as vehicle, A1.subsector"
  #sqlqry += " , B1.EVAC_NODE"
  sqlqry += " FROM " + personXareaTbl + " AS A1" 
  #sqlqry += " INNER JOIN " + areaTblnm + " AS B1" 
  #sqlqry += " ON A1.subsector = B1.subsector"
  sqlqry += " ORDER BY A1.person"
  
  print("Running query <"+sqlqry+">. Please wait!")
  temp2_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of temp2_df"); print(temp2_df.shape);  print(temp2_df.tail(5))
  
  
  # In[33]:
  
  
  print("\nUpdate subsector fields in trip table")
  trip_df = pd.merge(trip_df, temp2_df, on='vehicle', how='left')
  print("Shape of trip_df with subsectors")
  print(trip_df.shape);   print(trip_df.tail(5))
  
  
  # In[34]:
  
  
  print("\nGet arrival times of vehicles")
  sqlqry = "DROP TABLE IF EXISTS temp1_df"
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  
  sqlqry = "CREATE TABLE  temp1_df AS "
  sqlqry += "SELECT vehicle, max(seqno) as arrseqno"
  #sqlqry += ", 1 AS isSAFE"
  sqlqry += " FROM " + vehTblnm 
  sqlqry += " GROUP BY vehicle"
  sqlqry += " ORDER BY vehicle"
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
    
  
  
  # In[35]:
  
  
  print("\nFind arrival links of vehicles")
  sqlqry = "SELECT A1.vehicle as vehicle, A1.link as arrlink, A1.type AS arrtype"
  sqlqry += " FROM " + vehTblnm + " AS A1" 
  sqlqry += " INNER JOIN temp1_df AS B1"
  sqlqry += " ON ((A1.vehicle=B1.vehicle) AND (A1.seqno = B1.arrseqno))" 
  sqlqry += " ORDER BY A1.vehicle"
  
  print("Running query <"+sqlqry+">. Please wait!")
  temp2_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of temp2_df")
  print(temp2_df.shape)
  print(temp2_df.tail(5))
  
  
  # In[36]:
  
  
  #clean up temp1_df
  sqlqry = "DROP TABLE IF EXISTS temp1_df"
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  
  print("\nUpdate arrival fields in trip table")
  trip_df = pd.merge(trip_df, temp2_df, on='vehicle', how='left')
  trip_df.drop(['arrtype'], axis=1, inplace=True) 
  
  print("Shape of trip_df with arrival times");   print(trip_df.shape);   print(trip_df.tail(5))
  
  
  # In[37]:
  
  
  print("Update aborted or congested trips")
  #idx = (trip_df['arrtype'] in {'vehicle aborts', 'stuckAndAbort', 'agentInCongestion'})
  #idx = (trip_df['arrtype']=='vehicle aborts') 
  #idx = ((trip_df['arrtype']=='vehicle aborts') or (trip_df['arrtype']=='stuckAndAbort') or (trip_df['arrtype']=='agentInCongestion'))
  
  #trip_df.loc[idx,'isSAFE'] = 0 
  #print("Shape of trip_df with min-max link times");  print(trip_df.shape);  print(trip_df.tail(5))
  
  
  # In[38]:
  
  
  sqlqry = "SELECT linkID as arrlink, min(origstart) AS arrnode1, min(origend) AS arrnode2"
  sqlqry += " FROM " + linkTblnm 
  sqlqry += " GROUP BY linkID"
  sqlqry += " ORDER BY linkID"
  
  print("Running query <"+sqlqry+">. Please wait!")
  temp2_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of temp2_df");  print(temp2_df.shape);  print(temp2_df.tail(5))
  
  
  # In[39]:
  
  
  print("\nUpdate arrival nodes in trip table")
  trip_df = pd.merge(trip_df, temp2_df, on='arrlink', how='left')
  print("Shape of trip_df with arrival nodes");  print(trip_df.shape);  print(trip_df.tail(5))
  
  
  # In[40]:
  
  
  print("\nGet nodes for node SHP data")
  sqlqry = "SELECT CAST(CAST(ID AS INT) AS TEXT) AS nodeID"
  sqlqry += ", CAST(SAFE_SES AS INT) AS isSAFE"
  sqlqry += " FROM " + nodeTblnm 
  sqlqry += " ORDER BY ID"
  
  print("Running query <"+sqlqry+">. Please wait!")
  temp2_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of temp2_df")
  print(temp2_df.shape)
  print(temp2_df.tail(5))
  
  
  # In[41]:
  
  
  trip_df = pd.merge(trip_df, temp2_df, left_on='arrnode2', right_on='nodeID', how='left')
  trip_df.drop(['nodeID'], axis=1, inplace=True)
  print("Shape of trip_df")
  print(trip_df.shape)
  print(trip_df.tail(5))
  
  
  # In[42]:
  
  
  #write table in database 
  trip_df.to_sql(tripTblnm, conn, index=False)
  print("\nWriting of SQL table <"+tripTblnm+"> completed!")
  
  # Commit the changes
  conn.commit()
  
  
  # In[43]:
  
  
finally:
  #clean up
  del [[veh_df, vehmaxtime_df, maxseq_df, journey_df, trip_df, temp2_df ]] 
  gc.collect()
  veh_df = pd.DataFrame()
  vehmaxtime_df = pd.DataFrame()
  maxseq_df = pd.DataFrame()
  journey_df = pd.DataFrame()
  trip_df = pd.DataFrame()
  temp2_df = pd.DataFrame()
    
  #Close database file
  conn.close()
    
  print( "Execution of <Run_py_vehXtripsum> completed!")
  
