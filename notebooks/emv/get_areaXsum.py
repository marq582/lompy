# This code creates the area summary table <onerunXareasum> in the analysis database.
# The code assumes that the output table <vehicTotals> from Run_py_vehicTotals.wsx is already in the database.
# The code assumes that the output table <vehXtripsum> from Run_py_vehXtripsum.wsx is already in the database.

import sqlite3
import pandas as pd
import sys, traceback
import os
import gc

#fileName = "D:/EMV/Scen1/output/matsim_output.db"

print ("Current working directory")
print(os.getcwd())
os.chdir(os.path.dirname(fileName))
print ("New working directory")
print(os.getcwd())


# In[67]:


# input tables
tripTblnm = "vehXtripsum"
nodeTblnm = "vehicTotals"
areaSHPtbl = "areaSHPdata"
vehevacTbl = "vehXevacnode"

# output tables
summaryTblnm = "onerunXareasum"
detldTblnm = "area_evactrips"


# In[68]:


# Connect to the database file
conn = sqlite3.connect(fileName)
c = conn.cursor()


# In[69]:


try: 
  #initial cleanup
  sqlqry = "DROP TABLE IF EXISTS " + summaryTblnm
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  
  
  # In[70]:
  
  
  print("\nSetup area summary table") 
  sqlqry = "SELECT areaID as subsector"
  sqlqry += ", sum(totvehicle) as Num_veh FROM "
  sqlqry += nodeTblnm 
  sqlqry += " GROUP BY areaID"
  sqlqry += " ORDER BY areaID"
   
  print("Running query <"+sqlqry+">. Please wait!")
  area_df = pd.read_sql_query(sqlqry, conn)
  print("DF Shape of area_df");  print(area_df.shape);  print(area_df.tail(5));  
    
  vehic_ins_val = area_df['Num_veh'].sum()    
  print("\nNum_vehic_ins = " + str(vehic_ins_val))
  
  
  # In[71]:
  
  
  print("\nGet detailed trip time stats (success and fail) by area")
  sqlqry = "SELECT"
  sqlqry += " A1.subsector as subsector"
  #sqlqry += ", B1.origend as StartNode"
  sqlqry += ", A1.arrnode2 as EndNode"
  #sqlqry += ", CAST(A1.isSAFE AS INT) AS isSAFE"
  sqlqry += ", A1.isSuccess"
  sqlqry += ", count(A1.vehicle) as Num_evac"
  sqlqry += ", min(A1.deptime)/60.0 as first_dep_min"
  sqlqry += ", max(A1.deptime)/60.0 as last_dep_min"
  sqlqry += ", min(A1.arrtime)/60.0 as first_arr_min"
  sqlqry += ", max(A1.arrtime)/60.0 as last_arr_min"
  sqlqry += ", avg(A1.arrtime-A1.deptime)/60.0 as avg_triptime_min"
  sqlqry += ", max(A1.arrtime-A1.deptime)/60.0 as max_triptime_min"
  sqlqry += " FROM "+tripTblnm+" AS A1"
  #sqlqry += " INNER JOIN "+ vehevacTbl +" AS B1 ON (A1.link=B1.vehicle)"     
  sqlqry += " GROUP BY A1.subsector,  A1.isSuccess, A1.arrnode2"
  sqlqry += " ORDER BY A1.subsector,  A1.isSuccess, A1.arrnode2"
    
  print("Running query <"+sqlqry+">. Please wait!")
  temp1_df = pd.read_sql_query(sqlqry, conn)
  print("DF Shape of temp1_df");  print(temp1_df.shape);  print(temp1_df.tail(5))
  
  
  # In[72]:
  
  
  print("merge temp1 with area_df")
  detevac_df = pd.merge(area_df, temp1_df,on='subsector',how='right')
  detevac_df['pct_demand'] = 0
  detevac_df.loc[detevac_df['Num_evac']>0, 'pct_demand'] = detevac_df['Num_evac'] / detevac_df['Num_veh']
  
  print("DF Shape of detevac_df")
  print(detevac_df.shape)
  print(detevac_df.tail(5))
  
  
  # In[73]:
  
  
  #write table in CSV 
  summTbl_csv = detldTblnm + ".csv"
  detevac_df.to_csv(summTbl_csv, index=False)
  print("\nWriting of CSV table <"+summTbl_csv+"> completed!")
  
  
  # In[74]:
  
  
  temp1_df = detevac_df.groupby(['subsector'])['Num_evac'].sum().reset_index()
  temp1_df.columns= ['subsector', 'Num_evac']
  #print(temp1_df.tail(5))
  area_df = pd.merge(area_df, temp1_df, on='subsector', how='left')
  #print(area_df.tail(5))
  
  temp1_df = detevac_df.loc[detevac_df['isSuccess']<1]
  temp2_df = temp1_df.groupby(['subsector'])['Num_evac'].sum().reset_index()
  temp2_df.columns= ['subsector', 'Num_fail']
  area_df = pd.merge(area_df, temp2_df, on='subsector', how='left')
  print("DF Shape of area_df with Num_fail");  print(area_df.shape);  print(area_df.tail(5))
  
  
  # In[75]:
  
  
  temp1_df = detevac_df.loc[detevac_df['isSuccess']>=1]
  temp2_df = temp1_df.groupby(['subsector'])['Num_evac'].sum().reset_index()
  temp2_df.columns= ['subsector', 'Num_success']
  area_df = pd.merge(area_df, temp2_df, on='subsector', how='left')
  area_df["Num_fail"].fillna(0, inplace = True)
  print("DF Shape of area_df with Num_success");  print(area_df.shape);  print(area_df.tail(5))
  
  
  # In[76]:
  
  
  print("\nGet evacuation time by area only for successful trips")
  sqlqry = "SELECT"
  sqlqry += " A1.subsector as subsector"
  sqlqry += ", (A1.arrtime-A1.deptime)/60.0 as evactime_min"
  sqlqry += " FROM "+tripTblnm+" AS A1"
  sqlqry += " WHERE A1.isSuccess>=1"
  print("Running query <"+sqlqry+">. Please wait!")
  evactime_df = pd.read_sql_query(sqlqry, conn)
  
  evacstd_df = pd.DataFrame(evactime_df.groupby(['subsector'])['evactime_min'].std().reset_index(name='std_evactime_min'))
  print("DF Shape of evacstd_df")
  print(evacstd_df.shape)
  print(evacstd_df.tail(5))
  
  
  # In[77]:
  
  
  #create area summary table
  print("\nGet  travel time stats by area")
  sqlqry = "SELECT"
  sqlqry += " A1.subsector as subsector"
  sqlqry += ", min(A1.deptime)/60.0 as first_dep_min"
  sqlqry += ", max(A1.deptime)/60.0 as last_dep_min"
  sqlqry += ", min(A1.arrtime)/60.0 as first_arr_min"
  sqlqry += ", max(A1.arrtime)/60.0 as last_arr_min"
  sqlqry += ", avg(A1.arrtime-A1.deptime)/60.0 as avg_evactime_min"
  sqlqry += ", max(A1.arrtime-A1.deptime)/60.0 as max_evactime_min"
  sqlqry += " FROM "+tripTblnm+" AS A1"
  sqlqry += " WHERE A1.isSuccess>=1"
  sqlqry += " GROUP BY A1.subsector"
  sqlqry += " ORDER BY A1.subsector"
    
  print("Running query <"+sqlqry+">. Please wait!")
  temp1_df = pd.read_sql_query(sqlqry, conn)
  print("DF Shape of temp1_df")
  print(temp1_df.shape)
  print(temp1_df.tail(5))
  
  
  # In[78]:
  
  
  #merge temp1 with area_df
  #area_df = pd.merge(area_df, temp1_df,on='subsector',how='right')
  area_df = pd.merge(area_df, temp1_df,on='subsector',how='left')
  area_df = pd.merge(area_df, evacstd_df, on='subsector', how='left')
  #area_df['pct_success'] = 0
  area_df['pct_success'] = area_df['Num_evac']
  area_df.loc[area_df['Num_evac']>0, 'pct_success'] = 0.0
  area_df.loc[area_df['Num_success']>0, 'pct_success'] = 100.0 *  area_df['Num_success'] / area_df['Num_veh']
  
  print("DF Shape of area_df")
  print(area_df.shape)
  print(area_df.head(5))
  print(area_df.tail(5))
  
  
  # In[79]:
  
  
  #sort area_df wrt to Num_stck, pct_success
  area_df2 = area_df.sort_values(['Num_fail', 'pct_success', 'avg_evactime_min'], ascending=[False, True, False])
  #delete other columns
  area_df2.drop(['first_dep_min', 'last_dep_min', 'first_arr_min'], axis = 1, inplace = True)
  area_df2.drop(['last_arr_min', 'std_evactime_min', 'max_evactime_min'  ], axis = 1, inplace = True)
  #get top 5
  area_df2 = area_df2.head(5)
  print("DF Shape of area_df2")
  print(area_df2.shape)
  print(area_df2.head(5))
  #write table in CSV
  summTbl_csv = "worst_subsectors.csv"
  area_df2.to_csv(summTbl_csv, index=False)
  print("\nWriting of CSV table <"+summTbl_csv+"> completed!")
  
  
  # In[80]:
  
  
  #add Grand_Total row to area_df
  sum_row = {col: area_df[col].sum() for col in area_df}
  min_row = {col: area_df[col].min() for col in area_df}
  max_row = {col: area_df[col].max() for col in area_df}
  min_row = {col: area_df[col].min() for col in area_df}
  area_df3 = pd.DataFrame(sum_row, index=["Total"])
  area_df3['subsector'] = 'Valley_wide'
  area_df3.loc[area_df3['Num_success']>0, 'pct_success'] = 100.0 *  area_df3['Num_success'] / area_df3['Num_veh']
  
  area_df3['avg_evactime_min'] = area_df['avg_evactime_min'].mean()
  area_df3['first_arr_min'] = area_df['first_arr_min'].min()
  area_df3['first_dep_min'] = area_df['first_dep_min'].min()
  area_df3['last_arr_min'] = area_df['last_arr_min'].max()
  area_df3['last_dep_min'] = area_df['last_dep_min'].max()
  area_df3['std_evactime_min'] = area_df['std_evactime_min'].mean()
  area_df3['max_evactime_min'] = area_df['max_evactime_min'].max()
  colms = ['subsector', 'Num_veh', 'Num_evac', 'Num_fail', 'Num_success', 'first_dep_min', 'last_dep_min', 'first_arr_min', 'last_arr_min', 'avg_evactime_min', 'std_evactime_min', 'max_evactime_min', 'pct_success']
  area_df3 = area_df3[colms]
  
  print("DF Shape of area_df3")
  print(area_df3.shape)
  print(area_df3.head(5))  
  
  
  # In[81]:
  
  
  #write table in CSV 
  summTbl_csv = "valley-wide_summary.csv"
  area_df3.to_csv(summTbl_csv, index=False)
  print("\nWriting of CSV table <"+summTbl_csv+"> completed!")
  
  
  # In[82]:
  
  
  #write table in CSV 
  summTbl_csv = summaryTblnm + ".csv"
  area_df.to_csv(summTbl_csv, index=False)
  print("\nWriting of CSV table <"+summTbl_csv+"> completed!")
  
  #write table in database 
  #area_df.to_sql(summaryTblnm, conn, index=False)
  #print("\nWriting of SQL table <"+summaryTblnm+"> completed!")
  
  # Commit the changes
  conn.commit()
  
finally:
  #clean up
  del [[area_df, area_df2, area_df3, detevac_df, evactime_df, evacstd_df, temp1_df, temp2_df ]] 
  gc.collect()
  area_df = pd.DataFrame()
  area_df2 = pd.DataFrame()
  area_df3 = pd.DataFrame()
  detevac_df = pd.DataFrame()
  evactime_df = pd.DataFrame()
  evacstd_df = pd.DataFrame()
  temp1_df = pd.DataFrame()
  temp2_df = pd.DataFrame()
  
  # Close database file
  conn.close()
  
  print( "\nExecution of <Run_py__onerunXareasum> completed!")
  
