# This code creates the personXarea table in the database from the output file <input_population_attrs.txt>.
# This code creates the tables <areaSHPdata> and <nodeSHPdata> in the analysis database.
# The code assumes that the  table  <event> is in the database.
 
import sqlite3
import pandas as pd
import sys, traceback
import os
import gc

#dbName = "D:/EMV/Scen1/output/matsim_output.db"

print ("Current working directory")
print(os.getcwd())
os.chdir(os.path.dirname(dbName))
print ("New working directory")
print(os.getcwd())

# output tables
summTblnm = "personXarea"
outputCSV = summTblnm+".csv"
vehEvacTbl = "vehXevacnode"

# output table
nodeSHPTbl = "nodeSHPdata"
areaSHPTbl = "areaSHPdata"
eventTbl  = 'event'
nodeTbl = "node"
linkTbl = "link"
personAreaTbl = "personXarea"

# Connect to the database file
conn = sqlite3.connect(dbName)
c = conn.cursor()

try: 
  #initial cleanup
  sqlqry = "DROP TABLE IF EXISTS " + summTblnm
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)

  print("Create personXarea table")
  
  filename = "../input_population_attrs.csv"
  
  plan_df = pd.read_csv(filename)
  plan_df.columns = ['person', 'subsector']
  plan_df = plan_df.astype({"person": str})
  plan_df = plan_df.sort_values(['person'],ascending=[True])
  
  print("Shape of plan_df");  print(plan_df.shape);  print(plan_df.tail(10))
  
  #write table in database 
  plan_df.to_sql(summTblnm, conn, index=False)
  print("\nWriting of SQL table <"+summTblnm+"> completed!")
  
  #write table in CSV 
  plan_df.to_csv(outputCSV, index=False)
  print("\nWriting of CSV table <"+outputCSV+"> completed!") 
  
  
  #initial cleanup
  sqlqry = "DROP TABLE IF EXISTS " + nodeSHPTbl
  print("\nRunning query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  
  sqlqry = "DROP TABLE IF EXISTS " + areaSHPTbl
  print("\nRunning query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  
  sqlqry = "DROP TABLE IF EXISTS " + vehEvacTbl
  print("\nRunning query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)


  print("\nExtract nodes from node table") 
  sqlqry = "SELECT id, x, y, 0 as EVAC_SES, 0 as SAFE_SES"
  sqlqry += " FROM " + nodeTbl
  sqlqry += " ORDER BY id" 
  
  print("Running query <"+sqlqry+">. Please wait!")
  node_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of node_df")
  print(node_df.shape)
  print(node_df.tail(5))
  print(node_df.dtypes)

  print("\nSetup link table")  
  sqlqry = "SELECT DISTINCT id AS link "
  sqlqry += ", [from] AS origstart, [to] AS origend"
  sqlqry += " FROM "+ linkTbl
  sqlqry += " ORDER BY id"  
  print("Running query <"+sqlqry+">. Please wait!")
  link_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of link_df")
  print(link_df.shape)
  print(link_df.tail(5))

  print("\nSetup veh-area table")  
  sqlqry = "SELECT person AS vehicle, subsector"
  sqlqry += " FROM "+ personAreaTbl
  sqlqry += " ORDER BY subsector, person"  
  print("Running query <"+sqlqry+">. Please wait!")
  vehArea_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of vehArea_df")
  print(vehArea_df.shape)
  print(vehArea_df.tail(5))

  print("\nExtract Evac Link rows from event table with vehicle and time") 
  sqlqry = "SELECT vehicle, link, time"
  sqlqry += " FROM " + eventTbl
  sqlqry += " WHERE ((type='vehicle enters traffic')"
  sqlqry += " AND  (link<>''))"
  sqlqry += " GROUP BY vehicle, time, link" 
  sqlqry += " ORDER BY vehicle, time, link" 
  print("Running query <"+sqlqry+">. Please wait!")
  evLink_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of evLink_df")
  print(evLink_df.shape)
  print(evLink_df.tail(5))
  print(evLink_df.dtypes)
  
  temp1_df = evLink_df.groupby(['vehicle'], as_index=False)['time'].min()
  temp1_df['isEvac']= 1
  print("Shape of temp1_df")
  print(temp1_df.shape)
  print(temp1_df.tail(5))

  print("\nMerge EVAC into evLink_df") 
  temp2_df = pd.merge(evLink_df, temp1_df, on=['vehicle', 'time'], how='left')
  temp2_df["isEvac"].fillna(0, inplace = True)
  temp2_df = temp2_df[(temp2_df["isEvac"]>0)]
  print("Shape of temp2_df")
  print(temp2_df.shape)
  print(temp2_df.tail(5))
  print(temp2_df.dtypes)
  
  print("\nGet EVAC_NODE for vehicles") 
  vehevac_df = pd.merge(temp2_df, link_df, on=['link'], how='left')
  print("Shape of vehevac_df");print(vehevac_df.shape); print(vehevac_df.tail(5))
  
  #write table in database 
  vehevac_df.to_sql(vehEvacTbl, conn, index=False)
  print("\nWriting of SQL table <"+vehEvacTbl+"> completed!")

  print("\nMerge EVAC with personXarea") 
  temp2_df = pd.merge(temp2_df, vehArea_df, on=['vehicle'], how='left')
  print("Shape of temp2_df")
  print(temp2_df.shape)
  print(temp2_df.tail(5))

  temp1_df = temp2_df.groupby(["subsector", "link"], as_index=False)["vehicle"].count()
  #areaSHP.columns = ['id', 'isEvac', 'lcount']
  #areaSHP.drop('lcount', axis=1, inplace=True)
  print("Shape of temp1_df")
  print(temp1_df.shape)
  print(temp1_df.tail(5))
  

  print("Merge areaSHP with link to get endnodes")
  evac_df = pd.merge(temp1_df, link_df, on=['link'], how='left')
  print("Shape of evac_df")
  print(evac_df.shape)
  print(evac_df.tail(5))
  
  areaSHP = evac_df.groupby(['subsector','origend'], as_index=False)['vehicle'].sum()
  areaSHP.columns = ['SUBSECTOR','EVAC_NODE', 'TOTAL_VEH']
  print("Shape of areaSHP"); print(areaSHP.shape); print(areaSHP.tail(5))
  
  #write table in database 
  areaSHP.to_sql(areaSHPTbl, conn, index=False)
  print("\nWriting of SQL table <"+areaSHPTbl+"> completed!")
  
  areaSHP['LOOK_AHEAD']= 13.0
  #write table in CSV
  summTbl_csv = areaSHPTbl+".csv"
  areaSHP.to_csv(summTbl_csv, index=False)
  print("\nWriting of CSV table <"+summTbl_csv+"> completed!")
  
  evac_df["isEvac"] = 1
  temp1_df = pd.DataFrame()
  temp1_df = evac_df.groupby(['origstart', 'isEvac'], as_index=False)['link'].count()
  temp1_df.columns = ['id', 'isEvac1', 'lcount']
  temp1_df.drop('lcount', axis=1, inplace=True)
  print("Shape of temp1_df"); print(temp1_df.shape); print(temp1_df.tail(5))
  
  temp2_df = pd.DataFrame()
  temp2_df = evac_df.groupby(['origend', 'isEvac'], as_index=False)['link'].count()
  temp2_df.columns = ['id', 'isEvac2', 'lcount']
  temp2_df.drop('lcount', axis=1, inplace=True)
  print("Shape of temp2_df"); print(temp2_df.shape); print(temp2_df.tail(5))
  
  print("\nMerge temp1_df with nodes") 
  node_df = pd.merge(node_df, temp1_df, on=['id'], how='left')
  node_df["isEvac1"].fillna(0, inplace = True)
  
  print("\nMerge temp2_df with nodes") 
  node_df = pd.merge(node_df, temp2_df, on=['id'], how='left')
  node_df["isEvac2"].fillna(0, inplace = True)
  
  node_df["EVAC_SES"] = node_df["isEvac1"] + node_df["isEvac2"]
  print("Shape of node_df"); print(node_df.shape); print(node_df.tail(5))
  
  print("\nExtract SAFE nodes from event table from event file") 
  sqlqry = "SELECT vehicle, link, time"
  sqlqry += " FROM " + eventTbl
  sqlqry += " WHERE ((type='vehicle leaves traffic')"
  sqlqry += " AND  (link<>''))"
  sqlqry += " GROUP BY vehicle, time, link" 
  sqlqry += " ORDER BY vehicle, time, link" 
  print("Running query <"+sqlqry+">. Please wait!")
  safe_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of safe_df"); print(safe_df.shape); print(safe_df.tail(5))
  
 
  temp1_df = safe_df.groupby(['vehicle'], as_index=False)['time'].min()
  temp1_df['isSafe']= 1
  print("Shape of temp1_df"); print(temp1_df.shape); print(temp1_df.tail(5))
  
  print("\nMerge temp1 into safe_df") 
  temp2_df = pd.merge(safe_df, temp1_df, on=['vehicle', 'time'], how='left')
  temp2_df["isSafe"].fillna(0, inplace = True)
  temp2_df = temp2_df[(temp2_df["isSafe"]>0)]
  print("Shape of temp2_df"); print(temp2_df.shape); print(temp2_df.tail(5))
  
 
  safe_df = temp2_df.groupby(["link", "isSafe"], as_index=False)["vehicle"].count()
  safe_df.drop("vehicle", axis=1, inplace=True)
  print("Shape of safe_df"); print(safe_df.shape); print(safe_df.tail(5))
  
  print("\nMerge SAFE with LINK") 
  safe_df = pd.merge(safe_df, link_df, on=['link'], how='left')
  print("Shape of safe_df"); print(safe_df.shape); print(safe_df.tail(5)); #print(safe_df.dtypes)
  
 
  temp1_df = pd.DataFrame()
  temp1_df = safe_df.groupby(['origstart', 'isSafe'], as_index=False)['link'].count()
  temp1_df.columns = ['id', 'isSafe1', 'lcount']
  temp1_df.drop('lcount', axis=1, inplace=True)
  print("Shape of temp1_df"); print(temp1_df.shape); print(temp1_df.tail(5))
  
  temp2_df = pd.DataFrame()
  temp2_df = safe_df.groupby(['origend', 'isSafe'], as_index=False)['link'].count()
  temp2_df.columns = ['id', 'isSafe2', 'lcount']
  temp2_df.drop('lcount', axis=1, inplace=True)
  print("Shape of temp2_df"); print(temp2_df.shape); print(temp2_df.tail(5))
  
  print("\nMerge temp1_df with nodes") 
  node_df = pd.merge(node_df, temp1_df, on=['id'], how='left')
  node_df["isSafe1"].fillna(0, inplace = True)
  
  print("\nMerge temp2_df with nodes") 
  node_df = pd.merge(node_df, temp2_df, on=['id'], how='left')
  node_df["isSafe2"].fillna(0, inplace = True)
  
  node_df["SAFE_SES"] = node_df["isSafe1"] + node_df["isSafe2"]
  print("Shape of node_df"); print(node_df.shape); print(node_df.tail(5))
  
  
  #print("EVAC nodes cannot be SAFE nodes")
  #node_df.loc[(node_df["EVAC_SES"]>0), "SAFE_SES"] = 0
  node_df.drop(['isEvac1','isEvac2','isSafe1','isSafe2' ], axis=1, inplace=True)
  
  print("Shape of node_df"); print(node_df.shape); print(node_df.tail(5))
  
  
  temp1_df = pd.DataFrame()
  selrows = (node_df.EVAC_SES+node_df.SAFE_SES >0)
  temp1_df = node_df.loc[selrows]
  print("Shape of temp1_df"); print(temp1_df.shape); print(temp1_df.tail(5))
  
    
  #write table in CSV
  summTbl_csv = "nodes_EVAC_SAFE.csv"
  temp1_df.to_csv(summTbl_csv, index=False)
  print("\nWriting of CSV table <"+summTbl_csv+"> completed!")
 
  
  node_df.drop(['x','y'], axis=1, inplace=True)
  
  #write table in database 
  node_df.to_sql(nodeSHPTbl, conn, index=False)
  print("\nWriting of SQL table <"+nodeSHPTbl+"> completed!")
  

finally:

  # Close database file
  conn.close()
  
  #clean up
  del [[node_df, link_df, evac_df, safe_df, temp1_df, temp2_df, vehevac_df]]
  gc.collect()
  node_df = pd.DataFrame()
  link_df = pd.DataFrame()
  evac_df = pd.DataFrame()
  safe_df = pd.DataFrame()
  temp1_df = pd.DataFrame()
  temp2_df = pd.DataFrame()
  vehevac_df  = pd.DataFrame()
  print( "\n Clean up completed!")
  
  print( "\nExecution of <get_NodeSHPData> completed!")

