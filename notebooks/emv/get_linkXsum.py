#!/usr/bin/env python
# coding: utf-8

# In[2]:


# This code creates the link summary table <onerunXlinksum> in the analysis database.
# This table will exclude links created by Matsim that are not in the link shapefile.
# This code also creates the volume-capacity-ratio CSV file <volcapratio_per_interval.csv> in long format.
# The code assumes that the DBF table <linkSHPdata> from the link shapefile is already in the database.
# The code assumes that the output table <linkXIsSafe> from Run_py_vehicTotals.wsx is already in the database.
# The code assumes that the output table <vehXlinkXtime> from Run_py_vehXtripsum.wsx is already in the database.
# The code assumes that the output table <vehXtripsum> from Run_py_vehXtripsum.wsx is already in the database.

import sqlite3
import pandas as pd
import sys, traceback
import os
import gc


# In[3]:


#fileName = "D:/EMV/Scen1/output/matsim_output.db"


# In[4]:


print ("Current working directory")
print(os.getcwd())
os.chdir(os.path.dirname(fileName))
print ("\nNew working directory")
print(os.getcwd())


# In[5]:


# input tables
link_floodtime_file = os.path.join("..", "hydrograph_linkID_time.txt")
vehTblnm = "vehXlinkXtime"
linkTblnm = "linkXIsSafe"
tripTblnm = "vehXtripsum"
linkSHPtbl = "linkSHPdata"
volcapTbl = "volcapratio_per_interval" 

# output tables
summTblnm = "onerunXlinksum"


# In[6]:


# Connect to the database file
conn = sqlite3.connect(fileName)
c = conn.cursor()


# In[7]:


try: 
  #initial cleanup
  sqlqry = "DROP TABLE IF EXISTS " + summTblnm
  print("\nRunning query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  
  
  # In[10]:
  
  
  print("\nSetup link summary table") 
  sqlqry = "SELECT A1.link as link_id"
  sqlqry += ", CAST(B1.capacity as REAL) as link_capacity_hour"
  sqlqry += ", max(maxtime)/60 AS last_time_used"
  sqlqry += ", count(A1.vehicle) as actual_tot_volume"
  sqlqry += " FROM "+vehTblnm+" AS A1"
  sqlqry += " INNER JOIN "+linkTblnm+" AS B1 ON (A1.link=B1.linkID)" 
  sqlqry += " GROUP BY A1.link"
  sqlqry += " ORDER BY A1.link"
   
  print("\nRunning query <"+sqlqry+">. Please wait!")
  link_df = pd.read_sql_query(sqlqry, conn)
  
  link_df['floodtime_min'] = 300
    
  print("Shape of link_df");  print(link_df.dtypes);  print(link_df.tail(5))
  
  
  # In[11]:
  
  
  idx = (link_df['link_capacity_hour']>0)
  volcap_df = link_df.loc[idx, ['link_id', 'link_capacity_hour']]
  #volcap_df = link_df.loc[['link_id', 'link_capacity_hour']]
  print("Shape of volcap_df");  print(volcap_df.dtypes);  print(volcap_df.tail(5))
  
  
  # In[13]:
  
  
  print("\nGet no of vehicles per link stuck in link for more than 10 hours.")
  sqlqry = " SELECT link as link_id, Count(vehicle) AS num_fail_10H"
  sqlqry += " FROM "+vehTblnm
  sqlqry += " WHERE ((maxtime-mintime)>(60*60*10))" 
  sqlqry += " GROUP BY link"
  sqlqry += " ORDER BY link ASC"
   
  print("\nRunning query <"+sqlqry+">. Please wait!")  
  temp1_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of temp1_df");  print(temp1_df.dtypes);  print(temp1_df.tail(5))
  
  
  # In[14]:
  
  
  print("\n Merge temp1 with link_df")
  if (len(temp1_df.index)>0): 
      link_df = pd.merge(link_df, temp1_df, on='link_id', how='left')
  else: 
      link_df['num_fail_10H'] = 0
   
  print("\nGet min, max times") 
  sqlqry = "SELECT min(deptime) AS lotime"
  sqlqry += ", max(arrtime) AS hitime" 
  sqlqry += " FROM " + tripTblnm 
  print("Running query <"+sqlqry+">. Please wait!")
  minmax_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of minmax_df");  print(minmax_df.shape);  print(minmax_df.tail(5))
  
  
  # In[15]:
  
  
  intlen_min = 15
  first_seq = int(minmax_df.at[0,'lotime'] / (60*intlen_min))
  last_seq  = int(minmax_df.at[0,'hitime'] / (60*intlen_min))+1 
  print("\n firs_seq = "+str(first_seq)+"; last_seq = "+ str(last_seq))
  
  #finding max flow volumes and times
  print("\nGet <"+str(intlen_min)+"-minute> time table")
  d = { 'seqno' : range(first_seq,last_seq,1),
      'currmin'   : range(first_seq*intlen_min,last_seq*intlen_min,intlen_min),
      'nxtmin'   : range((first_seq+1)*intlen_min,(last_seq+1)*intlen_min,intlen_min),
      'currsecs'  : range(first_seq*intlen_min*60,last_seq*intlen_min*60,intlen_min*60),
      'nxtsecs'  : range((first_seq+1)*intlen_min*60,(last_seq+1)*intlen_min*60,intlen_min*60)}
  time_df = pd.DataFrame(d)
  time_df = time_df.sort_values(['seqno'],ascending=[True])
  print("Shape of time_df");  print(time_df.head(10));  print(time_df.tail(10))
  
  
  # In[17]:
  
  
  sqlqry = "DROP TABLE IF EXISTS timetable"
  print("\nRunning query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
    
  #write table in database 
  time_df.to_sql("timetable", conn, index=False)
  print("\nWriting of SQL table <timetable> completed!")
  
  
  # In[19]:
  
  
  print("\nGet veh flow values and times")
  sqlqry = "DROP TABLE IF EXISTS temp1_df"
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  
  sqlqry = "CREATE TABLE  temp1_df AS "
  sqlqry += " SELECT link as link_id, Count(vehicle) AS volume"
  sqlqry += " , currsecs as startpd, currmin as startmin"
  sqlqry += " FROM "+vehTblnm+", timetable"
  sqlqry += " WHERE ((nxtsecs>mintime) AND (currsecs<=mintime))" 
  sqlqry += " GROUP BY link, currsecs"
  sqlqry += " ORDER BY link ASC, Count(vehicle) DESC, currsecs DESC"
  
  print("\nRunning query <"+sqlqry+">. Please wait!")  
  c.execute(sqlqry)
  
  
  # In[20]:
  
  
  print("\nWrite Link Summary table")  
  intcolname = "max_cap_"+str(intlen_min)+"min"
  print("\nGet <"+intcolname+"> flow values and times")
  sqlqry = "SELECT link_id, volume as "+intcolname
  sqlqry += ", startpd as maxcap_secs, startmin as maxcap_min"
  sqlqry += " FROM temp1_df"
  sqlqry += " WHERE (volume>0)"
  sqlqry += " GROUP BY link_id"
  sqlqry += " HAVING MIN(ROWID)"
  sqlqry += " ORDER BY volume DESC, link_id"
  
  print("\nRunning query <"+sqlqry+">. Please wait!")  
  temp2_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of temp2_df")
  print(temp2_df.dtypes)
  print(temp2_df.head(5))
  
  print("\nInsert max flow fields in link table")
  link_df = pd.merge(link_df, temp2_df, on='link_id', how='left')
  
  link_df['max_pct_capacity'] = 0
  link_df.loc[link_df['link_capacity_hour']>0, 'max_pct_capacity'] = 100*(60/intlen_min)* link_df[intcolname] / link_df['link_capacity_hour']
  
  print("\nShape of link_df with max flow fields")
  print(link_df.shape);  print(link_df.dtypes);  print(link_df.tail(5))
   
  
  
  # In[22]:
  
  
  print("\nRemove links not in the events file")
  sqlqry = "SELECT linkID AS link_id, 1 AS isInSHP, isEVAC_SES as isEvacRoute"
  sqlqry += " FROM " + linkTblnm
  sqlqry += " WHERE (linkID<>'')"
  sqlqry += " ORDER BY linkID"
  
  print("\nRunning query <"+sqlqry+">. Please wait!")  
  temp2_df = pd.read_sql_query(sqlqry, conn)
  print("Shape of temp2_df");  print(temp2_df.shape);  print(temp2_df.dtypes)
  
  
  # In[23]:
  
  
  print("\nInsert isInSHP column in link table")
  link_df = pd.merge(link_df, temp2_df, on='link_id', how='left')
  link_df = link_df.loc[link_df['isInSHP']==1]
  link_df.drop(['isInSHP'], axis=1, inplace=True)
  print("\nShape of link_df reduced to links in events file");  print(link_df.shape);  print(link_df.tail(5))
  
  
  # In[24]:
  
  
  #clean up
  print("\nClean up database")
  sqlqry = "DROP TABLE IF EXISTS temp1_df"
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  sqlqry = "DROP TABLE IF EXISTS temp2_df"
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  sqlqry = "DROP TABLE IF EXISTS timetable"
  print("Running query <"+sqlqry+">. Please wait!")
  c.execute(sqlqry)
  
  # Commit the changes
  conn.commit()
  
  
  # In[25]:
  
  
  #sort link_df wrt to numstuck, max_pct_capacity
  link_df2 = link_df.sort_values(['num_fail_10H', 'floodtime_min', 'max_pct_capacity'], ascending=[False, True, False])
  #delete other columns
  link_df2.drop(['last_time_used', 'maxcap_secs', 'maxcap_min'], axis = 1, inplace = True)
  #get top 10
  link_df2 = link_df2.head(10)
  print("DF Shape of link_df2")
  print(link_df2.shape)
  print(link_df2.head(5))
  
  
  # In[26]:
  
  
  #write table in CSV
  summTbl_csv = "worst_links.csv"
  link_df2.to_csv(summTbl_csv, index=False)
  print("\nWriting of CSV table <"+summTbl_csv+"> completed!")
    
  #write table in CSV 
  summTbl_csv = summTblnm + ".csv"
  link_df.to_csv(summTbl_csv, index=False)
  print("\nWriting of CSV table <"+summTbl_csv+"> completed!")
  
  
  # In[30]:
  
  
finally:
  #clean up
  del [[volcap_df, minmax_df, time_df, temp1_df, temp2_df]] 
  gc.collect()
  volcap_df = pd.DataFrame()
  minmax_df = pd.DataFrame()
  time_df = pd.DataFrame()
  temp1_df = pd.DataFrame()
  temp2_df = pd.DataFrame()
    
  # Close database file
  conn.close()
  print( "\nExecution of CSV <Run_py_onerunXlinksum> completed!")
  
