#!/usr/bin/env python
# coding: utf-8

# In[78]:


# Main_areaXsum_df

import pandas as pd
import sys, traceback
import os
import gc


# In[79]:


Ensmblfile = pd.DataFrame()
area_df = pd.DataFrame()
link_df = pd.DataFrame()
vcr_df = pd.DataFrame()

#inputFilePath = df1.iloc[0,0]
#olddir = df2.iloc[0,0]
#newdir = df3.iloc[0,0]

#inputFilePath = "D:/Backup/cluster/flush5/mar582/Data_12082020/__minOutput/ScenarioRuns.csv"
inputFilePath = "D:/Backup/cluster/flush5/mar582/Data_12082020/__minOutput/ScenarioRuns_1.csv"
olddir = "/flush5/mar582/Data_12082020"
newdir = "D:/Backup/cluster/flush5/mar582/Data_12082020/__minOutput"
#newdir = "D:\\Backup\\cluster\\flush5\\mar582\\Data_12082020\\__minOutput"

inputFilePath = inputFilePath.replace("\\", "\\\\")
olddir = olddir.strip()               
newdir = newdir.strip()

print("inputFilePath = <" + inputFilePath + ">")
print("olddir = <" + olddir + ">")
print("newdir = <" + newdir + ">")


# In[80]:


User_file = pd.read_csv(inputFilePath)
print("\nInitial Shape of User_file")
print(User_file.dtypes)
print(User_file.tail(5))

isDirReplace = ((len(olddir)>0) and (len(newdir)>0) and (olddir != newdir))
print ("isDirReplace: ", isDirReplace)


# In[81]:


#create ensemble summary table
isfirst_ensmbl = True
for index, row in User_file.iterrows():
    curr_ensmbl_file = row['Output_Dir']
    curr_scen_name = "----"
    #curr_scen_name = row['Scen_Name']
    curr_scen_name = row[1]
    print("Index: <",curr_scen_name,"> <",curr_ensmbl_file,">")
    if (isDirReplace):
      curr_ensmbl_file = curr_ensmbl_file.replace(olddir,newdir)
    print("New file: <",curr_ensmbl_file,">")

    Ensmblfile = pd.read_csv(curr_ensmbl_file)
    Ensmblfile['Scen_Name'] = curr_scen_name
    #print("\nRead of Ensmblfile")
    #print(Ensmblfile.dtypes)
    #print(Ensmblfile.tail(5))

    if isfirst_ensmbl:
        Ensmbl_file = Ensmblfile
        isfirst_ensmbl = False
    else:
        Ensmbl_file = Ensmbl_file.append(Ensmblfile, ignore_index=True)


# In[82]:


Ensmbl_file['Flood_Events']= "---"
Ensmbl_file['Dam']= "---"

print("\nInitial Shape of Ensmbl_file")
print(Ensmbl_file.dtypes)
print(Ensmbl_file.tail(5))  


# In[83]:


for index_ef, row_ef in Ensmbl_file.iterrows():
    outdir = row_ef['output_dir']
    if (isDirReplace):
      outdir = outdir.replace(olddir, newdir) 
    outdir = outdir.replace("\\","/")
    Ensmbl_file.at[index_ef,'output_dir'] = outdir
    #print("row outdir:<"+ row_ef['output_dir'] +">") 
    
    fefile = row_ef['flood_event_file']
    fefile = fefile.replace("\\","/")
    ss0 = fefile.rsplit("/",1)[0]
    Ensmbl_file.at[index_ef,'Dam'] = ss0.rsplit("/",1)[1]

    #print("fefile:<"+fefile+">, row_ef['Dam']:<"+row_ef['Dam']+">") 
    ss1 = fefile.rsplit("/",1)[1]
    Ensmbl_file.at[index_ef,'Flood_Events'] = ss1.rsplit(".csv",1)[0]

print("\nCurrent Shape of Ensmbl_file")
print(Ensmbl_file.tail(5))  


# In[84]:


Ensmbl_file['Popn_year']= 2016
scen_list1 = ['B05A','B06A','B07A'] 
scen_list2 = ['B09','B09A','B11A','B13A','B15A','B17A','B19A','B21A','B10A','B12A','B14A','B16A','B18A','B20A','B22A'] 

for index_ef_1, row_ef_1 in Ensmbl_file.iterrows():
  Popnfile = row_ef_1['zone_shapefile']
  popyear = 0
  for scenID in scen_list1:  
      if scenID in Popnfile:  
          popyear = 2026
  for scenID in scen_list2:  
      if scenID in Popnfile:  
          popyear = 2041
  Ensmbl_file.at[index_ef_1,'Popn_year'] = popyear
  #Ensmbl_file['Popn_year'].astype(int)

print("\Final Shape of Ensmbl_file")
print(Ensmbl_file.dtypes)
print(Ensmbl_file.tail(5))

#write table in CSV
summTbl_csv = "test_ensmbl_df.csv"
Ensmbl_file.to_csv(summTbl_csv, index=False)
print("\n Writing of summary CSV table for <"+summTbl_csv+"> completed!")


# In[85]:


#create area summary table
isfirst = True
for index_ef, row_ef in Ensmbl_file.iterrows():
    curr_run_id = row_ef['run_id']
    curr_dir = row_ef['output_dir']
    area_file = curr_dir +'/output/onerunXareasum.csv'
    
    #print("area_file <",area_file,">")
    area_df = pd.read_csv(area_file)
    area_df['run_id'] = curr_run_id

    if isfirst:
            Main_areaXsum_df = area_df
            isfirst = False
    else:
            Main_areaXsum_df = Main_areaXsum_df.append(area_df, ignore_index=True)

Main_areaXsum_df["Num_fail"].fillna(0, inplace = True)
print("\nShape of Main_areaXsum_df")
print(Main_areaXsum_df.dtypes)
print(Main_areaXsum_df.tail(5))

#write table in CSV
summTbl_csv = "test_areaXsum_df.csv"
Main_areaXsum_df.to_csv(summTbl_csv, index=False)
print("\n Writing of summary CSV table for <"+summTbl_csv+"> completed!")


# In[86]:


del  [[Ensmblfile , area_df, link_df, vcr_df, Main_areaXsum_df]]
gc.collect()
Ensmblfile = pd.DataFrame()
area_df = pd.DataFrame()
link_df = pd.DataFrame()
vcr_df = pd.DataFrame()
Main_areaXsum_df = pd.DataFrame()

print( "\nExecution of <Main_areaXsum> completed.")


# In[ ]:




