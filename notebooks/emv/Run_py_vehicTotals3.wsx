<?xml version="1.0" encoding="UTF-8"?>
<operation atomic="false" bgcolor="4280839393" id="{732c569d-8f0c-48d9-9480-6ec5201d54e6}" label="Root" plugin="www.csiro.au/workspace/builtin" type="Workspace">
  <environment>
    <platform>windows 64bit</platform>
    <lastModified>2020-09-09T20:27:38</lastModified>
    <plugin name="www.csiro.au/workspace/builtin" uri="www.csiro.au/workspace/packages/5.4.2/Windows/x86_64"/>
    <plugin name="www.csiro.au/workspace/python" uri="www.csiro.au/workspace/packages/5.4.2/Windows/x86_64"/>
  </environment>
  <input name="Dependencies" size="0"/>
  <displays>
    <widgetwindow docklocation="8" floating="false">
      <iobase wsDataPath="wsDataPath:{557fbc54-c7e3-4803-a950-7888e78cffab}:input:Script"/>
      <displaywidget displayWidgetName="CSIRO::Widgets::JSEditWidget"/>
      <geometry>AdnQywACAAAAAAAAAAAAFgAAAy8AAAF4AAAAAAAAABYAAAMvAAABeAAAAAEAAAAAB4A=</geometry>
      <properties>
        <property name="replaceTabsWithSpaces" value="true"/>
      </properties>
    </widgetwindow>
  </displays>
  <operation bgcolor="4284395451" id="{557fbc54-c7e3-4803-a950-7888e78cffab}" label="Create databse" plugin="www.csiro.au/workspace/python" type="RunPythonScript">
    <input name="Dependencies" size="1"/>
    <input name="Script" preferredwidget="CSIRO::Widgets::JSEditWidget"># This code creates the tables &lt;vehicTotals&gt;  and &lt;linkXIsSafe&gt; in the analysis database.
# The code assumes that the Matsim output table &lt;event&gt; is already in the database.
# The code assumes that the Matsim output table &lt;link&gt; is already in the database.
# The code assumes that the &lt;nodeXzone&gt; table is already in the database.
# The code requires as input the databse filename.

import sqlite3
import pandas as pd
import sys, traceback
import os

print (&quot;Current working directory&quot;)
print(os.getcwd())
os.chdir(os.path.dirname(fileName))
print (&quot;New working directory&quot;)
print(os.getcwd())

# input tables
#areaSHPtbl = &quot;areaSHPdata&quot;
matsimEventTbl = &quot;event&quot;
#matsimNodeTbl = &quot;node&quot;
matsimLinkTbl = &quot;link&quot;
nodeXzoneTbl = &quot;nodeXzone&quot;

# output tables
vehTotalsTbl = &quot;vehicTotals&quot;
linkSafeTbl = &quot;linkXIsSafe&quot;

# Connect to the database file
conn = sqlite3.connect(fileName)
c = conn.cursor()

try: 
  #initial cleanup
  sqlqry = &quot;DROP TABLE IF EXISTS &quot; + vehTotalsTbl
  print(&quot;Running query &lt;&quot;+sqlqry+&quot;&gt;. Please wait!&quot;)
  c.execute(sqlqry)
  sqlqry = &quot;DROP TABLE IF EXISTS &quot; + linkSafeTbl
  print(&quot;Running query &lt;&quot;+sqlqry+&quot;&gt;. Please wait!&quot;)
  c.execute(sqlqry)  
  
  # Get ids of used links in event table
  print(&quot;\nSet up table of used links&quot;)
  sqlqry = &quot;SELECT link as linkID&quot;
  sqlqry += &quot; FROM &quot;+matsimEventTbl 
  sqlqry += &quot; WHERE link&lt;&gt;''&quot;
  sqlqry += &quot; GROUP BY link&quot;
  sqlqry += &quot; ORDER BY link&quot;
  print(&quot;Running query &lt;&quot;+sqlqry+&quot;&gt;. Please wait!&quot;)
  used_df = pd.read_sql_query(sqlqry, conn)
  print(&quot;Shape of used_df&quot;)
  print(used_df.shape)
  print(used_df.tail(5))
  #write table in CSV 
  used_df.to_csv(&quot;usedLinks.csv&quot;, index=False)
  print(&quot;Writing of CSV table &lt;usedLinks.csv&gt; completed!&quot;)

  print(&quot;\nSetup linkXisEvacXisSafe table&quot;)  
  sqlqry = &quot;SELECT DISTINCT id AS linkID, capacity&quot;
  sqlqry += &quot;, [from] AS origstart, [to] AS origend&quot;
  sqlqry += &quot; FROM &quot;+matsimLinkTbl
  sqlqry += &quot; ORDER BY id&quot;  
  print(&quot;Running query &lt;&quot;+sqlqry+&quot;&gt;. Please wait!&quot;)
  link_df = pd.read_sql_query(sqlqry, conn)
  print(&quot;Shape of link_df&quot;)
  print(link_df.shape)
  print(link_df.tail(5))

  print(&quot;\nReduce link table to those used in event&quot;)  
  link_df = pd.merge (link_df, used_df, how='inner', on=&quot;linkID&quot;)
  print(&quot;Shape of link_df&quot;)
  print(link_df.shape)
  print(link_df.tail(5))

  print(&quot;\nGet linkXzone table&quot;)
  sqlqry = &quot;SELECT&quot;
  sqlqry += &quot; A1.id as linkID&quot;
  #sqlqry += &quot;, B1.nodeID AS SRCNode&quot;
  sqlqry += &quot;, B1.zoneID AS zoneID&quot;
  sqlqry += &quot; FROM &quot;+matsimLinkTbl+&quot; AS A1&quot;
  sqlqry += &quot; LEFT JOIN &quot;+nodeXzoneTbl+&quot; AS B1 ON ((A1.[from]=B1.nodeID))&quot; 
  sqlqry += &quot; ORDER BY A1.id &quot;
  print(&quot;Running query &lt;&quot;+sqlqry+&quot;&gt;. Please wait!&quot;)
  temp1_df = pd.read_sql_query(sqlqry, conn)
  print(&quot;Shape of temp1_df&quot;)
  print(temp1_df.shape)
  print(temp1_df.tail(5))
  print(temp1_df.dtypes)

  print(&quot;\nAdd zoneid column to link table&quot;)  
  link_df = pd.merge (link_df, temp1_df, how='left', on=&quot;linkID&quot;)
  print(&quot;Shape of link_df&quot;)
  print(link_df.shape)
  print(link_df.tail(5))

  print(&quot;\nExtract data on source links&quot;) 
  sqlqry = &quot;SELECT link AS linkID&quot;
  #sqlqry += &quot;, type  as  acttype&quot;
  sqlqry += &quot;, 1 AS isSRCE&quot;
  sqlqry += &quot;, COUNT(vehicle) AS totalvehic&quot; 
  #sqlqry += &quot;, IS_DST AS isDEST&quot;
  sqlqry += &quot; FROM &quot; + matsimEventTbl
  sqlqry += &quot; WHERE (type='vehicle enters traffic')&quot;
  sqlqry += &quot; GROUP BY link&quot;
  sqlqry += &quot; ORDER BY totalvehic&quot; 
  print(&quot;Running query &lt;&quot;+sqlqry+&quot;&gt;. Please wait!&quot;)
  temp1_df = pd.read_sql_query(sqlqry, conn)
  print(&quot;Shape of temp1_df&quot;)
  print(temp1_df.shape)
  print(temp1_df.tail(5))

  print(&quot;\nAdd totalvehic column to link table&quot;)  
  link_df = pd.merge (link_df, temp1_df, how='left', on=&quot;linkID&quot;)
  print(&quot;Shape of link_df&quot;)
  print(link_df.shape)
  print(link_df.tail(5))

  print(&quot;\nGet vehicle totals per zoneID+startnode&quot;)
  vehtot_df = link_df.groupby(['zoneID', 'origstart'])['totalvehic'].sum().reset_index()
  vehtot_df.columns= ['zoneID', 'nodeID', 'totalvehic']
  print(vehtot_df.shape)
  print(vehtot_df.tail(5))

  print(&quot;\nDrop rows with no vehicles&quot;)
  vehtot_df.drop(vehtot_df[vehtot_df.totalvehic &lt; 1].index, inplace=True)

  print(&quot;\nGet vehicle totals per zoneID&quot;)
  sqlqry = &quot;SELECT areaID, sum(totvehicle) as grpsum&quot;
  sqlqry += &quot; FROM vehtot_df&quot; 
  sqlqry += &quot; GROUP BY areaID&quot; 
  print(&quot;Running query &lt;&quot;+sqlqry+&quot;&gt;. Please wait!&quot;)
  vehtot_df['isSRCE'] = 1
  vehtot_df['grpsum'] = vehtot_df.groupby(['zoneID'])['totalvehic'].transform('sum')

  #get proportion
  print(&quot;\nGet proportion per zoneID&quot;)
  vehtot_df['grpprop'] = 0
  vehtot_df.loc[vehtot_df.grpsum&gt;0, 'grpprop'] = vehtot_df.loc[vehtot_df.grpsum&gt;0,['totalvehic']].values/vehtot_df.loc[vehtot_df.grpsum&gt;0,['grpsum']].values

  print(&quot;Shape of vehtot_df&quot;)
  print(vehtot_df.shape)
  print(vehtot_df.tail(10))

  #write table in database 
  vehtot_df.to_sql(vehTotalsTbl, conn, index=False)
  print(&quot;\nWriting of SQL table &lt;&quot;+vehTotalsTbl+&quot;&gt; completed!&quot;)

  #write table in database 
  link_df.to_sql(linkSafeTbl, conn, index=False)
  print(&quot;\nWriting of SQL table &lt;&quot;+linkSafeTbl+&quot;&gt; completed!&quot;)

  # Commit the changes
  conn.commit()
  
  print( &quot;\nExecution of &lt;Run_py_vehicTotals&gt; completed!&quot;)
  
except: 
  traceback.print_exc(file=sys.stdout) 
  print (&quot;**** Error encountered. Printing traceback! ****&quot;)
  traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
  print(&quot;*** Print_exception:&quot;)
  # exc_type below is ignored on 3.5 and later
  traceback.print_exception(exc_type, exc_value, exc_traceback,
                              limit=2, file=sys.stdout)
finally:
  
  # Close database file
  conn.close()</input>
    <input name="Persistent namespace">0</input>
    <input name="Run in GUI thread">0</input>
    <view height="152" width="169" x="-304" y="-397.5"/>
    <namedinputs>
      <input dataname="fileName" dataplugin="www.csiro.au/workspace/builtin" datatype="QString"/>
    </namedinputs>
    <namedoutputs/>
  </operation>
  <operation bgcolor="4294931456" dataname="Run python script dependency" dataplugin="www.csiro.au/workspace/builtin" datatype="CSIRO::DataExecution::Dependency" id="{8371102e-4432-4c16-86ef-49e97ebf9ae3}" label="Run python script dependency" plugin="www.csiro.au/workspace/builtin" type="WorkspaceOutput">
    <input name="Dependencies" size="0"/>
    <view height="89" width="169" x="-54" y="-366"/>
  </operation>
  <operation bgcolor="4291611648" dataname="Database file" dataplugin="www.csiro.au/workspace/builtin" datatype="QString" id="{48a26dcb-f2cb-451c-8b3c-8bd1cf220487}" label="Database file" plugin="www.csiro.au/workspace/builtin" type="Variable">
    <input name="Dependencies" size="0"/>
    <input name="Database file">D:\EMV\Scen1\output\matsim_output.db</input>
    <view height="89" width="169" x="-864" y="-366"/>
  </operation>
  <operation bgcolor="4284395451" id="{98d77263-c695-43a3-991e-01b7239e7732}" label="Resolve Absolute File Path" plugin="www.csiro.au/workspace/builtin" type="ResolveAbsoluteFilePath">
    <input name="Dependencies" size="0"/>
    <input name="File name" nodata="true" preferredwidget="CSIRO::Widgets::FileNameWidget"/>
    <view height="89" width="169" x="-554" y="-366"/>
  </operation>
  <connection copy="false" dest_name="Run python script dependency" from_op="{557fbc54-c7e3-4803-a950-7888e78cffab}" src_name="Dependencies" to_op="{8371102e-4432-4c16-86ef-49e97ebf9ae3}"/>
  <connection copy="false" dest_name="File name" from_op="{48a26dcb-f2cb-451c-8b3c-8bd1cf220487}" src_name="Database file" to_op="{98d77263-c695-43a3-991e-01b7239e7732}"/>
  <connection copy="false" dest_name="fileName" from_op="{98d77263-c695-43a3-991e-01b7239e7732}" src_name="Relative path" to_op="{557fbc54-c7e3-4803-a950-7888e78cffab}"/>
</operation>
