#!/usr/bin/env python
# coding: utf-8

# In[85]:


#This code creates the BlkXGDD2 table which assigns the total GDD gor each sampling date
# **** Requirements ****
# All FullID  used must be TRIMmed and in UPPER case
# All FullID  used must not end in '|'
# All VarBlkID and BlockID  used must be TRIMmed and in UPPER case


# In[86]:


import sqlite3
import pandas as pd
import sys, traceback
import os
import numpy as np
import gc


# In[87]:


#function paramaters
#1. Path to database file
fileName = "C:/SharedDocs/Food SupplyChain/PernotRicard/Vintage-2010/GAMA-Matpred/MatPred/models/Testing/matpred3_py.db"

#INPUT TABLES
#2.Database table for block sampling data
blkSmplTbl ="A_blk_data"

#3.Database table for block harvest data
blkXhdateTbl = "A_blkXhdate"

#4.Database table for block location (longitude-latitude) data
blkLonlatTbl = "A_blkXlonlat"

#5.Database table for weather station temperature data
stnTempTbl = "A_all_stn_data"

#6.Database table for weather station location (longitude-latitude) data
stnLonlatTbl = "A_stnXlonlat"

#OUTPUT TABLES
#7. Database table for calculated block pct Bx vs aggregated EDD
blkXEDDTbl = "Y_blkXEDD_norepl"

#SELECTIONS
#8. Allow replacement of missing target Bx with average BX for (variety X harvest date)
allowHDateReplace = False

#9. Allow replacement of missing target Bx with average BX for (variety X harvest year)
allowHYearReplace = False


# In[113]:


print ("Current working directory")
print(os.getcwd())
os.chdir(os.path.dirname(fileName))

print("\nFUNCTION SETTINGS:")
print ("New working directory:", os.getcwd())
print ("1. Path to database file:",fileName)
print ("2.Database table for block sampling data:",blkSmplTbl)
print ("3.Database table for block harvest data:",blkXhdateTbl)
print ("4.Database table for block location (longitude-latitude) data:",blkLonlatTbl)
print ("5.Database table for weather station temperature data:",stnTempTbl)
print ("6.Database table for weather station location (longitude-latitude) data:",stnLonlatTbl)
print ("7. Database table for calculated block pct Bx vs aggregated EDD:",blkXEDDTbl)
print ("8. Allow replacement of missing target Bx with average BX for (variety X harvest date):",allowHDateReplace)
print ("9. Allow replacement of missing target Bx with average BX for (variety X harvest year):",allowHYearReplace)


# In[90]:


# intermediate tables
blkXpctBxTbl = "B_blkXpctBx"
blkStndistTbl = "B_blkXStndist"
stnEDDTblnm = "B_stnXEDDXdate"


# In[91]:


# Connect to the database file
conn = sqlite3.connect(fileName)
c = conn.cursor()

print("\n Tables in database:")
res = c.execute("SELECT name FROM sqlite_master WHERE type='table';")
for name in res:
    print (name[0])


# In[92]:


# GDD Table version 1
#get stations per year (grape growing season in Aust) 
print("\nGet stations per vintage year.") 
#print("Applies only to months from Jan 1 to June 30; Oct 1- Dec 31") 
sqlqry = "SELECT CAST(StationID AS TEXT) AS MetStnNo"
sqlqry += ", CAST(Year AS INTEGER) AS Year"
sqlqry += ", CAST(Month AS INTEGER) AS Month"
sqlqry += ", CAST(Day AS INTEGER) AS Day"
sqlqry += " FROM " + stnLonlatTbl 
sqlqry += ", (SELECT Year, Month, Day"
sqlqry += " FROM " + stnTempTbl
#sqlqry += " WHERE ((Month<7) OR (Month>9))" 
sqlqry += " GROUP BY Year, Month, Day"
sqlqry += " ORDER BY Year, Month, Day)"
 
print("\n Running query <"+sqlqry+">. Please wait!")
temp1_df = pd.read_sql_query(sqlqry, conn)
print("\n Shape of temp1_df"); print(temp1_df.shape)
print("\n temp1_df.head(5)"); print(temp1_df.head(5))
print("\n temp1_df.tail(5)"); print(temp1_df.tail(5))

sqlqry = "SELECT CAST(MetStnNo AS TEXT) AS MetStnNo"
sqlqry += ", CAST(Year AS INTEGER) AS Year"
sqlqry += ", CAST(Month AS INTEGER) AS Month"
sqlqry += ", CAST(Day AS INTEGER) AS Day"
sqlqry += ", CAST(MaxTemp_C AS FLOAT) AS MaxTemp, CAST(MinTemp_C AS FLOAT) AS MinTemp"
sqlqry += " FROM " + stnTempTbl
#sqlqry += " WHERE ((Month<7) OR (Month>9))" 
sqlqry += " ORDER BY MetStnNo, Year, Month, Day"
 
print("\n Running query <"+sqlqry+">. Please wait!")
temp2_df = pd.read_sql_query(sqlqry, conn)
print("\n Shape of temp2_df"); print(temp2_df.shape)
print("\n temp2_df.head(5)"); print(temp2_df.head(5))
print("\n temp2_df.tail(5)"); print(temp2_df.tail(5))

temp1_df = pd.merge(temp1_df, temp2_df, on=['MetStnNo','Year','Month','Day'], how='left')
temp1_df['iskeep']= True
temp1_df.loc[temp1_df['MaxTemp'].isnull() & temp1_df['MinTemp'].isnull(),'iskeep'] = False
temp1_df = temp1_df.loc[temp1_df['iskeep']]
#temp1_df.drop(['iskeep'], axis = 1, inplace = True)

print("\n Shape of temp1_df"); print(temp1_df.shape)
print("\n temp1_df.head(5)"); print(temp1_df.head(5))
print("\n temp1_df.tail(5)"); print(temp1_df.tail(5))


# In[93]:


temp1_df['nullMax'] = False
temp1_df['nullMin'] = False
#set nullMax = True if maxtemp is not real
temp1_df['nullMax'] = (temp1_df.applymap(np.isreal)['MaxTemp']==False)
#set nullMin = True if mintemp is not real
temp1_df['nullMin'] = (temp1_df.applymap(np.isreal)['MinTemp']==False)
#if maxtemp is missing but minTemp is valid , then Maxtemp = Mintemp+1
temp1_df.loc[temp1_df['nullMax'] & (temp1_df['nullMin']==False),'MaxTemp'] =  1.0 + temp1_df.loc[temp1_df['nullMax'] & (temp1_df['nullMin']==False),'MinTemp']  
#if mintemp is missing but maxTemp is valid , then Mintemp = Maxtemp-1
temp1_df.loc[temp1_df['nullMin'] & (temp1_df['nullMax']==False),'MinTemp'] = -1.0 + temp1_df.loc[temp1_df['nullMin'] & (temp1_df['nullMax']==False),'MaxTemp']  

#calculate EDD for the single day
temp1_df.loc[temp1_df['iskeep'],'EDD'] =  0.5 * (temp1_df.loc[temp1_df['iskeep'],'MaxTemp'] + temp1_df.loc[temp1_df['iskeep'],'MinTemp'] - 20.0) 
#delete other columns
temp1_df.drop(['MaxTemp','MinTemp','iskeep','nullMax','nullMin'], axis=1, inplace=True)
#if EDD <0, EDD=0
temp2_df = temp1_df.loc[temp1_df['EDD']<0]
temp1_df.loc[temp1_df['EDD']<0,'EDD'] =  0.0

print("\n Shape of temp1_df"); print(temp1_df.shape)
print(temp1_df.dtypes)
print("\n temp1_df.head(5)"); print(temp1_df.head(5))
print("\n temp1_df.tail(5)"); print(temp1_df.tail(5))

print("\n Shape of temp2_df");print(temp2_df.shape)
print(temp2_df.dtypes)
print("\n temp2_df.tail(5)"); print(temp2_df.tail(5))


# In[94]:


#table export
sqlqry = "DROP TABLE IF EXISTS " + stnEDDTblnm 
print("Running query <"+sqlqry+">. Please wait!")
c.execute(sqlqry)

#write table in database 
temp1_df.to_sql(stnEDDTblnm, conn, index=False)
print("\nWriting of SQL table <"+stnEDDTblnm+"> completed!")

#write table in CSV
summTbl_csv = stnEDDTblnm + ".csv"
temp1_df.to_csv(summTbl_csv, index=False)
print("\nWriting of CSV table <"+summTbl_csv+"> completed!")


# In[95]:


#clean up
del [[temp1_df, temp2_df]] 
gc.collect()
temp1_df = pd.DataFrame()
temp2_df = pd.DataFrame()

print( "\n Clean up for "+ stnEDDTblnm +" completed!")


# In[96]:


print("\nSetup blkXstndist summary table") 
sqlqry = "SELECT -1 as seqno, TRIM(UPPER(A1.VarBlkID)) as VarBlkID, B1.StationID"
sqlqry += ", ((A1.Lat-B1.Latitude)*(A1.Lat-B1.Latitude)+(A1.Long-B1.Longitude)*(A1.Long-B1.Longitude)) as dist_sqr, -1 as d_rank FROM "
sqlqry += blkLonlatTbl + " as A1" 
sqlqry += ", " + stnLonlatTbl +" AS B1" 
sqlqry += " ORDER BY A1.VarBlkID, dist_sqr"
 
print("Running query <"+sqlqry+">. Please wait!")
BlkxStndist_df = pd.read_sql_query(sqlqry, conn)
BlkxStndist_df['seqno'] = BlkxStndist_df.index.values

print("\n Shape of BlkxStndist_df"); print(BlkxStndist_df.shape)
print("\n BlkxStndist_df.head(40)"); print(BlkxStndist_df.head(40))

#get minimum seqno per block
temp1_df = BlkxStndist_df.groupby(['VarBlkID'])['seqno'].min().reset_index()
temp1_df.columns= ['VarBlkID', 'minseq']
print(temp1_df.shape)
print("\n temp1_df.head(10)"); print(temp1_df.head(10))

print("\n Update minseq fields in BlkxStndist_df table")
BlkxStndist_df = pd.merge(BlkxStndist_df, temp1_df, on='VarBlkID', how='left')
BlkxStndist_df['d_rank'] = BlkxStndist_df['seqno']-BlkxStndist_df['minseq']+1
BlkxStndist_df.drop(['minseq'], axis=1, inplace=True)
print("\n Shape of BlkxStndist_df"); print(BlkxStndist_df.shape)
print("\n BlkxStndist_df.head(40)"); print(BlkxStndist_df.head(40))


# In[97]:


#table export
sqlqry = "DROP TABLE IF EXISTS " + blkStndistTbl 
print("Running query <"+sqlqry+">. Please wait!")
c.execute(sqlqry)

#write table in database 
BlkxStndist_df.to_sql(blkStndistTbl , conn, index=False)
print("\nWriting of SQL table <"+blkStndistTbl +"> completed!")

#write table in CSV
summTbl_csv = blkStndistTbl  + ".csv"
BlkxStndist_df.to_csv(summTbl_csv, index=False)
print("\nWriting of CSV table <"+summTbl_csv+"> completed!")


# In[98]:


#clean up
del [[temp1_df, BlkxStndist_df]] 
gc.collect()
temp1_df = pd.DataFrame()
BlkxStndist_df = pd.DataFrame()

print( "\nClean up for "+ blkStndistTbl +" completed!")


# In[99]:


print("\nSetup block X targetBx X harvest-date table") 
sqlqry = "SELECT TRIM(UPPER(new_FullID)) AS FullID" 
sqlqry += ", CAST(Year AS INTEGER) AS Year"
sqlqry += ", CAST(Month AS INTEGER) AS Month"
sqlqry += ", CAST(Day AS INTEGER) AS Day"
sqlqry += ", CAST(target_Bx AS REAL) AS targetBx"
sqlqry += ", actual_harvest_date, count_harvests"
sqlqry += ", TRIM(UPPER(block_ID)) AS block_ID"
sqlqry += ", TRIM(UPPER(variety)) AS variety"
sqlqry += " FROM " + blkXhdateTbl 
sqlqry += " ORDER BY FullID, Year, Month, Day"

print("Running query <"+sqlqry+">. Please wait!")
blkXtgtBx_df = pd.read_sql_query(sqlqry, conn)
blkXtgtBx_df['isnullBX'] = True
blkXtgtBx_df.loc[blkXtgtBx_df['targetBx']>0, 'isnullBX'] = False
print("\n Shape of blkXtgtBx_df"); print(blkXtgtBx_df.shape)
print("\n blkXtgtBx_df.head(5)"); print(blkXtgtBx_df.head(5))
#print("\n blkXtgtBx_df.tail(5)"); print(blkXtgtBx_df.tail(5))

BXcount = len (blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX'], ['FullID','targetBx']].index)
print ("\n Number of Null Bx = ", BXcount)


# In[100]:


if (allowHDateReplace):
    print("\nSetup variety X date X avg(target Bx) table") 
    sqlqry = "SELECT TRIM(UPPER(variety)) AS variety, actual_harvest_date"
    sqlqry += ", Avg(CAST(target_Bx as REAL)) AS date_BX"
    sqlqry += " FROM "+ blkXhdateTbl 
    sqlqry += " WHERE (target_Bx>0)"
    sqlqry += " GROUP BY variety, actual_harvest_date"
    sqlqry += " ORDER BY variety, actual_harvest_date"

    print("\n Running query <"+sqlqry+">. Please wait!")
    temp1_df = pd.read_sql_query(sqlqry, conn)
    print("\n Shape of temp1_df"); print(temp1_df.shape)
    print("\n temp1_df.head(5)"); print(temp1_df.head(5))
    #print("\n temp1_df.tail(5)"); print(temp1_df.tail(5))

    print("\n Merge harvest data with replacement harvest date average")
    blkXtgtBx_df = pd.merge(blkXtgtBx_df, temp1_df, on=['variety','actual_harvest_date'], how='left')
    #print("\n merged blkXtgtBx_df.head(5)"); print(blkXtgtBx_df.head(5))

    print("\n Null target BX blkXtgtBx_df.head(5)"); print(blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX'] & blkXtgtBx_df['date_BX']>0].head(5))
    blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX'] & blkXtgtBx_df['date_BX']>0, 'targetBx'] = blkXtgtBx_df['date_BX'] 
    print("\n Replaced target BX blkXtgtBx_df.head(5)"); print(blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX'] & blkXtgtBx_df['date_BX']>0].head(5))
    blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX'] & blkXtgtBx_df['date_BX']>0, 'isnullBX'] = False 
    blkXtgtBx_df.drop(['date_BX'], axis = 1, inplace = True)

    BXcount = len (blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX'], ['FullID','targetBx']].index)
    print ("\n Number of Null Bx = ", BXcount)


# In[101]:


if (allowHYearReplace):
    print("\n Setup variety X year X avg(target Bx) table") 
    sqlqry = "SELECT TRIM(UPPER(variety)) AS variety, CAST(Year as INTEGER) AS Year"
    sqlqry += ", Avg(CAST(target_Bx as REAL)) AS year_BX"
    sqlqry += " FROM "+ blkXhdateTbl 
    sqlqry += " WHERE (target_Bx>0)"
    sqlqry += " GROUP BY variety, Year"
    sqlqry += " ORDER BY variety, Year"

    print("\n Running query <"+sqlqry+">. Please wait!")
    temp2_df = pd.read_sql_query(sqlqry, conn)
    print("\n Shape of temp2_df"); print(temp2_df.shape)
    print("\n temp2_df.head(5)"); print(temp2_df.head(5))
    #print("\n temp2_df.tail(5)"); print(temp2_df.tail(5))

    print("\n Merge harvest data with replacement harvest year average")
    blkXtgtBx_df = pd.merge(blkXtgtBx_df, temp2_df, on=['variety','Year'], how='left')
    #print("\n merged blkXtgtBx_df.head(5)"); print(blkXtgtBx_df.head(5))

    print("\n Null target BX blkXtgtBx_df.head(5)"); print(blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX'] & blkXtgtBx_df['year_BX']>0].head(5))
    blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX'] & blkXtgtBx_df['year_BX']>0, 'targetBx'] = blkXtgtBx_df['year_BX'] 
    print("\n Replaced target BX blkXtgtBx_df.head(5)"); print(blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX'] & blkXtgtBx_df['year_BX']>0].head(5))
    blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX'] & blkXtgtBx_df['year_BX']>0, 'isnullBX'] = False 
    blkXtgtBx_df.drop(['year_BX'], axis = 1, inplace = True)

    BXcount = len (blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX'], ['FullID','targetBx']].index)
    print ("\n Remaining number of Null Bx = ", BXcount)


# In[102]:


print("\n Remove remaining Null target BX"); 
print(blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX']].head(5))

blkXtgtBx_df = blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX']==False]
BXcount = len (blkXtgtBx_df.loc[blkXtgtBx_df['isnullBX'], ['FullID','targetBx']].index)
print ("\n Number of Null Bx = ", BXcount)
blkXtgtBx_df.drop(['isnullBX'], axis = 1, inplace = True)
print(blkXtgtBx_df.shape)
print("\n Final blkXtgtBx_df.head(5)"); print(blkXtgtBx_df.head(5))


# In[103]:


#clean up
del [[temp1_df, temp2_df]] 
gc.collect()
temp1_df = pd.DataFrame()
temp2_df = pd.DataFrame()

print( "\nClean up for blkXtargetBX table completed!")


# In[104]:


print("\nSetup block X Sampling Bx table") 
sqlqry = "SELECT UPPER(TRIM(B1.FullID)) AS FullID"
sqlqry += ", B1.GrowerCode"
sqlqry += ", UPPER(TRIM(B1.VarBlkID)) AS VarBlkID"
sqlqry += ", CAST(B1.BXvalue AS REAL) AS BXvalue"
sqlqry += ", B1.Sampledate"
sqlqry += ", CAST(B1.VintageYear AS INTEGER) AS Year"
#sqlqry += " FROM " + blkLonlatTbl + " AS A1"
sqlqry += " FROM " + blkSmplTbl + " AS B1" 
#sqlqry += " INNER JOIN " + blkSmplTbl + " AS B1" 
sqlqry += " INNER JOIN " + blkLonlatTbl + " AS A1"
sqlqry += " ON (A1.VarBlkID = B1.VarBlkID)"
sqlqry += " WHERE ((B1.Sampledate<>'')" 
sqlqry += " AND (BXvalue>0))"
sqlqry += " ORDER BY B1.VintageYear, B1.VarBlkID, B1.Sampledate"

print("Running query <"+sqlqry+">. Please wait!")
BlkxBXxEDD_df = pd.read_sql_query(sqlqry, conn)

print("\n Shape of BlkxBXxEDD_df"); print(BlkxBXxEDD_df.shape)
print("\n BlkxBXxEDD_df.head(5)"); print(BlkxBXxEDD_df.head(5))
print("\n BlkxBXxEDD_df.tail(5)"); print(BlkxBXxEDD_df.tail(5))


# In[105]:


print ("Add Month and Day columns from Sampledate")
BlkxBXxEDD_df['s_Date'] = pd.to_datetime(BlkxBXxEDD_df['Sampledate'],dayfirst=True)
BlkxBXxEDD_df['Month'] = BlkxBXxEDD_df['s_Date'].dt.month
BlkxBXxEDD_df['Day'] = BlkxBXxEDD_df['s_Date'].dt.day
BlkxBXxEDD_df.drop(['s_Date'], axis = 1, inplace = True)

print("\n BlkxBXxEDD_df.head(10)"); print(BlkxBXxEDD_df.head(10))


# In[106]:


print ("Merge Block-Sampling table with Block-targetBX table")
print("\n blkXtgtBx_df.head(5)"); print(blkXtgtBx_df.head(5))
temp1_df = blkXtgtBx_df.loc[blkXtgtBx_df['targetBx']>0, ['FullID', 'targetBx', 'variety', 'actual_harvest_date']]
temp2_df = pd.merge(BlkxBXxEDD_df, temp1_df, on=['FullID'], how='left')
temp2_df['pct_Bx'] = 0.0
temp2_df['aggEDD'] = 0.0
temp2_df['DayCount'] = 0.0
temp2_df.loc[temp2_df['targetBx']>0, ['pct_Bx']] = 100.0 * temp2_df['BXvalue'] / temp2_df['targetBx']
print("\n Sampling before merge"); print(temp2_df.shape)
temp2_df = temp2_df.loc[temp2_df['pct_Bx']>0]
print("\n Sampling after merge");print(temp2_df.shape)
print("\n temp2_df.head(5)"); print(temp2_df.head(5))


# In[107]:


#clean up
del [[BlkxBXxEDD_df]] 
BlkxBXxEDD_df = pd.DataFrame()
BlkxBXxEDD_df = temp2_df
del [[temp1_df, temp2_df, blkXtgtBx_df]] 
gc.collect()
temp1_df = pd.DataFrame()
temp2_df = pd.DataFrame()
blkXtgtBx_df = pd.DataFrame()

print( "\nClean up for blk-pctBX-EDD table completed!")


# In[108]:


print("\nSetup blkIDXyear summary table")
temp1_df = BlkxBXxEDD_df[['VarBlkID', 'Year', 'aggEDD']]
print("\n Shape of temp1_df");print(temp1_df.shape)
print("\n temp1_df.tail(5)"); print(temp1_df.tail(5))

BlkxYr_df = temp1_df.groupby(['VarBlkID', 'Year'])['aggEDD'].min().reset_index()
BlkxYr_df = BlkxYr_df.sort_values(['Year', 'VarBlkID'], ascending=[True, True])

print("\n Shape of BlkxYr_df");print(BlkxYr_df.shape)
#print(BlkxYr_df.dtypes)
#print("\n BlkxYr_df.head(5)"); print(BlkxYr_df.head(5))
print("\n BlkxYr_df.tail(5)"); print(BlkxYr_df.tail(5))


# In[109]:


print("Get available years from Stn-EDD table")
sqlqry = "SELECT CAST(Year AS INTEGER) AS Year, MAX(CAST(Month AS INTEGER)) as maxMonth"
sqlqry += " FROM " + stnEDDTblnm
sqlqry += " GROUP BY Year"
sqlqry += " ORDER BY Year"

print("\n Running query <"+sqlqry+">. Please wait!")
temp2_df = pd.read_sql_query(sqlqry, conn)
#print("\n Types of temp2_df");print(temp2_df.dtypes)
#print("\n Shape of temp2_df");print(temp2_df.shape)
#print("\n temp2_df.tail(10)"); print(temp2_df.tail(10))
#print("\n Types of BlkxYr_df");print(BlkxYr_df.dtypes)
print ("Merge BlkxYr_df table with Year-EDD table")
BlkxYr_df = pd.merge(BlkxYr_df, temp2_df, on=['Year'], how='left')
#print("\n Shape of BlkxYr_df before");print(BlkxYr_df.shape)
#print("\n BlkxYr_df.tail(5)"); print(BlkxYr_df.tail(5))

BlkxYr_df = BlkxYr_df.loc[BlkxYr_df['maxMonth']>0]
print("\n Shape of BlkxYr_df after");print(BlkxYr_df.shape)
print("\n BlkxYr_df.head(5)"); print(BlkxYr_df.head(5))
print("\n BlkxYr_df.tail(5)"); print(BlkxYr_df.tail(5))


# In[111]:


#*** for testing
#print("\n **** For testing only ****")
#BlkxYr_df = BlkxYr_df.loc[BlkxYr_df['Year']>2008]
#BlkxYr_df = BlkxYr_df.head(20)  #use only top 20
#print("\n BlkxYr_df shape"); print(BlkxYr_df.shape)
#print("\n BlkxYr_df.tail(5)"); print(BlkxYr_df.tail(5))


# In[112]:


row_knt = 0

#get blokID and vintage year to forecast
#currBlkID = "AH|504215|GSAB"  #***
#currYear = 2008    #***
#if (currYear>0):  #*** for testing

for row in BlkxYr_df.itertuples():
    row_knt += 1

    currBlkID = getattr(row, 'VarBlkID')  #***
    currYear =  getattr(row, 'Year')      #***
    print("row = <"+str(row_knt)+">, <"+currBlkID+">, <"+str(currYear)+">")

    #get distance ranks of weather stations 
    #print("\n Distance ranks of weather stations.") 
    sqlqry = "SELECT *"
    sqlqry += " FROM " + blkStndistTbl  
    sqlqry += " WHERE (VarBlkID = '" + currBlkID + "')"
    sqlqry += " ORDER BY d_rank"

    #print("\n Running query <"+sqlqry+">. Please wait!")
    temp1_df = pd.read_sql_query(sqlqry, conn)
    #print("\n Shape of temp1_df"); print(temp1_df.shape)
    #print("\n temp1_df.head(5)"); print(temp1_df.head(5))
    
    maxrank = temp1_df['d_rank'].max()

    # data frame 2
    d2 = {'VarBlkID':pd.Series([currBlkID,currBlkID,currBlkID,currBlkID,currBlkID,currBlkID]),
        'StnRank':pd.Series([1,2,3,1,2,3]),       
        'dist_sqr':pd.Series([-1.0,-1.0,-1.0,-1.0,-1.0,-1.0]), 
        'EDD':pd.Series([0.0,0.0,0.0,0.0,0.0,0.0]),       
        'Year':pd.Series([currYear-1,currYear-1,currYear-1,currYear,currYear,currYear])}

    temp2_df = pd.DataFrame(d2)
    #print("\n temp2_df dtypes"); print(temp2_df.dtypes)  
    #print("\n temp2_df.head(10)"); print(temp2_df.head(10)) 

    #get days in vintage year (grape growing season in Aust) 
    #print("\nGet days in vintage year from Oct 1 (previous year ) to June 30 (current year)") 
    sqlqry = "SELECT CAST(Year AS INTEGER) AS IYear"
    sqlqry += ", CAST(Month AS INTEGER) AS IMonth"
    sqlqry += ", CAST(Day AS INTEGER) AS IDay"
    sqlqry += " FROM " + stnTempTbl  
    sqlqry += " WHERE (((IYear=" + str(currYear) + ") AND (IMonth<7))" 
    sqlqry += " OR ((IYear=" + str(currYear-1) + ") AND (IMonth>9)))" 
    sqlqry += " GROUP BY IYear, IMonth, IDay"
    sqlqry += " ORDER BY IYear, IMonth, IDay"

    #print("\n Running query <"+sqlqry+">. Please wait!")
    temp3_df = pd.read_sql_query(sqlqry, conn)
    
    #print("\n dtypes of temp3_df");print(temp3_df.dtypes)
    #print("\n temp3_df.head(10)"); print(temp3_df.head(10))

    temp3_df.rename(columns={'IYear':'Year',
                          'IMonth':'Month',
                          'IDay':'Day'}, 
                 inplace=True)
    #print("\n temp3_df.tail(10)"); print(temp3_df.tail(10))

    #print("\n Create BlkID X Year X GDD Table")
    BlkXstnXEDD_df = pd.merge(temp2_df, temp3_df, on='Year', how='outer')
    #sort area_df wrt to Year, Month, Day
    BlkXstnXEDD_df = BlkXstnXEDD_df.sort_values(['Year','Month','Day'], ascending=[True,True,True])

    #print("\n Shape of BlkXstnXEDD_df"); print(BlkXstnXEDD_df.shape)
    #print("\n BlkXstnXEDD_df.head(10)"); print(BlkXstnXEDD_df.head(10))
    #print("\n BlkXstnXEDD_df.tail(10)"); print(BlkXstnXEDD_df.tail(10)) 
    
    currrank = 1
    min_dist = BlkXstnXEDD_df['dist_sqr'].min()
    #print("Mindist = ",min_dist)
    
    #if (min_dist<0): #for testing
    while ((min_dist<0) & (currrank <= maxrank)):
        selStn = temp1_df.loc[temp1_df['d_rank']==currrank,'StationID'].min()
        selDist = temp1_df.loc[temp1_df['d_rank']==currrank,'dist_sqr'].min()
        selDist = "%.5f" % selDist
        #print("currrank <",currrank,">")
        #print("Selected station <",selStn,">")
        #print("Station dist-squared <",selDist,">")

        temp4_df = BlkXstnXEDD_df.loc[BlkXstnXEDD_df['dist_sqr']<0,['StnRank','Year','Month','Day']]
        temp4_df = temp4_df.groupby(['Year','Month','Day'])['StnRank'].min().reset_index()
        #print("\n Nrows of temp4_df"); print(len(temp4_df))
        #print("\n temp4_df dtypes"); print(temp4_df.dtypes)
        #print("\n temp4_df.head(10)"); print(temp4_df.head(10))

        #get station data for vintage year 
        #print("\n Get station data for vintage year ") 
        sqlqry = "SELECT CAST(Year AS INTEGER) AS Year"
        sqlqry += ", CAST(Month AS INTEGER) AS Month"
        sqlqry += ", CAST(Day AS INTEGER) AS Day"
        sqlqry += ", EDD AS EDD2"
        sqlqry += ", "+str(selDist)+ " as dist_sqr2"
        sqlqry += " FROM " + stnEDDTblnm   
        sqlqry += " WHERE ((MetStnNo='" + selStn + "')"
        sqlqry += " AND ((Year=" + str(currYear) + ")"
        sqlqry += " OR (Year=" + str(currYear-1) + ")))"
        sqlqry += " ORDER BY Year, Month, Day"

        #print("\n Running query <"+sqlqry+">. Please wait!")
        temp5_df = pd.read_sql_query(sqlqry, conn)
        #print("\n Nrows of temp5_df"); print(len(temp5_df))
        #print("\n Shape of temp5_df"); print(temp5_df.shape)
        #print("\n temp5_df.head(40)"); print(temp5_df.head(40))

        if (len(temp5_df)>0):            
            temp5_df = pd.merge(temp5_df, temp4_df, on=['Year', 'Month','Day'], how='inner')
            #print("\n Shape of temp5_df"); print(temp5_df.shape)
            #print("\n temp5_df.head(10)"); print(temp5_df.head(10))      
            #print("\n BlkXstnXEDD_df.head(10)"); print(BlkXstnXEDD_df.head(10))      

            temp6_df = pd.merge(BlkXstnXEDD_df, temp5_df, on=['Year', 'Month','Day', 'StnRank'], how='left')
            temp6_df.loc[temp6_df['dist_sqr2']>0, 'dist_sqr'] = temp6_df['dist_sqr2']
            temp6_df.loc[temp6_df['dist_sqr2']>0, 'EDD'] = temp6_df['EDD2']   
            #delete other columns
            temp6_df.drop(['dist_sqr2', 'EDD2'], axis = 1, inplace = True)

            #print("\nDF Shape of temp6_df")
            #print(temp6_df.shape)
            #print("\n temp6_df.head(40)"); print(temp6_df.head(40))

            del [[BlkXstnXEDD_df]] 
            BlkXstnXEDD_df = temp6_df
            del [[temp6_df]] 
            temp6_df =  pd.DataFrame()

        del [[temp4_df, temp5_df]]
        temp4_df =  pd.DataFrame()
        temp5_df =  pd.DataFrame()
        #gc.collect()

        #print("\nCurrent rank = ", currrank)
        #print("\nDF Shape of BlkXstnXEDD_df")
        #print(BlkXstnXEDD_df.shape)
        #print("\n BlkXstnXEDD_df.head(10)"); print(BlkXstnXEDD_df.head(10))
        #print("\n BlkXstnXEDD_df.tail(10)"); print(BlkXstnXEDD_df.tail(10))

        min_dist = BlkXstnXEDD_df['dist_sqr'].min()
        #print("New Mindist = ",min_dist)    
        currrank += 1
       
    #print ("Calculate weighted GDD") 
    BlkXstnXEDD_df['w_dist'] =  1.0/BlkXstnXEDD_df['dist_sqr']
    BlkXstnXEDD_df['w_EDD'] = BlkXstnXEDD_df['EDD']/BlkXstnXEDD_df['dist_sqr']

    #print(BlkXstnXEDD_df.shape)
    #print("\n BlkXstnXEDD_df.head(10)"); print(BlkXstnXEDD_df.head(10))
    #print("\n BlkXstnXEDD_df.tail(10)"); print(BlkXstnXEDD_df.tail(10))     
    
    temp4_df = BlkXstnXEDD_df.groupby(['VarBlkID','Year','Month','Day']).agg({'w_dist':'sum','w_EDD':'sum'})
    temp4_df['avgEDD']=temp4_df['w_EDD']/temp4_df['w_dist']
    temp4_df['nDays']= 1.0
    temp4_df.drop(['w_dist','w_EDD'], axis=1, inplace=True)

    #get cumulative EDD
    temp4_df = temp4_df.sort_values(['VarBlkID','Year','Month','Day'], ascending=[True,True,True,True])
    temp4_df['cumEDD'] = temp4_df.groupby(['VarBlkID'])['avgEDD'].cumsum()
    temp4_df['cumDays'] = temp4_df.groupby(['VarBlkID'])['nDays'].cumsum()
    #print(temp4_df.shape)
    #print("\n temp4_df.head(10)"); print(temp4_df.head(10))
    temp4_df.drop(['avgEDD','nDays'], axis=1, inplace=True)
    #print("\n temp4_df.tail(10)"); print(temp4_df.tail(10))  
    
    #print("\n Join cumEDD values with sampling dates")
    #print("\n BlkxBXxEDD_df.tail(10) "); print(BlkxBXxEDD_df.tail(10))    
    temp1_df = pd.merge(BlkxBXxEDD_df, temp4_df,on=['VarBlkID','Year','Month','Day'],how='left')
    #print("\n temp1_df.tail(10) Before "); print(temp1_df.loc[temp1_df['cumEDD']>0].tail(10))    
    temp1_df.loc[temp1_df['cumEDD']>0,'aggEDD']=temp1_df.loc[temp1_df['cumEDD']>0,'cumEDD']
    temp1_df.loc[temp1_df['cumEDD']>0,'DayCount']=temp1_df.loc[temp1_df['cumEDD']>0,'cumDays']
    temp1_df.drop(['cumEDD','cumDays'], axis=1, inplace=True)   
    #print("\n temp1_df.tail(10) for BlockID "); print(temp1_df.loc[temp1_df['aggEDD']>0].tail(10))   

    del [[BlkxBXxEDD_df]] 
    BlkxBXxEDD_df = temp1_df
    #print("\n BlkxBXxEDD_df.head(5)"); print(BlkxBXxEDD_df.head(5))     
    del [[BlkXstnXEDD_df, temp1_df, temp2_df, temp3_df, temp4_df ]]
    temp1_df =  pd.DataFrame()
    temp2_df =  pd.DataFrame()    
    temp3_df =  pd.DataFrame()
    temp4_df =  pd.DataFrame()    
    BlkXstnXEDD_df =  pd.DataFrame() 
    gc.collect()

print("Total blocks-samples processed = <"+str(row_knt)+">")    


# In[114]:


print("\n PreView of output table <"+blkXEDDTbl+">.")
print("\n BlkxBXxEDD_df Shape"); print(BlkxBXxEDD_df.shape)     
print("\n BlkxBXxEDD_df.head(10)"); print(BlkxBXxEDD_df.head(10))     
print("\n BlkxBXxEDD_df.tail(10)"); print(BlkxBXxEDD_df.tail(10))


# In[115]:


#table export
sqlqry = "DROP TABLE IF EXISTS " + blkXEDDTbl
print("Running query <"+sqlqry+">. Please wait!")
c.execute(sqlqry)

#write table in database 
BlkxBXxEDD_df.to_sql(blkXEDDTbl, conn, index=False)
print("\nWriting of SQL table <"+blkXEDDTbl+"> completed!")

#write table in CSV
summTbl_csv = blkXEDDTbl + ".csv"
BlkxBXxEDD_df.to_csv(summTbl_csv, index=False)
print("\nWriting of CSV table <"+summTbl_csv+"> completed!")


# In[116]:


#clean up
del [[BlkxYr_df, BlkxBXxEDD_df]]
gc.collect()
BlkxYr_df = pd.DataFrame()
BlkxBXxEDD_df = pd.DataFrame()
print( "\nClean up completed!")


# In[117]:


# Commit the changes
conn.commit()

# Close database file
conn.close()

print ("\n Completed execution of pythod code for Calc_blkXeddXdata")


# In[ ]:




